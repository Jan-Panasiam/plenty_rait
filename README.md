# PlentyMarkets Reorder and inventory transfer tool (RAIT)

## Introduction

This is a collection of tools with the unified goal of calculating the correct reorder or redistribution (moving stock in between 2 warehouses) amount. Each tool is designed to handle one sub-section of problem
1. `plenty_sync` - communicate with Plentymarkets, pull data and create redistributions and reorders
2. `amazon_import` - Pull data from Amazon export files, this is only required if you do not maintain your FBA orders on Plentymarkets
3. `sales_statistic` - Calculate monthly and daily statistics for each variation/date/origin combination, as well as the total of all origins for each month/variation combination
4. `prediction_tool` - Use the statistics to predict future sales for multiple origins or to analyze different variations sale curves
5. `report` - Create a custom sales report for a given time range and place it into either a separate .csv file or directly into a .xlsx/.xlsm sheet
6. `update_spreadsheet` - Update a .xlsx/.xlsm sheet, with the latest data from the database

Everything revolves around a spreadsheet file, which is used as the user interface for the application. The reason for this choice is simply to use a common and well known and tested interface and fill it with the required data.
The ultimate goal of this tool is to keep the sub-tools as flexible as possible without too much coupling, in order to allow replacing tools or to use only a specific selection, this is currently not the case though.

## Installation

Each tool is implemented with the [poetry](https://python-poetry.org/) package manager, make sure that this dependency is properly installed.

Other base dependencies are:
`sudo apt-get install libmariadb3 libmariadb-dev`

Setup mariadb on ubuntu/debian:
`sudo apt-get install mariadb-server && sudo mysql_secure_installation`
Answer the questions like this:
=> root pw, n, Y, Y, Y, Y

## Development

### Hooks

The `utils/hooks/` folder within this project contains two useful bash scripts, which should be used to ensure code-quality and -correctness. Just peform the following two commands and you are good to go:
```bash
mv utils/hooks/commit-msg.sh .git/hooks/commit-msg
mv utils/hooks/pre-commit.sh .git/hooks/pre-commit
```

These will perform a simple spelling check whenever you create a commit message and run the unit tests for all sub-projects you want to commit changes for.
You can exclude specific words from being checked by the spell-checker by appending the word to the personal dictionary with: `echo "word" >> ~/.config/git/.en_personal.rws`.
And you can commit without running tests by calling `git commit` with `--no-verify`.
