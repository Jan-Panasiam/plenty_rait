#!/bin/bash

# INFO: Use git commit --no-verify to skip these commands
# =======================================================

# Get a list of sub-projects, where the unit tests shall be run

# Get the list of changed files
changed_files=$(git diff --cached --name-only)

declare -a directories
for file in $changed_files
do
    # Find the lowest folder within the path
    directory=$(dirname "$file")
    while [[ $directory == *"/"* ]]; do
        directory=$(dirname "$directory")
    done
    directories+=("$directory")
done

for directory in $directories
do
    python3 -m pytest -q $directory
    return_code=$?
    if [ $return_code -ne 0 ]; then
        echo "Abort commit because of failed pytest @ $directory"
        exit 1
    fi
done

# ==========================================================
# Check if the file name of changed files do not contain ASCII characters

if git rev-parse --verify HEAD >/dev/null 2>&1
then
	against=HEAD
else
	# Initial commit: diff against an empty tree object
	against=$(git hash-object -t tree /dev/null)
fi

# If you want to allow non-ASCII filenames set this variable to true.
allownonascii=$(git config --bool hooks.allownonascii)

# Redirect output to stderr.
exec 1>&2

# Cross platform projects tend to avoid non-ASCII filenames; prevent
# them from being added to the repository. We exploit the fact that the
# printable range starts at the space character and ends with tilde.
if [ "$allownonascii" != "true" ] &&
	# Note that the use of brackets around a tr range is ok here, (it's
	# even required, for portability to Solaris 10's /usr/bin/tr), since
	# the square bracket bytes happen to fall in the designated range.
	test $(git diff --cached --name-only --diff-filter=A -z $against |
	  LC_ALL=C tr -d '[ -~]\0' | wc -c) != 0
then
	cat <<\EOF
Error: Attempt to add a non-ASCII file name.

This can cause problems if you want to work with people on other platforms.

To be portable it is advisable to rename the file.

If you know what you are doing you can disable this check using:

  git config hooks.allownonascii true
EOF
	exit 1
fi

# If there are whitespace errors, print the offending file names and fail.
exec git diff-index --check --cached $against --
