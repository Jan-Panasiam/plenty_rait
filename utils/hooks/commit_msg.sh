#!/bin/sh

if ! type aspell > /dev/null; then
  echo "Warning: aspell is not installed"
  exit
fi

personal_dict=$HOME/.config/.en_personal.rws

# Setup a simple personal dictionary if it doesn't exist
if [ ! -f "$personal_dict" ]; then
    if [ ! -d "$HOME/.config/git" ]; then
        mkdir -p $HOME/.config/git
    fi
    # Create a new personal dictionary with one entry "haha"
    echo "personal_ws-1.1 en 0
    haha" >> $personal_dict
fi

words="$(grep -v '^#' "$1" \
         | aspell --personal=$personal_dict list)"
if [ -n "$words" ]; then
  echo "Error: The following words are misspelled"
  echo "$words" | sed 's/^/  /'
  echo "To add new words to your personal dictionary: echo \"word\" >> $personal_dict"
  exit 1
fi
