"""
Sub application of the Plenty RAIT tool with the purpose of predicting
future sales and place the results back into the database.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
from decimal import Decimal, InvalidOperation
import getpass
import keyring
import os
import pandas
import pathlib
import sys
import configparser
import argparse
from loguru import logger


from prediction_tool.service import Forecaster, PredictionSession, DataFormat


PROG_NAME = 'prediction_tool'
USER = os.getlogin()
if sys.platform == 'linux':
    BASE_PATH = os.path.join(
        '/', 'home', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )
elif sys.platform == 'win32':
    BASE_PATH = os.path.join(
        'C:\\', 'Users', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )

if not os.path.exists(BASE_PATH):
    pathlib.Path(BASE_PATH).mkdir(parents=True, exist_ok=True)

DB_CONFIG = os.path.join(BASE_PATH, 'db_config.ini')
CONFIG_FOLDER = os.path.join(BASE_PATH, PROG_NAME)
if not os.path.exists(CONFIG_FOLDER):
    pathlib.Path(CONFIG_FOLDER).mkdir(parents=True, exist_ok=True)

CONFIG_PATH = os.path.join(CONFIG_FOLDER, 'config.ini')

if not os.path.exists(DB_CONFIG):
    open(DB_CONFIG, 'a').close()

if not os.path.exists(CONFIG_PATH):
    open(CONFIG_PATH, 'a').close()


def setup_argparser() -> argparse.ArgumentParser:
    now = datetime.datetime.now().strftime("%Y-%m-%d")
    default_file_path = str(f"./prediction_results_{now}.csv")
    parser = argparse.ArgumentParser(prog=PROG_NAME)
    parser.add_argument('--action', '--function', required=False,
                        help='Choose the action mode of the tool',
                        choices=['prediction', 'analysis', 'visual_sales'],
                        type=str, dest='action')
    parser.add_argument('--algorithm', '-a', required=False,
                        help='Choose a specific algorithm to predict sales.',
                        choices=['simple02', 'simple04', 'simpleauto',
                                 'holtlinear', 'holtexpo', 'winterlinear'],
                        type=str, dest='algorithm')
    parser.add_argument('--with_backup', '--backup', '-b', required=False,
                        help='Create a backup of the target table before '
                        'writing to it.', dest='backup', action='store_true')
    parser.add_argument('--database', '--db', '-d', required=False,
                        help='Name of the database',
                        dest='db')
    parser.add_argument('--data_format', required=True,
                        help='The type of format for the inital data',
                        choices=['monthly_progression', 'monthly_total',
                                 'daily_total'],
                        dest='dataformat')
    parser.add_argument('--forecast_range', '--forecasts', '--fcasts',
                        required=False, help='Number of months to forecasts',
                        type=int, dest='fcasts', default=12)
    parser.add_argument('--host', '-l', required=False,
                        help='The location of the database remote server or '
                             'localhost',
                        dest='host', default='localhost')
    parser.add_argument('--month', '--mon', required=False,
                        help="Month for the 'montly_progression' dataformat.",
                        type=int, dest='month')
    parser.add_argument('--origin_id', '--origin', '--ori', '--origins',
                        required=False,
                        help='Comma-separated list of origins to '
                        'predict/analyse. (-1 for all)',
                        default='-1', type=str, dest='origins')
    parser.add_argument('--output', '--out', '-o', required=False,
                        help="Write the prediction results to the specified "
                        "destination.", choices=['file', 'database'],
                        default='file', dest='output')
    parser.add_argument('--custom_file_location', '--file_location', '--file',
                        required=False, help='custom file location for the '
                        'prediction results.',
                        type=argparse.FileType('w', encoding='utf-8'),
                        dest='file_path', default=default_file_path)
    parser.add_argument('--reset', '-r', required=False,
                        help='Reset the system keyring (Db user name & '
                        'password)',
                        action='store_true', dest='reset')
    parser.add_argument('--rollback', required=False,
                        help='Rollback a certain table to a previous state',
                        type=argparse.FileType('r'), dest='rollback')
    parser.add_argument('--variation_id', '--variation', '--var',
                        '--variations', required=False,
                        help='Comma-separated list of variations to '
                        'predict/analyse. (-1 for all)',
                        default='-1', type=str, dest='variations')

    parser = parser.parse_args()

    if parser.month and parser.month not in range(1, 13):
        logger.error("Invalid 'month' parameter must be an integer between "
                     f"1-12. Got: {parser.month}.")
        sys.exit(1)
    if parser.action == 'monthly_progression' and not parser.month:
        logger.error("The 'monthly_progression' data-format requires the "
                     "'month' parameter.")
        sys.exit(1)

    if parser.rollback and not os.path.exists(parser.rollback.name):
        logger.error(f"Invalid file for rollback. [{parser.rollback}]")
        sys.exit(1)

    # When we write to the database we always create a backup first
    if parser.output == 'database' and not parser.backup:
        logger.info("Automatically activate database table backup.")
        parser.backup = True

    return parser


def setup() -> tuple:
    """
    Handle command line arguments and prepare the connection to the database.

    Return:
                            [tuple]     -   parsed CLI arguments and
                                            DB connection string
    """
    args = setup_argparser()

    db_config = configparser.ConfigParser()
    db_config.read(DB_CONFIG)
    config = configparser.ConfigParser()
    config.read(CONFIG_PATH)

    (host, db_name) = get_database(config=db_config, args=args)
    (user, password) = get_database_credentials(reset=args.reset,
                                                host=host,
                                                database=db_name)
    connection_string = str(
        f"mysql+pymysql://{user}:{password}@{host}/{db_name}")
    return (args, connection_string)


def get_database(config: configparser.ConfigParser,
                 args: argparse.ArgumentParser) -> tuple:
    if args.db:
        db_name = args.db
    elif config.has_option(section='DATABASE', option='database_name'):
        db_name = config['DATABASE']['database_name']
    else:
        logger.error("Database name required either from the database "
                     "configuration or from CLI argument.")
        sys.exit(1)

    if args.host:
        host = args.host
    elif config.has_option(section='DATABASE', option='host'):
        host = config['DATABASE']['host']
    else:
        logger.info("No host declared in the configuration or CLI "
                    "arguments, fall back to default 'localhost'")
        host = 'localhost'

    return (host, db_name)


def get_database_credentials(reset: bool, host: str, database: str) -> tuple:
    user = keyring.get_password('order_list_database_identity', 'user')
    password = keyring.get_password('order_list_database_identity', 'password')
    if not user or not password or reset:
        print(f"Login credentials for {database}@{host}:")
        user = input('Username: ')
        password = getpass.getpass()
        keyring.set_password('order_list_database_identity', 'user', user)
        keyring.set_password('order_list_database_identity', 'password',
                             password)
        return (user, password)
    return (user, password)


def create_backup_filename(tablename: str) -> tuple:
    now = datetime.datetime.now().strftime('%Y-%m-%d')
    return pathlib.Path(
        CONFIG_FOLDER, '.' + tablename + '_' + now + '_backup.db')


def parse_comma_separated_list(string: str) -> list:
    result = []
    for sub_str in string.split(','):
        try:
            result.append(int(sub_str))
        except ValueError:
            result.append(Decimal(sub_str))
        except InvalidOperation:
            logger.error(f"Invalid value: {sub_str}. The provided "
                         "argument must be a comma-separated string of integer"
                         "/decimal IDs")
            return None

    if -1 in result:
        return None
    return result


def main():
    (args, connection_string) = setup()
    variations = parse_comma_separated_list(string=args.variations)
    origins = parse_comma_separated_list(string=args.origins)
    if args.dataformat == 'daily_total':
        tablename = 'daily_statistics'
    else:
        tablename = 'statistics'

    session = PredictionSession()
    session.create_database_session(connection_string=connection_string)

    if args.backup:
        filename = create_backup_filename(tablename=tablename)
        session.backup_table(table=tablename, dest_file=filename)

    if args.rollback:
        session.rollback_table(table=tablename, source_file=args.rollback.name)

    data = DataFormat(format_type=args.dataformat,
                      session=session.session)
    df = data.get_database_entries(month=args.month, variation_id=variations,
                                   origin_id=origins)
    if len(df.index) == 0:
        logger.warning("No data entries found for variations: "
                       f"{'ALL' if variations is None else variations} and "
                       f"origins: {'ALL' if origins is None else origins}.")
        sys.exit(0)
    if variations is None:
        variations = df['variation_id'].unique().tolist()

    # For test purposes defined as constant list
    algorithms = ['simple02', 'simpleauto',
                  'holtlinear', 'winterlinear']
    forecast_algorithm = args.algorithm if args.algorithm else 'winterlinear'

    forecaster = Forecaster(session=session.session, algorithms=algorithms,
                            format_type=args.dataformat)

    if args.action == 'visual_sales':
        forecaster.visualize_sales(data=df, variations=variations)

    if args.action == 'prediction':
        fcasts = args.fcasts
        if args.dataformat == 'daily_total':
            fcasts *= 30
        result_table = pandas.DataFrame()
        for variation in variations:
            predictions = forecaster.get_predictions(
                data=df[df['variation_id'] == variation],
                fcasts=fcasts, algorithm=forecast_algorithm
            )
            result_table = result_table.append(predictions)

        if args.output == 'file':
            result_table.to_csv(args.file_path, sep=';', na_rep='',
                                index=False)

        if args.output == 'database':
            origin = Decimal('-1') if origins is None else origins[0]
            if not data.write_database_entries(data=result_table,
                                               origin_id=origin):
                logger.error("Database write failed.")

    if args.action == 'analysis':
        result_table = pandas.DataFrame()
        forecasts = {}
        best_algorithm = {}
        for variation in variations:
            tmp_data = forecaster.algorithm_compare(
                data=df[df['variation_id'] == variation],
                write_to=str(f'monthly_progress_{variation}'),
                fcasts=args.fcasts)
            if len(tmp_data['data'].index) == 0:
                continue
            forecasts[str(variation)] = tmp_data['fcasts']
            if not tmp_data['best']:
                continue
            best_algorithm[str(variation)] = tmp_data['best']
            result_table = result_table.append(tmp_data['data'])

        forecaster.create_plot(data=result_table, forecasts=forecasts,
                               best_algorithm=best_algorithm)
