import datetime
import pandas
import pytest
from decimal import Decimal

from plenty_sync.schema import (
    StockModel, IdentificationModel, OrdersModel, OrderItemsModel
)
from plenty_sync.service import (
    get_stock, retrieve_value_by_id, get_amount, DataImport
)


IDENTIFICATION_TABLE_COLUMNS = ['variation_id', 'ean', 'sku', 'asin_all',
                                'name']
STOCK_TABLE_COLUMNS = ['variation_id', 'ean', 'stock_warehouse_1',
                       'stock_warehouse_2', 'stock_warehouse_3',
                       'stock_warehouse_4', 'stock_warehouse_5',
                       'stock_warehouse_6', 'stock_warehouse_7',
                       'stock_warehouse_8', 'stock_warehouse_9']
ORDER_TABLE_COLUMNS = ['order_id', 'order_item_id', 'variation_id', 'item_id',
                       'external_id', 'name', 'origin_id', 'net_price',
                       'gross_price', 'quantity', 'creation_date',
                       'payment_date', 'delivery_date', 'b2b']


def setup_database(session, sources: dict) -> None:
    try:
        session.add_all(sources['orders'])
        session.add_all(sources['order_items'])
        session.add_all(sources['stock'])
        session.add_all(sources['ident'])
    except Exception:
        session.rollback()
    else:
        session.commit()


def add_and_commit(session, entries) -> None:
    try:
        session.add_all(entries)
    except Exception:
        session.rollback()
    else:
        session.commit()


@pytest.fixture
def test_existing_stock() -> list:
    stock = [
        # empty entry
        StockModel(variation_id=1234, item_id=123, stock_warehouse_1=0,
                   stock_warehouse_2=0),
        # stock in one warehouse
        StockModel(variation_id=2345, item_id=234, stock_warehouse_1=5,
                   stock_warehouse_2=0),
        # stock in two warehouses
        StockModel(variation_id=3456, item_id=345, stock_warehouse_1=5,
                   stock_warehouse_2=5),
        # stock in two non consecutive warehouses
        StockModel(variation_id=4567, item_id=456, stock_warehouse_1=5,
                   stock_warehouse_5=5),
    ]

    return stock


@pytest.fixture
def test_identification_table() -> list:
    table = []
    return table


@pytest.fixture
def test_existing_orders() -> list:
    orders = []
    return orders


@pytest.fixture
def test_existing_order_items() -> list:
    order_items = []
    return order_items


@pytest.fixture
def sources(test_identification_table: list, test_existing_orders: list,
            test_existing_order_items: list,
            test_existing_stock: list) -> dict:
    return {'orders': test_existing_orders,
            'order_items': test_existing_order_items,
            'stock': test_existing_stock,
            'ident': test_identification_table}


@pytest.fixture
def sample_api_variations():
    variations = [{
        'id': 1234, 'isMain': False, 'mainVariationId': 1230, 'itemId': 123,
        'categoryVariationId': 1234, 'marketVariationId': 1234,
        'clientVariationId': 1234, 'salesPriceVariationId': 1234,
        'supplierVariationId': 1234, 'warehouseVariationId': 1234,
        'position': 2, 'isActive': True, 'number': '123',
        'model': '', 'externalId': 'test_id_1', 'parentVariationId': None,
        'parentVariationQuantity': None, 'availability': 2,
        'estimatedAvailableAt': None, 'purchasePrice': 0, 'mainWarehouseId': 1,
        'name': 'test article 1', 'weightG': 1, 'weightNetG': 0,
        'widthMM': 2, 'lengthMM': 3, 'heightMM': 4,
        'propertyVariationId': '1234', 'tagVariationId': '1234',
        'stock': [{
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 1234, 'itemId': 123, 'warehouseId': 1,
            'physicalStock': 10, 'reservedStock': 2, 'netStock': 8,
            'reorderLevel': 0, 'deltaReorderLevel': -10
        }, {
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 1234, 'itemId': 123, 'warehouseId': 2,
            'physicalStock': 20, 'reservedStock': 0, 'netStock': 20,
            'reorderLevel': 0, 'deltaReorderLevel': -20
        }]
    }, {
        'id': 2345, 'isMain': False, 'mainVariationId': 2340, 'itemId': 234,
        'categoryVariationId': 2345, 'marketVariationId': 2345,
        'clientVariationId': 2345, 'salesPriceVariationId': 2345,
        'supplierVariationId': 2345, 'warehouseVariationId': 2345,
        'position': 3, 'isActive': True, 'number': '234', 'model': '',
        'externalId': 'test_id_2', 'parentVariationId': None,
        'parentVariationQuantity': None, 'availability': 2,
        'estimatedAvailableAt': None, 'purchasePrice': 0,
        'mainWarehouseId': 1, 'name': 'test article 2', 'weightG': 1,
        'weightNetG': 0, 'widthMM': 2, 'lengthMM': 3, 'heightMM': 4,
        'propertyVariationId': '2345', 'tagVariationId': '2345',
        'stock': [{
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 2345, 'itemId': 234, 'warehouseId': 1,
            'physicalStock': 20, 'reservedStock': 5, 'netStock': 15,
            'reorderLevel': 0, 'deltaReorderLevel': -20
        }, {
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 2345, 'itemId': 234, 'warehouseId': 2,
            'physicalStock': 30, 'reservedStock': 0, 'netStock': 30,
            'reorderLevel': 0, 'deltaReorderLevel': -30
        }]
    }, {
        'id': 3456, 'isMain': False, 'mainVariationId': 3450, 'itemId': 345,
        'categoryVariationId': 3456, 'marketVariationId': 3456,
        'clientVariationId': 3456, 'salesPriceVariationId': 3456,
        'supplierVariationId': 3456, 'warehouseVariationId': 3456,
        'position': 4, 'isActive': True, 'number': '345', 'model': '',
        'externalId': 'test_id_3', 'parentVariationId': None,
        'parentVariationQuantity': None, 'availability': 2,
        'estimatedAvailableAt': None, 'purchasePrice': 0,
        'mainWarehouseId': 1, 'name': 'test article 3', 'weightG': 1,
        'weightNetG': 0, 'widthMM': 2, 'lengthMM': 3, 'heightMM': 4,
        'propertyVariationId': '3456', 'tagVariationId': '3456',
        'stock': [{
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 3456, 'itemId': 345, 'warehouseId': 1,
            'physicalStock': 30, 'reservedStock': 10, 'netStock': 20,
            'reorderLevel': 0, 'deltaReorderLevel': -30
        }, {
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 3456, 'itemId': 345, 'warehouseId': 3,
            'physicalStock': 40, 'reservedStock': 0, 'netStock': 40,
            'reorderLevel': 0, 'deltaReorderLevel': -40
        }]
    }]
    return variations


@pytest.fixture
def sample_warehouse_config():
    config = {
        'warehouse_1': 1,
        'warehouse_2': 2,
        'warehouse_3': 3
    }
    return config


@pytest.fixture
def sample_order_items():
    """ Minimal representation of order items from an API response """
    variations = [
        {
            'amounts': [
                {
                    'currency': 'EUR',
                    'priceGross': 24.109,
                    'priceNet': 23.109
                },
                {
                    'currency': 'GBP',
                    'priceGross': 20.92,
                    'priceNet': 19.92
                }
            ],
            'id': 12345,
            'itemVariationId': 1234,
            'orderId': 5678,
            'quantity': 1,
            'referrerId': 4.02,
            'typeId': 1,
            'variation': {
                'itemId': 1234567,
                'name': 'test_name',
            }
        },
        {
            'amounts': [
                {
                    'currency': 'GBP',
                    'priceGross': 19.92,
                    'priceNet': 19.92
                }
            ],
            'id': 12345,
            'itemVariationId': 1234,
            'orderId': 5678,
            'quantity': 1,
            'referrerId': 4.02,
            'typeId': 1,
            'variation': {
                'itemId': 1234567,
                'name': 'test_name',
            }
        }
    ]
    return variations


def describe_get_stock():
    def with_no_config(sample_api_variations: list) -> None:
        for variation in sample_api_variations:
            result = get_stock(variation=variation, warehouses=None)

            assert [] == result

    def with_no_variations(sample_warehouse_config: dict) -> None:
        result = get_stock(variation={}, warehouses=sample_warehouse_config)

        assert [] == result

    def with_variations_and_config(sample_api_variations: list,
                                   sample_warehouse_config: dict) -> None:
        expected = [
            [10, 20, 0],
            [20, 30, 0],
            [30, 0, 40]
        ]
        for i, variation in enumerate(sample_api_variations):
            result = get_stock(variation=variation,
                               warehouses=sample_warehouse_config)

            assert expected[i] == result


def describe_retrieve_value_by_id():
    def with_invalid_section() -> None:
        sample = {'section1': 1, 'section2': 'abc'}
        with pytest.raises(KeyError):
            retrieve_value_by_id(json=sample, id_value=1,
                                 section='section3', match_field='test',
                                 target_field='value')

    def with_non_numeric_id_field() -> None:
        sample = {
            'section1': [
                {'id_field': 'abc', 'value': 'test'}
            ]
        }

        assert '' == retrieve_value_by_id(json=sample, id_value=1,
                                          section='section1',
                                          match_field='id_field',
                                          target_field='value')

    def with_no_matching_id() -> None:
        sample = {
            'section1': [
                {'id_field': 1, 'value': 'test'},
                {'id_field': 2, 'value': 'test2'},
            ]
        }

        assert '' == retrieve_value_by_id(json=sample, id_value=3,
                                          section='section1',
                                          match_field='id_field',
                                          target_field='value')

    def with_valid_field_and_id() -> None:
        sample = {
            'section1': [
                {'id_field': 1, 'value': 'test'},
                {'id_field': 2, 'value': 'test2'},
            ]
        }

        assert 'test' == retrieve_value_by_id(json=sample, id_value=1,
                                              section='section1',
                                              match_field='id_field',
                                              target_field='value')

    def with_numeric_value() -> None:
        sample = {
            'section1': [
                {'id_field': 1, 'value': 12},
                {'id_field': 2, 'value': 'test2'},
            ]
        }

        assert '12' == retrieve_value_by_id(json=sample, id_value=1,
                                            section='section1',
                                            match_field='id_field',
                                            target_field='value')


def describe_get_amount():
    def with_invalid_type(sample_order_items: list) -> None:
        invalid_types = ['netto', 'abc', '']

        for type in invalid_types:
            with pytest.raises(KeyError):
                get_amount(json=sample_order_items[0], type=type)

    def with_valid_type_without_euro_price(sample_order_items: list) -> None:
        assert Decimal(0) == get_amount(json=sample_order_items[1], type='net')

    def with_valid_type_and_euro_price(sample_order_items: list) -> None:
        assert '23.11' == str(get_amount(json=sample_order_items[0],
                                         type='net'))
        assert '24.11' == str(get_amount(json=sample_order_items[0],
                                         type='gross'))


def assert_identification_table(session, expectation) -> None:
    """ Helper function for sync_identification_data tests """
    for index, row in enumerate(session.query(IdentificationModel).all()):
        expected_values = expectation[index]
        assert expected_values.variation_id == row.variation_id
        assert expected_values.ean == row.ean
        assert expected_values.sku == row.sku
        assert expected_values.asin_all == row.asin_all
        assert expected_values.name == row.name


def describe_sync_identification_data():
    def with_same_entries(db_session) -> None:
        dataframe = pandas.DataFrame(
            [[1234, '1234567891010', 'test_sku_0', 'TESTASIN00', 'name0']],
            columns=IDENTIFICATION_TABLE_COLUMNS)

        db_entry = IdentificationModel(
            variation_id=1234, ean='1234567891010', sku='test_sku_0',
            asin_all='TESTASIN00', name='name0'
        )
        add_and_commit(session=db_session, entries=[db_entry])
        expected_table = [db_entry]

        importer = DataImport(session=db_session)
        importer.sync_identification_data(dataframe=dataframe)

        assert_identification_table(session=db_session,
                                    expectation=expected_table)

    def with_matching_var_id(db_session) -> None:
        dataframe = pandas.DataFrame(
            [[1235, '1234567891011', 'test_sku_1', 'TESTASIN01', 'name1+']],
            columns=IDENTIFICATION_TABLE_COLUMNS)

        add_and_commit(session=db_session, entries=[
            IdentificationModel(
                variation_id=1235, ean='1234567891011', sku='test_sku_1',
                asin_all='TESTASIN01', name='name1'
            )
        ])

        expected_table = [
            IdentificationModel(
                variation_id=1235, ean='1234567891011', sku='test_sku_1',
                asin_all='TESTASIN01', name='name1+'
            )
        ]

        importer = DataImport(session=db_session)
        importer.sync_identification_data(dataframe=dataframe)

        assert_identification_table(session=db_session,
                                    expectation=expected_table)

    def with_matching_sku_and_ean(db_session) -> None:
        dataframe = pandas.DataFrame(
            [[2345, '1234567891012', 'test_sku_2', 'TESTASIN02', 'name2']],
            columns=IDENTIFICATION_TABLE_COLUMNS)

        add_and_commit(session=db_session, entries=[
            IdentificationModel(
                variation_id=1236, ean='1234567891012', sku='test_sku_2',
                asin_all='TESTASIN02', name='name2'
            )
        ])

        expected_table = [
            IdentificationModel(
                variation_id=2345, ean='1234567891012', sku='test_sku_2',
                asin_all='TESTASIN02', name='name2'
            )
        ]

        importer = DataImport(session=db_session)
        importer.sync_identification_data(dataframe=dataframe)

        assert_identification_table(session=db_session,
                                    expectation=expected_table)

    def with_missing_export_entry(db_session) -> None:
        dataframe = pandas.DataFrame(
            [[1237, '1234567891013', 'test_sku_3', 'TESTASIN03', 'name3']],
            columns=IDENTIFICATION_TABLE_COLUMNS)

        add_and_commit(session=db_session, entries=[
            IdentificationModel(
                variation_id=1237, ean='1234567891013', sku='test_sku_3',
                asin_all='TESTASIN03', name='name3'
            ),
            IdentificationModel(
                variation_id=2346, ean='1234567891015', sku='test_sku_5',
                asin_all='TESTASIN05', name='name5'
            )
        ])

        expected_table = [
            IdentificationModel(
                variation_id=1237, ean='1234567891013', sku='test_sku_3',
                asin_all='TESTASIN03', name='name3'
            )
        ]

        importer = DataImport(session=db_session)
        importer.sync_identification_data(dataframe=dataframe)

        assert_identification_table(session=db_session,
                                    expectation=expected_table)

    def with_missing_db_entry(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1238, '1234567891014', 'test_sku_4', 'TESTASIN04', 'name4'],
                [2346, '1234567891015', 'test_sku_5', 'TESTASIN05', 'name5']
            ],
            columns=IDENTIFICATION_TABLE_COLUMNS)

        add_and_commit(session=db_session, entries=[
            IdentificationModel(
                variation_id=1238, ean='1234567891014', sku='test_sku_4',
                asin_all='TESTASIN04', name='name4'
            )
        ])

        expected_table = [
            IdentificationModel(
                variation_id=1238, ean='1234567891014', sku='test_sku_4',
                asin_all='TESTASIN04', name='name4'
            ),
            IdentificationModel(
                variation_id=2346, ean='1234567891015', sku='test_sku_5',
                asin_all='TESTASIN05', name='name5'
            )
        ]

        importer = DataImport(session=db_session)
        importer.sync_identification_data(dataframe=dataframe)

        assert_identification_table(session=db_session,
                                    expectation=expected_table)


def assert_stock_table(session, expectation) -> None:
    assert session.query(StockModel).count() == len(expectation)
    for index, row in enumerate(session.query(StockModel).all()):
        expected_values = expectation[index]
        assert expected_values.variation_id == row.variation_id
        assert expected_values.ean == row.ean
        for num in range(1, 10):
            expect = getattr(expected_values, f'stock_warehouse_{num}')
            actual = getattr(row, f'stock_warehouse_{num}')
            assert expect == actual


def describe_add_stock():
    def with_no_changes(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1234, '1234567891010', 0, 5, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        add_and_commit(session=db_session, entries=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_1=0,
                stock_warehouse_2=5, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        importer = DataImport(session=db_session)
        importer.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)

    def with_adjusted_stock(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1234, '1234567891010', 0, 7, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        add_and_commit(session=db_session, entries=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_1=0,
                stock_warehouse_2=7, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        importer = DataImport(session=db_session)
        importer.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)

    def with_new_location(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1234, '1234567891010', 5, 5, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        add_and_commit(session=db_session, entries=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_1=5,
                stock_warehouse_2=5, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        importer = DataImport(session=db_session)
        importer.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)

    def with_removed_variation(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1234, '1234567891010', 0, 5, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        add_and_commit(session=db_session, entries=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            ),
            StockModel(
                variation_id=2345, ean='1234567891011', stock_warehouse_1=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_1=0,
                stock_warehouse_2=5, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        importer = DataImport(session=db_session)
        importer.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)

    def with_added_variation(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1234, '1234567891010', 0, 5, 0, 0, 0, 0, 0, 0, 0],
                [2345, '1234567891011', 5, 0, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        add_and_commit(session=db_session, entries=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_1=0,
                stock_warehouse_2=5, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            ),
            StockModel(
                variation_id=2345, ean='1234567891011', stock_warehouse_1=5,
                stock_warehouse_2=0, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        importer = DataImport(session=db_session)
        importer.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)

    def with_changed_variation_id(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [2345, '1234567891010', 0, 10, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        add_and_commit(session=db_session, entries=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=2345, ean='1234567891010', stock_warehouse_1=0,
                stock_warehouse_2=10, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        importer = DataImport(session=db_session)
        importer.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)


def assert_order_table(session, expectation) -> None:
    assert len(expectation) == session.query(OrdersModel).count()
    order_attributes = ['origin_id', 'netto_total', 'gross_total',
                        'creation_date', 'payment_date', 'delivery_date',
                        'b2b', 'external_id']
    order_item_attributes = ['external_id', 'origin_id', 'variation_id', 'ean',
                             'item_id', 'name', 'quantity', 'netto', 'gross',
                             'statistic_entry', 'daily_statistic_entry']
    assert session.query(OrdersModel).count() == len(expectation)
    for index, row in enumerate(session.query(OrdersModel).all()):
        expected_values = expectation[index]
        for o_attr in order_attributes:
            expect = getattr(expected_values, o_attr)
            actual = getattr(row, o_attr)
            assert expect == actual
        for expect_item, actual_item in zip(expected_values.order_items,
                                            row.order_items):
            for oi_attr in order_item_attributes:
                expect = getattr(expect_item, oi_attr)
                actual = getattr(actual_item, oi_attr)
                assert expect == actual


def setup_id_table(func):
    def wrapper(db_session):
        add_and_commit(session=db_session, entries=[
            IdentificationModel(
                variation_id=1234, ean='1234567891011', sku='test_sku_1',
                asin_all='TESTASIN01', name='test_name1'
            ),
            IdentificationModel(
                variation_id=2345, ean='1234567891012', sku='test_sku_2',
                asin_all='TESTASIN02', name='test_name2'
            )
        ])
        func(db_session=db_session)
    return wrapper


def describe_handle_order():
    @setup_id_table
    def with_new_order_and_one_item(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [12345, 98765, 1234, 101, 'externid', 'test_name_1',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        expected_table = [
            OrdersModel(
                id=12345, origin_id=Decimal("1.0"),
                netto_total=Decimal("15.0"), gross_total=Decimal("18.0"),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=False,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal("1.0"),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal("15.0"), gross=Decimal("18.0"),
                        statistic_entry=-1, daily_statistic_entry=-1
                    )
                ]
            )
        ]

        importer = DataImport(session=db_session)
        importer.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)

    @setup_id_table
    def with_new_order_and_two_item(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [12345, 98765, 1234, 101, 'externid', 'test_name_1',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False],
                [12345, 98766, 2345, 101, 'externid', 'test_name_2',
                 1.0, 15.0, 18.0, 2, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        expected_table = [
            OrdersModel(
                id=12345, origin_id=Decimal("1.0"),
                netto_total=Decimal("45.0"), gross_total=Decimal("54.0"),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=False,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal("1.0"),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal("15.0"), gross=Decimal("18.0"),
                        statistic_entry=-1, daily_statistic_entry=-1
                    ),
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal("1.0"),
                        variation_id=2345, ean='1234567891012', item_id=101,
                        name='test_name_2', quantity=2, netto=Decimal("15.0"),
                        gross=Decimal("18.0"), statistic_entry=-1,
                        daily_statistic_entry=-1
                    )
                ]
            )
        ]

        importer = DataImport(session=db_session)
        importer.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)

    @setup_id_table
    def with_two_new_orders(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [12345, 98765, 1234, 101, 'externid', 'test_name_1',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False],
                [23456, 98766, 2345, 101, 'externid2', 'test_name_2',
                 4.01, 15.0, 18.0, 2, '2021-05-03T19:00:00+02:00',
                 '2021-05-06T06:00:00+02:00', '2021-05-06T09:00:00+02:00',
                 False]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        expected_table = [
            OrdersModel(
                id=12345, origin_id=Decimal("1.0"),
                netto_total=Decimal("15.0"), gross_total=Decimal("18.0"),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=False,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal("1.0"),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal("15.0"), gross=Decimal("18.0"),
                        statistic_entry=-1, daily_statistic_entry=-1
                    )
                ]
            ),
            OrdersModel(
                id=23456, origin_id=Decimal("4.01"),
                netto_total=Decimal("30.0"), gross_total=Decimal("36.0"),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 6),
                delivery_date=datetime.date(2021, 5, 6), b2b=False,
                external_id='externid2', order_items=[
                    OrderItemsModel(
                        external_id='externid2', origin_id=Decimal("4.01"),
                        variation_id=2345, ean='1234567891012', item_id=101,
                        name='test_name_2', quantity=2, netto=Decimal("15.0"),
                        gross=Decimal("18.0"), statistic_entry=-1,
                        daily_statistic_entry=-1
                    )
                ]
            )
        ]

        importer = DataImport(session=db_session)
        importer.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)

    @setup_id_table
    def with_new_b2b_order(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [12345, 98765, 1234, 101, 'externid', 'test_name_1',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 True]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        expected_table = [
            OrdersModel(
                id=12345, origin_id=Decimal("1.0"),
                netto_total=Decimal("15.0"), gross_total=Decimal("18.0"),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=True,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal("1.0"),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal("15.0"), gross=Decimal("18.0"),
                        statistic_entry=-1, daily_statistic_entry=-1
                    )
                ]
            )
        ]

        importer = DataImport(session=db_session)
        importer.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)

    @setup_id_table
    def with_existing_order(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [12345, 98765, 1234, 101, 'externid', 'test_name_1',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        add_and_commit(session=db_session, entries=[
            OrdersModel(
                id=12345, origin_id=Decimal("1.0"),
                netto_total=Decimal("15.0"), gross_total=Decimal("18.0"),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=False,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal("1.0"),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal("15.0"), gross=Decimal("18.0"),
                        statistic_entry=-1, daily_statistic_entry=-1
                    )
                ]
            )
        ])
        expected_table = [
            OrdersModel(
                id=12345, origin_id=Decimal("1.0"),
                netto_total=Decimal("15.0"), gross_total=Decimal("18.0"),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=False,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal("1.0"),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal("15.0"), gross=Decimal("18.0"),
                        statistic_entry=-1, daily_statistic_entry=-1
                    )
                ]
            )
        ]

        importer = DataImport(session=db_session)
        importer.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)
