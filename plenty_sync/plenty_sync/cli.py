import os
import sys
import argparse
import configparser
import getpass
import datetime
import dateutil.parser as date_parser
from dateutil.parser._parser import ParserError
import keyring

from loguru import logger

from plenty_sync import service


class InvalidDate(Exception):
    def __init__(self, date_type: str, date_str: str):
        if date_type not in ['start', 'end']:
            raise RuntimeError("date type for InvalidDate Exception must be "
                               f"'start' or 'end'. Got: {date_type}")
        self.date_type = date_type
        self.date_str = date_str
        super().__init__()

    def __str__(self):
        return f"Invalid {self.date_type} date [{self.date_str}]"


PROG_NAME = 'plenty_sync'
USER = os.getlogin()
if sys.platform == 'linux':
    BASE_PATH = os.path.join(
        '/', 'home', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )
elif sys.platform == 'win32':
    BASE_PATH = os.path.join(
        'C:\\', 'Users', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )

DB_CONFIG = os.path.join(BASE_PATH, 'db_config.ini')
CONFIG_FOLDER = os.path.join(BASE_PATH, PROG_NAME)
if not os.path.exists(CONFIG_FOLDER):
    os.mkdir(CONFIG_FOLDER)

CONFIG_PATH = os.path.join(CONFIG_FOLDER, 'config.ini')

if not os.path.exists(DB_CONFIG):
    open(DB_CONFIG, 'a').close()

if not os.path.exists(CONFIG_PATH):
    open(CONFIG_PATH, 'a').close()


def setup_argparser():
    """ Return: [Argparse Object] """
    parser = argparse.ArgumentParser(prog='')
    parser.add_argument('--verbose', '-v', required=False,
                        help='Add messages to explain what the program does',
                        action='store_true', dest='verbose')
    parser.add_argument('--reset-credentials', '-r', '--reset', required=False,
                        help="Reset the credentials within your system's"
                             " keyring",
                        action='store_true', dest='reset')
    parser.add_argument('-s', '--start', '--from', '-f', required=False,
                        help='start date for the time range to fetch data',
                        dest='start')
    parser.add_argument('-e', '--end', '--to', '-t', required=False,
                        help='end date for the time range to fetch data',
                        dest='end')
    parser.add_argument('--stock', required=False,
                        help='Synchronize the stock from Plentymarkets to'
                        ' the database.', action='store_true', dest='stock')
    parser.add_argument('--order', required=False,
                        help='Synchronize the order from Plentymarkets to'
                        ' the database.', action='store_true', dest='order')
    parser.add_argument('--identity', required=False,
                        help='Import identification information for variations'
                        '(EAN, ASIN)', action='store_true', dest='identity')
    parser.add_argument('--create_tables', required=False,
                        help='Create the initial database tables,'
                        'skip creation if exists',
                        action='store_true', dest='create_tables')
    parser.add_argument('--host', '-l', required=False,
                        help='The location of the database remote server or '
                             'localhost',
                        dest='host', default='localhost')
    parser.add_argument('--database', '--db', '-d', required=False,
                        help='Name of the database',
                        dest='db')

    namespace = parser.parse_args()
    return namespace


def get_time_range(parser: argparse.Namespace) -> tuple:
    if parser.start:
        try:
            date_parser.parse(parser.start)
        except ParserError as err:
            raise InvalidDate(date_type='start',
                              date_str=parser.start) from err
    if parser.end:
        try:
            end_date = date_parser.parse(parser.end)
        except ParserError as err:
            raise InvalidDate(date_type='end', date_str=parser.end) from err

    if parser.start and parser.end:
        return (parser.start, parser.end)

    date_format = "%Y-%m-%dT%H:%M:%S"
    now = datetime.datetime.now()
    now_str = now.strftime(date_format)
    if parser.start and not parser.end:
        return (parser.start, now_str)

    if not parser.start and parser.end:
        start_date = end_date - datetime.timedelta(days=1)
        return (start_date.strftime(date_format), parser.end)

    if not parser.start and not parser.end:
        start_date = now - datetime.timedelta(hours=12)
        return (start_date.strftime(date_format), now.strftime(date_format))


def get_collect_options(parser: argparse.Namespace) -> dict:
    """
    Set up the additional elements to be pulled from the REST API.

    In order to reduce API calls, get a response that is sufficient for as
    many modes(stock, identity, order) as possible. Reuse that response for
    all appropriate modes.

    Parameters:
        parser          [Namespace] -   parser arguments from the CLI

    Return:
                        [dict]      -   Mapping of REST API request route to
                                        additional element list
    """
    options = {'order': [], 'variation': ['variationBarcodes']}
    if parser.order:
        options['order'] += ['orderItems.variation', 'contactReceiver']
    if parser.stock:
        options['variation'] += ['stock']
    if parser.identity:
        options['variation'] += ['marketItemNumbers']
    return options


def setup() -> tuple:
    """
    Handle command line arguments and prepare the connection to the database.

    Return:
                            [tuple]     -   parsed CLI arguments,
                                            DB connection string and
                                            config
    """
    args = setup_argparser()

    db_config = configparser.ConfigParser()
    db_config.read(DB_CONFIG)
    config = configparser.ConfigParser()
    config.read(CONFIG_PATH)

    (host, db_name) = get_database(config=db_config, args=args)
    (user, password) = get_database_credentials(reset=args.reset,
                                                host=host,
                                                database=db_name)
    connection_string = str(
        f"mysql+pymysql://{user}:{password}@{host}/{db_name}")
    return (args, connection_string, config)


def get_database(config: configparser.ConfigParser,
                 args: argparse.ArgumentParser) -> tuple:
    if args.db:
        db_name = args.db
    elif config.has_option(section='DATABASE', option='database_name'):
        db_name = config['DATABASE']['database_name']
    else:
        logger.error("Database name required either from the database "
                     "configuration or from CLI argument.")
        sys.exit(1)

    if args.host:
        host = args.host
    elif config.has_option(section='DATABASE', option='host'):
        host = config['DATABASE']['host']
    else:
        logger.info("No host declared in the configuration or CLI "
                    "arguments, fall back to default 'localhost'")
        host = 'localhost'

    return (host, db_name)


def get_database_credentials(reset: bool, host: str, database: str) -> tuple:
    user = keyring.get_password('order_list_database_identity', 'user')
    password = keyring.get_password('order_list_database_identity', 'password')
    if not user or not password or reset:
        print(f"Login credentials for {database}@{host}:")
        user = input('Username: ')
        password = getpass.getpass()
        keyring.set_password('order_list_database_identity', 'user', user)
        keyring.set_password('order_list_database_identity', 'password',
                             password)
        return (user, password)
    return (user, password)


def get_warehouses(config: configparser.ConfigParser) -> dict:
    mapping: dict = {}
    warehouse_id: int = 0
    for warehouse in config['PLENTY']['warehouses'].split(';'):
        if not warehouse.find(':'):
            logger.warning(f"Invalid warehouse entry {warehouse}.\n"
                           "Required format: {database column}:{warehouse id},"
                           " separated by ';'")
            continue
        entry = warehouse.split(':')
        db_column = entry[0]
        try:
            warehouse_id = int(entry[1])
        except ValueError:
            logger.warning(f"Invalid warehouse ID: {entry[1]}")
            continue
        mapping[db_column] = warehouse_id

    return mapping


def cli():
    (args, connection_string, config) = setup()

    collector = service.DataCollector(
        api_base_url=config['PLENTY']['base_url'],
        config=config,
        collect_options=get_collect_options(parser=args))
    importer = service.DataImport()
    importer.create_database_session(connection_string=connection_string)

    if args.create_tables:
        importer.create_tables()

    if args.identity:
        dataframe = collector.pull_identification_data()
        importer.sync_identification_data(dataframe=dataframe)

    if args.order:
        dataframe = collector.pull_purchase_orders(
            date_range=get_time_range(parser=args))
        importer.handle_order(dataframe=dataframe)

    if args.stock:
        warehouses = get_warehouses(config=config)

        dataframe = collector.pull_stock_data(warehouse_config=warehouses)

        importer.add_stock(dataframe=dataframe)
