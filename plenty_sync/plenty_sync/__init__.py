"""**plenty_sync**

Synchronize data stored on a MariaDB relational database with data from the PlentyMarkets API.
"""
__version__ = "0.0.1"
