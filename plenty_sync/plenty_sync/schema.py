"""
Synchronize Plentymarkets orders, stock etc. with the MariaDB database.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import generic_repr
from sqlalchemy import (
    Column, Integer, Text, String, Date, Numeric, Boolean, create_engine,
    ForeignKey
)
from loguru import logger


Base = declarative_base()


@generic_repr
class OrdersModel(Base):
    __tablename__ = "orders"

    id = Column(Integer, primary_key=True, nullable=False)
    origin_id = Column(Numeric(precision=8, scale=2), nullable=False)
    netto_total = Column(Numeric(precision=8, scale=2), nullable=False)
    gross_total = Column(Numeric(precision=8, scale=2), nullable=False)
    creation_date = Column(Date, nullable=False)
    payment_date = Column(Date)
    delivery_date = Column(Date)
    b2b = Column(Boolean, nullable=False)
    external_id = Column(Text)
    order_items = relationship("OrderItemsModel", backref="orders")


@generic_repr
class OrderItemsModel(Base):
    __tablename__ = "order_items"

    id = Column(Integer, primary_key=True, nullable=False)
    order_id = Column(Integer, ForeignKey(OrdersModel.id), nullable=False)
    external_id = Column(Text)
    origin_id = Column(Numeric(precision=8, scale=2), nullable=False)
    variation_id = Column(Integer, nullable=False)
    ean = Column(String(13))
    item_id = Column(Integer, nullable=False)
    name = Column(Text)
    quantity = Column(Integer, nullable=False)
    netto = Column(Numeric(precision=8, scale=2), nullable=False)
    gross = Column(Numeric(precision=8, scale=2), nullable=False)
    statistic_entry = Column(Integer, nullable=True, default=-1)
    daily_statistic_entry = Column(Integer, nullable=True, default=-1)
    order = relationship("OrdersModel",
                         foreign_keys="OrderItemsModel.order_id",
                         lazy="joined")


@generic_repr
class StockModel(Base):
    __tablename__ = "current_stock"

    variation_id = Column(Integer, primary_key=True, autoincrement=False)
    ean = Column(String(13))
    stock_warehouse_1 = Column(Integer, default=0)
    stock_warehouse_2 = Column(Integer, default=0)
    stock_warehouse_3 = Column(Integer, default=0)
    stock_warehouse_4 = Column(Integer, default=0)
    stock_warehouse_5 = Column(Integer, default=0)
    stock_warehouse_6 = Column(Integer, default=0)
    stock_warehouse_7 = Column(Integer, default=0)
    stock_warehouse_8 = Column(Integer, default=0)
    stock_warehouse_9 = Column(Integer, default=0)

    def update_by_dataframe(self, dataframe):
        """
        Get a matching row from a dataframe and update the database entry.
        """
        row = dataframe[dataframe['ean'] == self.ean].to_dict(orient='records')
        if len(row) > 1:
            logger.warning(f"[{self.__tablename__}] Multiple rows match ean "
                           f"{self.ean}.\nVariation ID: "
                           f"{[x['variation_id'] for x in row]}")
        try:
            for key, value in row[0].items():
                setattr(self, key, value)
        except IndexError:
            logger.warning(f"[{self.__tablename__}] No row matches ean "
                           f"{self.ean}")


@generic_repr
class IdentificationModel(Base):
    __tablename__ = "identification"

    variation_id = Column(Integer, primary_key=True, autoincrement=False)
    ean = Column(String(13))
    sku = Column(Text(50))
    asin_all = Column(String(10))
    name = Column(Text(300))

    def update_by_dataframe(self, dataframe):
        """
        Get a matching row from a dataframe and update the database entry.
        """
        row = dataframe[dataframe['ean'] == self.ean].to_dict(orient='records')
        if len(row) > 1:
            logger.warning(f"[{self.__tablename__}] Multiple rows match ean "
                           f"{self.ean}.\nVariation ID: "
                           f"{[x['variation_id'] for x in row]}")
        try:
            for key, value in row[0].items():
                setattr(self, key, value)
        except IndexError:
            logger.warning(f"[{self.__tablename__}] No row matches ean "
                           f"{self.ean}")


class DataAccessLayer:
    def __init__(self):
        self.connection = ''
        self.engine = None
        self.Session = None
        self.session = None

    def connect(self):
        self.engine = create_engine(self.connection)
        Base.metadata.bind = self.engine
        Base.metadata.create_all(self.engine)
        self.session = sessionmaker(bind=self.engine)


dal = DataAccessLayer()
