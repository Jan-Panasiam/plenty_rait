"""
Synchronize Plentymarkets orders, stock etc. with the MariaDB database.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import importlib
import datetime
from decimal import Decimal
import pandas
from sqlalchemy.orm import scoped_session, sessionmaker
import plenty_api
from loguru import logger

from plenty_sync.schema import (
    OrdersModel, OrderItemsModel, StockModel, IdentificationModel, dal
)



ORDER_TYPE_SALES_ORDER = 1
ORDER_ITEM_TYPE_VARIATION = 1


def retrieve_value_by_id(json: dict, id_value: int, section: str,
                         match_field: str, target_field: str) -> str:
    """
    Retrieve a specific value matching to a ID from a JSON response.

    Parameters:
        json            [dict]  -   Response from Plentymarkets API call
        id_value        [int]   -   ID to match in the elements of the given
                                    section
        section         [str]   -   Section within the JSON to search at
        match_field     [str]   -   Field within the section where the
                                    value has to match the @id_value
        target_field    [str]   -   Section to take the return value from, if
                                    the id matches

    Return:
                        [str]   -   The found value represented as a string
    """
    if section not in json:
        raise KeyError(f"Invalid JSON section {section}")

    if not isinstance(id_value, int):
        try:
            id_value = int(id_value)
        except ValueError:
            logger.warning(f"Invalid ID value: {id_value} must be an integer.")
            return ''

    for element in json[section]:
        if element[match_field] == id_value:
            return str(element[target_field])

    return ''


def get_ean(json: dict, ean_id: int) -> str:
    """
    Retrieve the correct barcode from the JSON response

    Parameters:
        json            [dict]  -   Response from Plentymarkets API call
        ean_id          [int]   -   ID of the barcode assigned by
                                        Plentymarkets.
                                        (Lookup: Setup -> Article -> Barcodes)

    Return:
                        [str]   -   The EAN barcode as a string
    """
    return retrieve_value_by_id(json=json, id_value=ean_id,
                                section='variationBarcodes',
                                match_field='barcodeId',
                                target_field='code')


def get_asin(json: dict, asin_id: int) -> str:
    """
    Retrieve the correct ASIN code from the JSON response.

    Parameters:
        json            [dict]  -   Response from Plentymarkets API call
        asin_id         [int]   -   ID of the country connected to the ASIN

    Return:
                        [str]   -   The ASIN code as a string
    """
    return retrieve_value_by_id(json=json, id_value=asin_id,
                                section='marketItemNumbers',
                                match_field='countryId',
                                target_field='value')


def get_stock(variation: dict, warehouses: dict) -> list:
    """
    Retrieve the stock of a variation from a specific set of warehouses.

    Parameters:
        variation       [dict]  -   Part of the plentymarkets API response
        warehouse       [dict]  -   set of warehouses from the config

    Return:
                        [list]  -   Stock values in the order of the warehouses
    """
    if not warehouses or not variation:
        return []

    stock_map: dict = {}
    for stock in variation['stock']:
        stock_id = int(stock['warehouseId'])
        if stock_id not in list(warehouses.values()):
            continue
        stock_map[str(stock_id)] = stock['physicalStock']

    # Sort the values into the correct order
    return_values: list = []
    for warehouse_id in warehouses.values():
        if str(warehouse_id) not in list(stock_map.keys()):
            return_values.append(0)
            continue
        return_values.append(int(stock_map[str(warehouse_id)]))

    return return_values


def get_date_by_type(json: dict, type: str) -> str:
    """
    Get a specific date from the list of dates associated with an order.

    Parameters:
        json            [dict]  -   Part of the plentymarkets API response
        type            [str]   -   type name
                                    (see valid values below)

    Return:
                        [str]  -   date string in the W3C date format
    """
    valid_values = {
        'deleted': 1,
        'created': 2,
        'paid': 3,
        'last_update': 4,
        'completed': 5,
        'returned': 6,
        'payment_due_to': 7,
        'estimated_shipping': 8,
        'started': 9,
        'ended': 10,
        'estimated_delivery': 11,
        'purchased': 16,
        'finished': 17
    }
    if type not in valid_values.keys():
        raise KeyError(f"Invalid date type {type}")
    return retrieve_value_by_id(json=json, id_value=valid_values[type],
                                section='dates', match_field='typeId',
                                target_field='date')


def get_property_by_type(json: dict, type: str) -> str:
    """
    Get a specific property value from the order properties by its type.

    Parameters:
        json            [dict]  -   Part of the plentymarkets API response
        type            [str]   -   type name
                                    (see valid values below)

    Return:
                        [str]  -    external order number from the marketplace
    """
    valid_values = {
        'warehouse': 1,
        'shipping_profile': 2,
        'payment_method': 3,
        'payment_status': 4,
        'external_shipping_profile': 5,
        'document_language': 6,
        'external_order_id': 7,
        'customer_sign': 8,
        'dunning_level': 9,
        'seller_account': 10,
        'weight': 11,
        'width': 12,
        'length': 13,
        'height': 14,
        'flag': 15,
        'external_token_id': 16,
        'external_item_id': 17,
        'coupon_code': 18,
        'coupon_type': 19,
        'sales_tax_id_number': 34,
        'main_document_number': 33,
        'payment_transaction_id': 45,
        'external_tax_service': 47,
        'merchant_id': 60,
        'report_id': 61,
        'preferred_storage_location_id': 63,
        'amazon_shipping_label': 64,
        'ebay_plus': 994,
        'fulfillment_service': 995
    }
    if type not in valid_values.keys():
        raise KeyError(f"Invalid property type {type}")
    return retrieve_value_by_id(json=json, id_value=valid_values[type],
                                section='properties', match_field='typeId',
                                target_field='value')


def get_amount(json: dict, type: str) -> Decimal:
    """
    Get either the net or gross price from an order item.

    Parameters:
        json            [dict]  -   Order item within a order REST API response
        type            [str]   -   type name
                                    (either 'net' or 'gross')

    Return:
                        [Decimal]-  Price in Euro with 2 decimal places
    """
    valid_values = {'net': 'priceNet', 'gross': 'priceGross'}
    if type not in valid_values.keys():
        raise KeyError(f"Invalid price amount type {type}")

    for amount in json['amounts']:
        if amount['currency'] != 'EUR':
            continue
        return Decimal(amount[valid_values[type]]).quantize(Decimal('0.01'))

    return Decimal(0)


def is_b2b_customer(json: dict, b2b_class_id: int) -> bool:
    """
    Check if the receiver contact is a buisness to business customer.

    Parameters:
        json            [dict]  -   Part of the plentymarkets API response
        b2b_class_id    [int]   -   User-configured ID of the customer class
                                    associated with b2b customers

    Return:
                        [bool]
    """
    try:
        return json['contactReceiver']['classId'] == b2b_class_id
    except TypeError:
        return False


def transform_to_dateobject(date_str: str) -> datetime.date:
    """
    Get a date object from a given W3C date string.

    Parameter:
        date_str        [str]   -   Date as string from `pull_purchase_orders`

    Return:
              [datetime.date]
    """
    if not date_str or len(date_str) != 25:
        return None

    return datetime.date.fromisoformat(date_str[:-15])


class DataImport:
    def __init__(self, session=None):
        self.session = session

    def create_database_session(self, connection_string: str) -> None:
        dal.connection = connection_string
        dal.connect()

        dal.Session = scoped_session(sessionmaker(bind=dal.engine))
        dal.session = dal.Session()
        dal.Session.registry.clear()

        self.session = dal.session

    def create_tables(self) -> None:
        for table in ['amazon_orders', 'amazon_order_items']:
            if not dal.engine.dialect.has_table(dal.engine, table):
                table_model = importlib.import_module(
                    'amazon_import.schema')
                orm_table = getattr(table_model, table)
                orm_table.__table__.create(bind=dal.engine, checkfirst=True)

    def __update_non_order_table(self, dataframe: pandas.DataFrame,
                                 model) -> None:
        """
        Synchronize a database table with a given pandas DataFrame.

        The export stems from Plentymarkets, delete rows that were deleted on
        Plentymarkets and add rows that are found in Plentymarkets but not
        in the database.

        This method should only be used for tables where an EAN is found at
        most once.

        Parameters:
            dataframe   [DataFrame] -   Export from the Plentymarkets REST API
            model       [Declarative
                         Meta]      -   Class identifier for the database
                                        schema
        """
        db_eans = []
        # Delete all variations removed from Plentymarkets
        self.session.query(model).filter(
            model.ean.notin_(dataframe['ean'])
        ).delete(synchronize_session='fetch')

        # Update existing rows
        for db_row in self.session.query(model).all():
            db_row.update_by_dataframe(dataframe=dataframe)
            db_eans.append(db_row.ean)

        # Add rows that were newly created on Plentymarkets
        new_rows = dataframe[~dataframe['ean'].isin(db_eans)]
        if len(new_rows.index) > 0:
            self.session.add_all(
                [model(**x) for x in new_rows.to_dict(orient='records')]
            )
        self.session.commit()

    def add_stock(self, dataframe: pandas.DataFrame) -> None:
        """
        Update the stock information for each product on the database.

        The export dictates how the database is filled, therefore all
        entries missing in the export are removed from the database.

        Parameters:
            dataframe   [DataFrame] -   Generated dataframe from
                                        `pull_stock_data`
        """
        self.__update_non_order_table(dataframe=dataframe, model=StockModel)

    def sync_identification_data(self, dataframe: pandas.DataFrame) -> None:
        """
        Update the identification entries for each product on the database.

        The export dictates how the database is filled, therefore all
        entries missing in the export are removed from the database.

        Parameters:
            dataframe   [DataFrame] -   Generated dataframe from
                                        `pull_identification_data`
        """
        self.__update_non_order_table(dataframe=dataframe,
                                      model=IdentificationModel)

    def handle_order(self, dataframe):
        existing_orders = [x for x, in self.session.query(OrdersModel.id)]
        new_entries = dataframe[~dataframe['order_id'].isin(existing_orders)]
        # Mute a false-positive warning
        pandas.set_option('mode.chained_assignment', None)
        new_entries.loc[:,'ean'] = [
            self.session.query(IdentificationModel).filter(
                IdentificationModel.variation_id == var
            ).one().ean for var in new_entries['variation_id']
        ]
        pandas.set_option('mode.chained_assignment', 'warn')

        for order in new_entries['order_id'].unique():
            order_match_rows = new_entries[new_entries['order_id'] == order]
            assert len(order_match_rows['origin_id'].unique()) == 1
            assert len(order_match_rows['external_id'].unique()) == 1
            (total_net, total_gross) = [
                sum([
                    price * qty for price, qty in zip(
                        order_match_rows[type], order_match_rows['quantity'])
                ]) for type in ['net_price', 'gross_price']
            ]
            order_items = [
                OrderItemsModel(
                    id=v.order_item_id,
                    external_id=v.external_id, origin_id=v.origin_id,
                    variation_id=v.variation_id, ean=v.ean, item_id=v.item_id,
                    name=v.name, quantity=v.quantity, netto=v.net_price,
                    gross=v.gross_price
                ) for v in order_match_rows.itertuples()
            ]
            # Take the first entry that matches the order id, as the attributes
            # required for the order creation are the same for each entry.
            entry = order_match_rows.iloc[0]
            order = OrdersModel(
                id=entry.order_id,
                origin_id=entry.origin_id, netto_total=total_net,
                gross_total=total_gross,
                creation_date=transform_to_dateobject(entry.creation_date),
                payment_date=transform_to_dateobject(entry.payment_date),
                delivery_date=transform_to_dateobject(entry.delivery_date),
                b2b=entry.b2b,
                external_id=entry.external_id,
                order_items=order_items
            )
            self.session.add(order)

        self.session.commit()


class DataCollector:
    def __init__(self, api_base_url: str, config, collect_options: dict):
        self.config = config
        self.collect_options = collect_options
        self.variations = []

        self.api = plenty_api.PlentyApi(base_url=api_base_url, debug=False)
        self.api.cli_progress_bar = True

    def pull_stock_data(self, warehouse_config: dict) -> pandas.DataFrame:
        """
        Get the stock data of the given warehouses for all variations.

        Parameters:
            warehouse_config[dict]  -   Mapping of database table columns to
                                        Plentymarkets warehouse IDs

        Return:
                        [DataFrame]
        """
        if not self.variations:
            self.variations = self.api.plenty_api_get_variations(
                additional=self.collect_options['variation']
            )
        columns = ['variation_id', 'ean'] + list(warehouse_config.keys())
        dataframe_data = []
        ean_id = self.config['PLENTY']['ean_barcode_id']
        for variation in self.variations:
            ean = get_ean(json=variation, ean_id=ean_id)
            if not ean or ean == '0':
                continue
            row = [variation['id'], ean]
            row += get_stock(variation=variation, warehouses=warehouse_config)
            dataframe_data.append(row)

        dataframe = pandas.DataFrame(dataframe_data, columns=columns)
        return dataframe.fillna(0)

    def pull_identification_data(self) -> pandas.DataFrame:
        """
        Get identification data (barcodes, skus, name) for all variations.

        Both parameters are supplied by the user via configuration.

        Return:
                        [DataFrame]
        """
        columns = ['variation_id', 'ean', 'sku', 'asin_all', 'name']
        if not self.variations:
            self.variations = self.api.plenty_api_get_variations(
                additional=self.collect_options['variation']
            )
        dataframe_data = []
        ean_id = self.config['PLENTY']['ean_barcode_id']
        asin_id = self.config['PLENTY']['asin_id']
        for variation in self.variations:
            ean = get_ean(json=variation, ean_id=ean_id)
            if not ean or ean == '0':
                continue
            row = [
                variation['id'], get_ean(json=variation, ean_id=ean_id),
                variation['number'], get_asin(json=variation, asin_id=asin_id),
                variation['name']
            ]
            dataframe_data.append(row)
        return pandas.DataFrame(dataframe_data, columns=columns)

    def pull_purchase_orders(self, date_range: tuple) -> pandas.DataFrame:
        """
        Prepare a dataframe with a row for each order item for orders in range.

        Use only Euro currency prices.

        You can retrieve the customer classes on your system via the
        `/rest/accounts/contacts/classes` route

        Parameters:
            date_range  [tuple]     -   start and end dates for order fetch
                                        supplied by the other via CLI arguments
            b2b_class_id[int]       -   Id of the the customer class associated
                                        with business to business customers,
                                        supplied by the user via configuration

        Return:
                        [DataFrame]
        """
        columns = [
            'order_id', 'order_item_id', 'variation_id', 'item_id',
            'external_id', 'name', 'origin_id', 'net_price', 'gross_price',
            'quantity', 'creation_date', 'payment_date', 'delivery_date', 'b2b'
        ]
        if len(date_range) != 2:
            logger.error("Invalid date range: {date_range}")
            return pandas.DataFrame()

        (start, end) = date_range
        b2b_class_id = self.config['PLENTY']['b2b_customer_class_id']
        orders = self.api.plenty_api_get_orders_by_date(
            start=start, end=end, date_type="creation",
            additional=self.collect_options['order'],
            refine={'orderType': ORDER_TYPE_SALES_ORDER,
                    'statusFrom': 7, 'statusTo': 8})

        dataframe_data = []
        for order in orders:
            if not order['contactReceiver']:
                continue
            external_id = get_property_by_type(json=order,
                                               type='external_order_id')
            payment_date = get_date_by_type(json=order, type='paid')
            delivery_date = get_date_by_type(json=order, type='completed')
            b2b = is_b2b_customer(json=order, b2b_class_id=b2b_class_id)
            for item in order['orderItems']:
                if item['typeId'] != ORDER_ITEM_TYPE_VARIATION:
                    continue
                variation_id = item['itemVariationId']
                variation_name = item['variation']['name']
                item_id = item['variation']['itemId']
                item_net = get_amount(json=item, type='net')
                item_gross = get_amount(json=item, type='gross')
                dataframe_data.append([
                    order['id'], item['id'], variation_id, item_id,
                    external_id, variation_name, order['referrerId'],
                    item_net, item_gross, item['quantity'], order['createdAt'],
                    payment_date, delivery_date, b2b
                ])

        return pandas.DataFrame(dataframe_data, columns=columns)
