# Synchronize orders, stock, and identification data with a MariaDB database

## Introduction

This sub-application of the **Plenty RAIT** tool has two purposes:
1. Update the MariaDB database with the latest stock, order, and identification data (mapping of ASIN, Plentymarkets Variation ID, EAN, SKU, NAME)
2. Create new redistributions from a given list of variation IDs and redistribution quantities between two warehouses on Plentymarkets

## Configuration

The application requires some basic information in order to work properly, you have to provide the URL for the Plentymarkets REST API endpoint (from Setup->Settings->API->Data->Host (prepended by 'https://')). Additionally, in order to fetch the correct stock data you need to specify the warehouses you want to use (map them to columns of the `current_stock` table in the database) (locate the warehouse IDs under Setup->Stock->Warehouse->{target warehouse}->Settings). The `ean_barcode_id` and `asin_id` determine which of the many possible barcodes and ASINs shall be used for the `identification` table.  
The configuration file is located under `~/.config/plenty_rait/plenty_sync/config.ini` on a Linux system and under `C:\users\{$USER}\.config/plenty_rait/plenty_sync/config.ini` on a Windows system, it will be created automatically when you launch the application for the first time.  
The `b2b_customer_class_id` is required to identify, which orders are *b2b* and which orders are *b2c* sales. In order to find the correct ID you can use the following series of python instructions:
```python
import plenty_api
import requests
plenty = plenty_api.PlentyApi("https://company.plentymarkets-cloud01.com")
print(requests.get(plenty.url + '/rest/accounts/contacts/classes', headers=plenty.creds).json())
```

Example configuration:
```ini
[PLENTY]
base_url=https://company.plentymarkets-cloud01.com
warehouses=stock_warehouse_1:104;stock_warehouse_2:111
ean_barcode_id=1
asin_id=0
b2b_customer_class_id=3
```

The syntax for the `warehouses` field is: `warehouses={column from the 'current_stock' table}:{ID from Plentymarkets}`.

## Usage

In order to use the application, you need to create a REST API user with the appropriate rights on Plentymarkets under: Setup->Settings->User->Accounts.
When the application is started you will be prompted for the user name and password and these credentials will be saved into the system-wide keyring.

Here are the possible routines, they may be connected in any order.

* Create the database tables in case they don't exist
```bash
python3 -m plenty_sync --create_tables
```
* Synchronize the stock for the mapped warehouses
```bash
python3 -m plenty_sync --stock
```
* Update the identification table
```bash
python3 -m plenty_sync --identity
```
* Import orders into the MariaDB database from Plentymarkets
```bash
python3 -m plenty_sync --order --from 2021-03-05 --to 2021-04-05
```
