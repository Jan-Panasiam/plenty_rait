# Plenty RAIT: Report tool

This tool is part of the Plentymarkets Reorder And Inventory Transfer tool, it is used to save the sales within a given time-range into a sheet or a separate `csv` file.

## Installation

Either install with the plenty_rait installation script or seperatly with: `poetry install`

## Configuration

You have to configure which origins you want to present and the assigned column-name in the output file.

Example:
```bash
[FORMAT]
amazon_fbm_ger=4.01
amazon_fbm_ger_b2b=4.21
amazon_fba_ger=104.01
```
In this case `amazon_fbm_ger` will be the column name and it will contain the sales for the origin `4.01`.

## Usage

There are 3 different report date-ranges:
* Number of days in the past (`--vk 30` (now: 2020-12-10) -> (until: 2020-11-10))
* Specific range between two dates (`--start 2020-11-10 --end 2020-12-10`)
* A whole year (`--year 2020` (2020-01-01 - 2020-12-31 (unless the year is not over yet)))

You can specify a file location or use the default path, which saves the file as `report.csv` into the `.config/plenty_rait` directory.

Examples:
```bash
python3 -m report --vk 30
python3 -m report --start 2020-11-10 --end 2020-12-30 --file ~/test_report.csv
```
