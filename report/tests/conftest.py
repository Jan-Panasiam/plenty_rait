import pytest
from sqlalchemy.orm import scoped_session, sessionmaker
from report.schema import dal, Base


@pytest.fixture(scope='function')
def session(request):
    dal.connection = str('mysql+pymysql://testuser:testpw1234@localhost'
                         '/testdb')

    dal.connect()
    dal.Session = scoped_session(sessionmaker(bind=dal.engine))
    dal.session = dal.Session()
    dal.Session.registry.clear()

    request.addfinalizer(Base.metadata.drop_all)
    return dal.session


@pytest.fixture(scope='function')
def db_session(request, session):
    request.addfinalizer(session.rollback)
    return session
