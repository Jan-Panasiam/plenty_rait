import pytest
import datetime

from report.service import Report
from report.schema import DailyStatisticModel, IdentificationModel


def setup_database(session, sources: dict) -> None:
    try:
        session.add_all(sources['stats'])
        session.add_all(sources['ident'])
    except Exception:
        session.rollback()
    else:
        session.commit()


@pytest.fixture
def test_statistic() -> list:
    stats = [
        DailyStatisticModel(
            variation_id=1234, origin_id=4.03, day=13, month=7,
            year=2018, quantity=2
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.03, day=15, month=1,
            year=2019, quantity=1
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.01, day=5, month=3,
            year=2019, quantity=2
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.01, day=14, month=5,
            year=2019, quantity=3
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.01, day=24, month=8,
            year=2019, quantity=5
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.03, day=17, month=3,
            year=2020, quantity=2
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.01, day=28, month=6,
            year=2020, quantity=3
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=104.02, day=25, month=9,
            year=2020, quantity=6
        )
    ]
    return stats


@pytest.fixture
def test_ident() -> list:
    ident = [
        IdentificationModel(variation_id=1234, ean='1234567891011',
                            sku='test_sku', asin_all='B012345678')
    ]
    return ident


@pytest.fixture
def incorrect_ident() -> list:
    ident = [
        IdentificationModel(variation_id=2345, ean='1234567891013',
                            sku='test_sku_2', asin_all='B012345679')
    ]
    return ident


@pytest.fixture
def sources(test_statistic, test_ident) -> dict:
    sources = {
        'stats': test_statistic,
        'ident': test_ident
    }
    return sources


@pytest.fixture
def err_sources(test_statistic, incorrect_ident) -> dict:
    sources = {
        'stats': test_statistic,
        'ident': incorrect_ident
    }
    return sources


def describe_select_by_date_range() -> None:
    def with_no_sales_range(db_session) -> None:
        report = Report(year=2020, session=db_session)
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            report.select_by_date_range(origin_id=104.01)
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

    def with_no_entry_in_range(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(sales_range='31', session=db_session)
        report.end = datetime.date(2020, 12, 3)
        result = report.select_by_date_range(origin_id=104.02)
        assert 0 == len(result.index)

    def with_range_within_month(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(sales_range='10', session=db_session)
        report.end = datetime.date(2020, 3, 20)
        result = report.select_by_date_range(origin_id=4.03)
        assert 2 == result['quantity'].sum()
        assert 1 == len(result.index)

    def with_range_within_year(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(sales_range='71', session=db_session)
        report.end = datetime.date(2019, 6, 20)
        result = report.select_by_date_range(origin_id=4.01)
        assert 3 == result['quantity'].sum()
        assert 1 == len(result.index)

    def with_range_within_two_year_span(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(sales_range='531', session=db_session)
        report.end = datetime.date(2020, 8, 13)
        result = report.select_by_date_range(origin_id=4.01)
        print(result)
        assert 13 == result['quantity'].sum()
        assert 1 == len(result.index)

    def with_range_within_multi_year_span(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(sales_range='689', session=db_session)
        report.end = datetime.date(2020, 3, 20)
        result = report.select_by_date_range(origin_id=4.03)
        assert 5 == result['quantity'].sum()
        assert 1 == len(result.index)

    def with_no_entry_in_ident_table(db_session, err_sources: dict) -> None:
        setup_database(session=db_session, sources=err_sources)
        report = Report(sales_range='90', session=db_session)
        report.end = datetime.date(2020, 3, 20)
        result = report.select_by_date_range(origin_id=4.01)
        assert 0 == len(result.index)

    def with_missing_ident_table(db_session, sources: dict) -> None:
        try:
            db_session.add_all(sources['stats'])
        except Exception:
            db_session.rollback()
        else:
            db_session.commit()
        report = Report(sales_range='4', session=db_session)
        report.now = datetime.date(2020, 3, 20)
        result = report.select_by_date_range(origin_id=4.03)
        assert 0 == len(result.index)


def describe_select_by_year() -> None:
    def with_no_year(db_session) -> None:
        report = Report(sales_range=30, session=db_session)
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            report.select_by_year(origin_id=104.01)
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

    def with_no_entry_in_year(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(year=2017, session=db_session)
        result = report.select_by_year(origin_id=104.01)
        assert 0 == len(result.index)

    def with_single_entry_in_year(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(year=2018, session=db_session)
        result = report.select_by_year(origin_id=4.03)
        assert 2 == result['quantity'].sum()
        assert 1 == len(result.index)

    def with_multiple_entries_in_year(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(year=2019, session=db_session)
        result = report.select_by_year(origin_id=4.01)
        assert 10 == result['quantity'].sum()
        assert 1 == len(result.index)
