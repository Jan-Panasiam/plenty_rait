import configparser

from report.cli import parse_origins


def describe_parse_origins():
    def with_missing_format_configuration():
        config = configparser.ConfigParser()
        config['Wrong'] = {'test': '1',
                           'test2': 'yes'}
        expect = {}
        result = parse_origins(config=config)

        assert expect == result

    def with_single_correct_entry():
        config = configparser.ConfigParser()
        config['FORMAT'] = {'fba_ger': '104.01'}
        expect = {'104.01': 'fba_ger'}
        result = parse_origins(config=config)

        assert expect == result

    def with_multiple_correct_entries():
        config = configparser.ConfigParser()
        config['FORMAT'] = {'fba_ger': '104.01',
                            'fbm_ger': '4.01'}
        expect = {'104.01': 'fba_ger',
                  '4.01': 'fbm_ger'}
        result = parse_origins(config=config)

        assert expect == result

    def with_incorrect_entry():
        config = configparser.ConfigParser()
        config['FORMAT'] = {'fba_ger': 'abc'}
        expect = {}
        result = parse_origins(config=config)

        assert expect == result

    def with_some_incorrect_entries():
        config = configparser.ConfigParser()
        config['FORMAT'] = {'fba_ger': '104.01',
                            'test2': 'yes',
                            'fbm_ger': '4.01',
                            'shop': '1',
                            'ebay': 'xyz'}
        expect = {'1': 'shop', '4.01': 'fbm_ger',
                  '104.01': 'fba_ger'}
        result = parse_origins(config=config)

        assert expect == result

    def without_config():
        expect = {}
        result = parse_origins(config=None)

        assert expect == result
