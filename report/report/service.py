"""
Sub application of the Plenty RAIT tool with the purpose of reporting
sales for a given time-range as a sheet or separate csv.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
import pandas
from sqlalchemy import and_, func
from sqlalchemy.orm import scoped_session, sessionmaker, Query
from sqlalchemy.orm.exc import NoResultFound
import sys

from loguru import logger

from report.schema import (
    dal, DailyStatisticModel, IdentificationModel
)


class Report:
    def __init__(self, sales_range: int = 0, year: int = 0,
                 date_range: tuple = None, session=None):
        """
        The date range is fixed during the instantiation of the object.

        When month is -1 (default), then the whole year is used.
        """
        self.session = session
        self.year = year
        self.sales_range = sales_range
        self.start = None
        self.end = None
        if sales_range and not date_range:
            self.end = datetime.datetime.now()
        elif date_range and not sales_range:
            self.start = date_range[0]
            self.end = date_range[1]

    def create_database_session(self, connection_string: str) -> None:
        dal.connection = connection_string
        dal.connect()

        dal.Session = scoped_session(sessionmaker(bind=dal.engine))
        dal.session = dal.Session()
        dal.Session.registry.clear()

        self.session = dal.session

    def select_by_date_range(self, origin_id: float) -> pandas.DataFrame:
        """
        Get all orders within the given date range, that were created on
        a specific marketplace.

        Parameter:
            origin_id           [float]     -   The marketplace origin ID from
                                                Plentymarkets.

        Return:
                                [DataFrame] -   A pandas dataframe with the
                                                columns:
                                                    [ASIN, quantity]
        """
        if not self.start and not self.end:
            logger.error("No date range or sales range provided. Abort.")
            sys.exit(1)
        if not self.__valid_origin(origin_id=origin_id):
            return pandas.DataFrame(columns=['ASIN', 'quantity'])

        if self.sales_range:
            self.start = self.end - datetime.timedelta(
                days=int(self.sales_range))

        items = self.session.query(
            DailyStatisticModel
        ).filter(
            and_(
                DailyStatisticModel.origin_id == origin_id,
                DailyStatisticModel.date_value <= self.end,
                DailyStatisticModel.date_value >= self.start
            )
        )

        return self.__build_dataframe(items=items)

    def select_by_year(self, origin_id: float) -> pandas.DataFrame:
        """
        Get all orders within the given year, that were created on a specific
        marketplace.

        Parameter:
            origin_id           [float]     -   The marketplace origin ID from
                                                Plentymarkets.

        Return:
                                [DataFrame] -   A pandas dataframe with the
                                                columns:
                                                    [ASIN, quantity]
        """
        if not self.year:
            logger.error("'select_by_year' requires a valid 'year' value. "
                         f"Got {self.year}.")
            sys.exit(1)

        if not self.__valid_origin(origin_id=origin_id):
            return pandas.DataFrame(columns=['ASIN', 'quantity'])

        items = self.session.query(DailyStatisticModel).filter(
            and_(
                DailyStatisticModel.year == self.year,
                DailyStatisticModel.origin_id == origin_id
            )
        )

        return self.__build_dataframe(items=items)

    def get_items_without_sales(self, sold_items: list,
                                columns: list) -> pandas.DataFrame:
        remaining_asin = self.session.query(IdentificationModel).filter(
            and_(
                IdentificationModel.asin_all.notin_(sold_items),
                IdentificationModel.asin_all != '0',
                IdentificationModel.ean != '0'
            )
        )

        raw_data = []
        for asin in remaining_asin.all():
            entry = [asin.asin_all]
            entry += [0 for x in columns]
            raw_data.append(entry)

        return pandas.DataFrame(raw_data, columns=['ASIN'] + columns)

    def __valid_origin(self, origin_id: float) -> bool:
        """
        Get the distinct origin ID values from the statistics table and
        check if the given ID is found.

        Parameter:
            origin_id           [float]     -   Origin ID from Plentymarkets

        Return:
                                [bool]      -   True if valid, False if not
        """
        origins = self.session.query(
            func.distinct(DailyStatisticModel.origin_id)
        ).filter(
            DailyStatisticModel.origin_id == origin_id
        )
        if not origins.all():
            return False
        return True

    def __get_asin(self, variation_id: int) -> str:
        try:
            asin = self.session.query(IdentificationModel).filter(
                IdentificationModel.variation_id == variation_id
            ).one()
        except NoResultFound:
            return ''
        return asin.asin_all

    def __build_dataframe(self, items: Query) -> pandas.DataFrame:
        raw_data = {}
        for item in items.all():
            if not item:
                break
            asin = self.__get_asin(variation_id=item.variation_id)
            if not asin or asin == '0':
                logger.warning(f"No ASIN found for {item.variation_id}, "
                               f"skip (qty: {item.quantity})")
                continue
            qty = item.quantity
            if asin not in raw_data.keys():
                raw_data[asin] = [asin, qty]
            else:
                raw_data[asin][1] += qty

        return pandas.DataFrame.from_dict(raw_data, orient='index',
                                          columns=['ASIN', 'quantity'])
