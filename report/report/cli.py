"""
Sub application of the Plenty RAIT tool with the purpose of reporting
sales for a given time-range as a sheet or separate csv.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import configparser
import datetime
import getpass
import keyring
import openpyxl
from openpyxl.utils.dataframe import dataframe_to_rows
import os
import pandas
import pathlib
import sys

from loguru import logger

from report.service import Report


PROG_NAME = "sales_report"

USER = os.getlogin()
if sys.platform == 'linux':
    BASE_PATH = os.path.join(
        '/', 'home', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )
elif sys.platform == 'win32':
    BASE_PATH = os.path.join(
        'C:\\', 'Users', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )

DB_CONFIG = os.path.join(BASE_PATH, 'db_config.ini')
CONFIG_FOLDER = os.path.join(BASE_PATH, PROG_NAME)
if not os.path.exists(CONFIG_FOLDER):
    os.mkdir(CONFIG_FOLDER)

CONFIG_PATH = os.path.join(CONFIG_FOLDER, 'config.ini')

if not os.path.exists(DB_CONFIG):
    open(DB_CONFIG, 'a').close()

if not os.path.exists(CONFIG_PATH):
    open(CONFIG_PATH, 'a').close()


def setup_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog=PROG_NAME)
    parser.add_argument('--sales', '--vk', '-v', required=False,
                        help='Number of days to report.',
                        dest='vk', type=int)
    parser.add_argument('--date_range_start', '--range_start', '-s',
                        required=False,
                        help='Date range start for sales reports.',
                        dest='start_date', type=datetime.date.fromisoformat)
    parser.add_argument('--date_range_end', '--range_end', '-e',
                        required=False,
                        help='Date range end for sales reports.',
                        dest='end_date', type=datetime.date.fromisoformat)
    parser.add_argument('--year', '--sales_for_year', '-y', required=False,
                        help='Create a report for a full year.',
                        dest='year', type=int)
    parser.add_argument('--host', '-l', required=False,
                        help='The location of the database remote server or '
                             'localhost',
                        dest='host', default='localhost')
    parser.add_argument('--database', '--db', '-d', required=False,
                        help='Name of the database',
                        dest='db')
    parser.add_argument('--file', '-f', required=False,
                        help='Target destination for the finished report',
                        dest='path', default=os.path.join(BASE_PATH,
                                                          'report.csv'))
    parser.add_argument('--excel', '--xlsx', '--xlsm',
                        required=False, help='Save the report within an '
                        'xlsx/xlsm file to the given sheet @ `--sheet`',
                        dest='excel_path')
    parser.add_argument('--sheet', required=False,
                        help='Specifiy the name of the sheet within the given '
                        'xlsx/xlsm file @ `--excel`',
                        dest='excel_sheet')
    parser.add_argument('--reset', '-r', required=False,
                        help='Reset the system keyring',
                        action='store_true', dest='reset')
    args = parser.parse_args()

    if (not args.vk and not args.year and
            not args.start_date and not args.end_date):
        logger.error("Provide either the number of days until now, the year, "
                     "or a specific date range for the sales report. "
                     "[--vk, --year or (--range_start & --range_end)]")
        sys.exit(1)

    date_range_active = args.start_date and args.end_date
    if ((args.vk and args.year) or (args.vk and date_range_active) or
            (date_range_active and args.year)):
        logger.error("Choose exactly one of these three options. "
                     "[--vk, --year, or (--range_start & --range_end)]")
        sys.exit(1)

    if ((args.start_date and not args.end_date) or
            (not args.start_date and args.end_date)):
        logger.error("Provide both start and end date for a date range.")

    if args.excel_path or args.excel_sheet:
        if not (args.excel_path and args.excel_sheet):
            logger.error("The excel sheet argument requires both a file path "
                         "and a sheet name.")
            sys.exit(1)
        if not pathlib.Path(args.excel_path).is_file:
            logger.error(f"The given excel sheet at {args.excel_path} doesn't "
                         "exist.")
            sys.exit(1)

    return args


def get_database(config: configparser.ConfigParser,
                 args: argparse.ArgumentParser) -> tuple:
    if args.db:
        db_name = args.db
    elif config.has_option(section='DATABASE', option='database_name'):
        db_name = config['DATABASE']['database_name']
    else:
        logger.error("Database name required either from the database "
                     "configuration or from CLI argument.")
        sys.exit(1)

    if args.host:
        host = args.host
    elif config.has_option(section='DATABASE', option='host'):
        host = config['DATABASE']['host']
    else:
        logger.info("No host declared in the configuration or CLI "
                    "arguments, fall back to default 'localhost'")
        host = 'localhost'

    return (host, db_name)


def get_database_credentials(reset: bool, host: str, database: str) -> tuple:
    user = keyring.get_password('order_list_database_identity', 'user')
    password = keyring.get_password('order_list_database_identity', 'password')
    if not user or not password or reset:
        print(f"Login credentials for {database}@{host}:")
        user = input('Username: ')
        password = getpass.getpass()
        keyring.set_password('order_list_database_identity', 'user', user)
        keyring.set_password('order_list_database_identity', 'password',
                             password)
        return (user, password)
    return (user, password)


def parse_origins(config: configparser.ConfigParser) -> dict:
    """
    Read a map of column name to Plentymarkets origin ID combinations and
    create a dictionary of origin ID keys and column name values.

    Exchange key and value, as the configuration format is more natural for the
    user, while the ID acts as input for the select_by_date_range method.

    Parameter:
        config          [configparser object]   -   Configuration @CONFIG_PATH

    Return:
                        [dict]                  -   ID - column name dictionary
    """
    if not config or not config.has_section(section='FORMAT'):
        return {}

    output = {}

    for key, value in config['FORMAT'].items():
        try:
            float(value)
        except ValueError:
            continue
        output[str(value)] = str(key)

    return output


def equal_header(sheet, dataframe) -> bool:
    """
    Compare the header row of a dataframe and a Excel sheet for equality.
    """
    sheet_row = [x.value for x in list(sheet.rows)[0]]
    if list(dataframe.columns) == sheet_row:
        return True
    return False


def get_excel_column_by_index(index: int):
    if index <= 0:
        return ''
    return get_excel_column_by_index(index // 26) + chr(index % 26 + ord('A'))


def remove_special_chars(name: str) -> str:
    special_chars = ['?', '!', ':', '.', ',', '/', '\\',
                     'ä', 'ö', 'ü', 'Ä', 'Ü', 'Ö', 'ß']
    name = ''.join([c for c in name if c not in special_chars])
    name = name.replace(' ', '_').lower()
    return name


def main():
    args = setup_argparser()
    # Open the shared database configuration for the order list project
    db_config = configparser.ConfigParser()
    db_config.read(DB_CONFIG)
    config = configparser.ConfigParser()
    config.read(CONFIG_PATH)

    origins = parse_origins(config=config)
    if not origins:
        logger.error("No report format configuration found. For an example "
                     "look into the README.md file.")
        sys.exit(1)

    (host, db_name) = get_database(config=db_config, args=args)
    (user, password) = get_database_credentials(reset=args.reset,
                                                host=host,
                                                database=db_name)
    connection_string = str(
        f"mysql+pymysql://{user}:{password}@{host}/{db_name}")
    if args.vk:
        report = Report(sales_range=args.vk)
    elif args.year:
        report = Report(year=args.year)
    elif args.start_date and args.end_date:
        report = Report(date_range=(args.start_date, args.end_date))
    report.create_database_session(connection_string=connection_string)

    result_table = pandas.DataFrame()
    for origin in origins:
        if args.vk:
            table = report.select_by_date_range(origin_id=origin)
        elif args.year:
            table = report.select_by_year(origin_id=origin)
        elif args.start_date and args.end_date:
            table = report.select_by_date_range(origin_id=origin)

        if len(table.index) == 0:
            continue
        table.rename(columns={'quantity': origins[origin]}, inplace=True)
        table.fillna(0, inplace=True)
        if len(result_table.index) == 0:
            result_table = table
        else:
            result_table = result_table.merge(table, how="outer")

    result_table = result_table.fillna(value=0)
    result_table.reset_index(inplace=True, drop=True)

    items = result_table['ASIN'].values.tolist()
    cols = result_table.columns.values.tolist()[1:]
    remaining = report.get_items_without_sales(sold_items=items, columns=cols)
    result_table = pandas.concat([result_table, remaining])

    result_table[cols] = result_table[cols].applymap(int)
    total = result_table.sum(numeric_only=True, axis=1)
    result_table.insert(1, 'Total', total)
    result_table.sort_values(inplace=True, ascending=False, ignore_index=True,
                             by='Total')


    if not args.excel_path:
        result_table.to_csv(args.path, sep=';', na_rep='0', index=False)
        logger.info(f"Saved report at {args.path}.")
    else:
        workbook = openpyxl.load_workbook(args.excel_path)
        if args.excel_sheet not in workbook.get_sheet_names():
            sheet = workbook.create_sheet(title=args.excel_sheet)
        else:
            sheet = workbook[args.excel_sheet]
            if not equal_header(sheet=sheet, dataframe=result_table):
                logger.error("Overwrite is only possible if the headers are "
                             "equal, delete the sheet manually.")
                sys.exit(0)

            number_of_rows = sum(1 for e in sheet)
            sheet.delete_rows(0, number_of_rows)

        for row in dataframe_to_rows(result_table, index=False, header=True):
            sheet.append(row)

        named_range_name = remove_special_chars(
            name=args.excel_sheet + '_range')
        named_range_range = str(
            f"$A$2:${get_excel_column_by_index(index=len(origins)+2)}"
            f"${len(result_table.index) + 1}")
        try:
            if workbook.get_named_range(name=named_range_name):
                del workbook.defined_names[named_range_name]
        except KeyError:
            logger.info(f"Creating named range: {named_range_name}")
        workbook.create_named_range(named_range_name,
                                    sheet, named_range_range)

        workbook.save(args.excel_path)
        logger.info(f"Saved report at {args.excel_path} on sheet "
                    f"{args.excel_sheet}")

