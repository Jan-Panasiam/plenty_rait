"""
Sub application of the Plenty RAIT tool with the purpose of reporting
sales for a given time-range as a sheet or separate csv.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy_utils import generic_repr
from sqlalchemy import (
    Column, Integer, Text, String, Numeric, create_engine, func
)

Base = declarative_base()


@generic_repr
class DailyStatisticModel(Base):
    __tablename__ = "daily_statistics"

    id = Column(Integer, primary_key=True)
    variation_id = Column(Integer)
    ean = Column(String(13))
    origin_id = Column(Numeric(precision=8, scale=2))
    day = Column(Integer)
    month = Column(Integer)
    year = Column(Integer)
    quantity = Column(Integer)

    @hybrid_property
    def date_value(self):
        return func.str_to_date(func.concat(self.year, "-", self.month, "-",
                                            self.day), "%Y-%m-%d")


@generic_repr
class IdentificationModel(Base):
    __tablename__ = "identification"

    variation_id = Column(Integer, primary_key=True, autoincrement=False)
    ean = Column(String(13))
    sku = Column(Text(50))
    asin_all = Column(String(10))
    name = Column(Text(300))


class DataAccessLayer:
    def __init__(self):
        self.connection = ''
        self.engine = None
        self.Session = None
        self.session = None

    def connect(self):
        self.engine = create_engine(self.connection)
        Base.metadata.bind = self.engine
        Base.metadata.create_all(self.engine)
        self.session = sessionmaker(bind=self.engine)


dal = DataAccessLayer()
