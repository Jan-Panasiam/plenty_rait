"""
Sub application of the Plenty RAIT tool with the purpose of writing
identification, stock and prediciton data to a `xlsx/xlsm` file.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
import re
import dateutil.relativedelta
import numpy as np
import pandas
from sqlalchemy import and_, or_, func
from sqlalchemy.orm import scoped_session, sessionmaker
from openpyxl import load_workbook
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.formula.translate import Translator
from openpyxl.styles import Border, Font, PatternFill, GradientFill, Side
from loguru import logger

from update_spreadsheet.schema import (
    dal, StockModel, IdentificationModel, StatisticModel
)


def remove_deprecated_rows(source_df, sheet_df) -> pandas.DataFrame:
    """
    Remove variations from the Excel sheet (dataframe) that were deleted
    from the database.
    """
    test = np.where(
        ~sheet_df['variation_id'].isin(source_df['variation_id']), 1, 0)
    sheet_df['drop'] = test
    drop_indeces = sheet_df[sheet_df['drop'] == 1].index
    sheet_df = sheet_df.drop(drop_indeces)
    sheet_df = sheet_df.drop(['drop'], axis=1)

    return sheet_df


def update_old_identification_values(source_df, sheet_df) -> pandas.DataFrame:
    """
    Update identification values within the Excel sheet dataframe.
    """
    columns = ['ean', 'asin', 'name', 'sku']
    valid_columns = [x for x in columns if x in
                     list(source_df.columns) and x in list(sheet_df.columns)]

    new_df = pandas.merge(sheet_df, source_df, on='variation_id', how='inner',
                          suffixes=("_sheet", "_db"))
    for column in valid_columns:
        sheet_df[column] = new_df[str(f"{column}_db")]

    return sheet_df


def remove_duplicate_rows(source_df, sheet_df) -> pandas.DataFrame:
    """
    Remove rows from the source dataframe (export from database)
    that are already present in the sheet dataframe.
    """
    sheet_variations = sheet_df['variation_id'].values.tolist()
    source_df['duplicate'] = np.where(
        source_df['variation_id'].isin(sheet_variations), 1, 0)
    source_df = source_df[source_df['duplicate'] == 0]
    source_df = source_df.drop(['duplicate'], axis=1)
    source_df = source_df.reset_index(drop=True)

    return source_df


def insert_new_rows(source_df, sheet_df) -> pandas.DataFrame:
    """
    Combine two dataframes and sort them in ascending order.
    """
    for row in source_df.itertuples():
        sheet_df = sheet_df.append({
            'variation_id': row.variation_id,
            'ean': row.ean,
            'asin': row.asin,
            'name': row.name,
            'sku': row.sku
        }, ignore_index=True)
    sheet_df = sheet_df.fillna('')
    sheet_df = sheet_df.sort_values(by='variation_id').reset_index(drop=True)

    return sheet_df


def get_formulas(sheet_df) -> dict:
    """
    Get the formulas to be used for all rows from the first row and map
    them to the column names.
    """
    formulas = {}
    first_row = sheet_df.loc[0,].to_dict()

    for key, value in first_row.items():
        if not isinstance(value, str):
            continue
        if re.search(r'=.*\(.*\)', value):
            formulas[key] = value

    return formulas


def apply_formulas(sheet_df, formulas: dict) -> pandas.DataFrame:
    """
    Apply the different formulas to every row with according reference
    updates.
    """
    for i in sheet_df.index:
        for key in formulas:
            if i != 0:
                formulas[key] = Translator(
                    formulas[key], origin='B2').translate_formula('B3')
            sheet_df.loc[i, key] = formulas[key]

    return sheet_df


def get_prediction_min_max_date(source_df) -> tuple:
    min_year = source_df['year'].min()
    min_month = source_df[source_df['year'] == min_year]['month'].min()
    oldest_entry = datetime.datetime(min_year, min_month, 1)
    max_year = source_df['year'].max()
    max_month = source_df[source_df['year'] == max_year]['month'].max()
    newest_entry = datetime.datetime(max_year, max_month, 1)
    return (oldest_entry, newest_entry)


def daterange_generator(start_date, end_date):
    month_diff = (end_date.year - start_date.year) * 12 +\
        (end_date.month - start_date.month)
    for n in range(int(month_diff)):
        yield start_date + dateutil.relativedelta.relativedelta(months=+n)


def get_multi_level_columns_by_date_range(
        date_range: tuple) -> pandas.MultiIndex:
    years = []
    months = []

    for date in daterange_generator(start_date=date_range[0],
                                    end_date=date_range[1]):
        years.append(date.year)
        months.append(date.month)
    return pandas.MultiIndex.from_arrays([years, months],
                                         names=('year', 'month'))


def get_date_labels_from_date_range(date_range: tuple) -> list:
    result_dates = []
    for date in daterange_generator(start_date=date_range[0],
                                    end_date=date_range[1]):
        result_dates.append(str(f"{date.month}-{date.year}"))
    return result_dates


def get_excel_column_by_index(index: int):
    if index <= 0:
        return ''
    return get_excel_column_by_index(index // 26) + chr(index % 26 + ord('A'))


def remove_special_chars(name: str) -> str:
    special_chars = ['?', '!', ':', '.', ',', '/', '\\',
                     'ä', 'ö', 'ü', 'Ä', 'Ü', 'Ö', 'ß']
    name = ''.join([c for c in name if c not in special_chars])
    name = name.replace(' ', '_').lower()
    return name


class UpdateService:
    def __init__(self, sheet_config, session=None):
        self.sheet_config = sheet_config
        self.session = None
        if session:
            self.session = session

    def create_database_session(self, connection_string: str) -> None:
        dal.connection = connection_string
        dal.connect()

        dal.Session = scoped_session(sessionmaker(bind=dal.engine))
        dal.session = dal.Session()
        dal.Session.registry.clear()

        self.session = dal.session

    def fetch_stock_from_database(self):
        for sheet in self.sheet_config['sheets']:
            if sheet['sheet_type'] == 'stock':
                sheet_name = sheet['sheet_name']

        stock = self.session.query(StockModel)
        df = pandas.read_sql(stock.statement, self.session.bind)
        if len(df.index) == 0:
            logger.error("No stock data located in database.")
        df = df[list(self.sheet_config['stock_columns'].keys())]
        df.columns = self.sheet_config['stock_columns'].values()

        wb = load_workbook(self.sheet_config['file'])
        stock_sheet = wb[sheet_name]
        number_of_rows = sum(1 for e in stock_sheet)
        stock_sheet.delete_rows(0, number_of_rows)

        for row in dataframe_to_rows(df, index=False, header=True):
            stock_sheet.append(row)

        wb.save(self.sheet_config['file'])

    def __style_suggestion_sheet(self, sheet):
        thin_border = Side(border_style="thin", color="000000")
        thick_border = Side(border_style="double", color="000000")
        id_header = sheet['A1:E1'][0]
        for cell in id_header:
            cell.font = Font(name='Calibri', bold=True, size=12)
            cell.fill = PatternFill("solid", fgColor='00FF8080')
            cell.border = Border(bottom=thin_border)

        formula_header_1 = sheet['F1:K1'][0]
        formula_header_2 = sheet['L1:M1'][0]
        for cell in formula_header_1:
            cell.font = Font(name='Calibri', bold=True, size=12)
            cell.fill = PatternFill("solid", fgColor='00339966')
            cell.border = Border(bottom=thin_border)
        for cell in formula_header_2:
            cell.font = Font(name='Calibri', bold=True, size=12)
            cell.fill = PatternFill("solid", fgColor='00339966')
            cell.border = Border(bottom=thin_border)

        last_sales_header = sheet['J1:K1'][0]
        for cell in last_sales_header:
            cell.font = Font(name='Calibri', bold=True, size=11)
            cell.fill = PatternFill("solid", fgColor='ffc600')
            cell.border = Border(bottom=thin_border)

        adjustable_header = sheet['L1:Q1'][0]
        for cell in adjustable_header:
            cell.font = Font(name='Calibri', bold=True, size=12)
            cell.fill = PatternFill("solid", fgColor='00C0C0C0')
            cell.border = Border(bottom=thin_border)

        number_of_rows = sum(1 for e in sheet)

        i = 0
        for row in sheet.iter_rows(min_row=2, max_col=13, max_row=number_of_rows):
            j = 0
            for cell in row:
                if i % 2 == 0:
                    if j in [9, 10]:
                        cell.fill = PatternFill("solid", fgColor='cdf6cd')
                    else:
                        cell.fill = PatternFill("solid", fgColor='f6f6f6')
                else:
                    if j in [9, 10]:
                        cell.fill = PatternFill("solid", fgColor='e4f6e4')
                    else:
                        cell.fill = PatternFill("solid", fgColor='fbfbfb')
                if cell.col_idx == 13:
                    cell.border = Border(right=thick_border)
                j += 1
            i += 1

        sheet.freeze_panes = 'B2'

    def fetch_identification_data_from_database(self):
        """
        Update the identification data for each variation within every
        suggestion sheet.
        """
        sheet_names = []
        for sheet in self.sheet_config['sheets']:
            if sheet['sheet_type'] == 'suggestion':
                sheet_names.append(sheet['sheet_name'])

        inventory = self.session.query(IdentificationModel)
        db_df = pandas.read_sql(inventory.statement, self.session.bind)
        if len(db_df.index) == 0:
            logger.error("No inventory data located in database.")
            return

        db_df = db_df.rename(columns={'asin_all': 'asin'})
        db_df = db_df[db_df['ean'] != '0']

        wb = load_workbook(self.sheet_config['file'])

        for sheet_name in sheet_names:
            sheet = wb[sheet_name]
            data = sheet.values
            cols = next(data)
            data = list(data)
            df = pandas.DataFrame(data, columns=cols)
            if len(df.index) == 0:
                logger.error("Empty Excel sheet.")
                return
            df.dropna(subset=['variation_id'], inplace=True)
            formulas = get_formulas(sheet_df=df)
            df = remove_deprecated_rows(source_df=db_df, sheet_df=df)
            df = update_old_identification_values(source_df=db_df, sheet_df=df)
            db_df = remove_duplicate_rows(source_df=db_df, sheet_df=df)
            new_df = insert_new_rows(source_df=db_df, sheet_df=df)
            new_df = apply_formulas(sheet_df=new_df, formulas=formulas)

            number_of_rows = sum(1 for e in sheet)
            sheet.delete_rows(0, number_of_rows)

            for row in dataframe_to_rows(new_df, index=False, header=True):
                sheet.append(row)

            self.__style_suggestion_sheet(sheet=sheet)

            wb.save(self.sheet_config['file'])

    def __sufficient_predictions_check(self, oldest_entry, newest_entry):
        config_requirement = int(self.sheet_config['predicted_months'])
        month_diff = (newest_entry.year - oldest_entry.year) * 12 +\
            newest_entry.month - oldest_entry.month
        if month_diff < config_requirement:
            logger.warning(
                f"There are only predictions for the next {month_diff} months,"
                f" but the configuration requires {config_requirement}")

    def __style_prediciton_sheet(self, sheet):
        row = tuple(sheet.rows)[1]
        sheet['A2'].value = 'variation_id'
        for cell in row[1:]:
            if cell.value is not None:
                cell.fill = PatternFill("solid", fgColor="00FF0000")
        sheet.delete_rows(0, 1)

    def fetch_predictions_from_database(self):
        now = datetime.datetime.now()

        sheets = {}
        for sheet in self.sheet_config['sheets']:
            if sheet['sheet_type'] == 'prediction':
                sheets[sheet['sheet_name']] = sheet['origin_id']

        wb = load_workbook(self.sheet_config['file'])

        for sheet_name in sheets:
            future_predictions = and_(
                StatisticModel.origin_id == sheets[sheet_name],
                StatisticModel.predicted == 1,
                StatisticModel.quantity > 0,
                or_(
                    and_(
                        StatisticModel.year == now.year,
                        StatisticModel.month >= now.month
                    ),
                    StatisticModel.year > now.year
                )
            )
            predictions = self.session.query(StatisticModel).filter(
                future_predictions
            )
            db_df = pandas.read_sql(predictions.statement, self.session.bind)
            if len(db_df.index) == 0:
                logger.error("No inventory data located in database.")
                return

            (min_date, max_date) = get_prediction_min_max_date(
                source_df=db_df)
            self.__sufficient_predictions_check(oldest_entry=min_date,
                                                newest_entry=max_date)
            columns = get_multi_level_columns_by_date_range(
                date_range=(min_date, max_date))
            labels = get_date_labels_from_date_range(
                date_range=(min_date, max_date))

            unique_variations = self.session.query(
                func.distinct(StatisticModel.variation_id)
            ).filter(future_predictions)
            unique_variations = [x[0] for x in unique_variations.all()]

            sheet_df = pandas.DataFrame(columns=columns,
                                        index=unique_variations)

            for row in db_df.itertuples():
                sheet_df.loc[row.variation_id, (row.year, row.month)] =\
                    row.quantity
            sheet_df.fillna(0, inplace=True)
            sheet_df['variation_id'] = sheet_df.index
            variation_col = sheet_df.pop('variation_id')
            sheet_df.insert(0, 'variation_id', variation_col)
            sheet_df.reset_index(drop=True, inplace=True)

            sheet = wb[sheet_name]
            number_of_rows = sum(1 for e in sheet)
            sheet.delete_rows(0, number_of_rows)
            for row in dataframe_to_rows(sheet_df, index=False, header=True):
                sheet.append(row)

            for cell, new_value in zip(tuple(sheet.rows)[1][1:], labels):
                cell.value = new_value
            self.__style_prediciton_sheet(sheet=sheet)

            # Setup a named range with the same name as the sheet
            named_range_name = remove_special_chars(name=sheet_name)
            named_range_range = str(
                f"$A$1:$A${number_of_rows}")
            try:
                if wb.get_named_range(name=named_range_name):
                    del wb.defined_names[named_range_name]
            except KeyError:
                logger.info(f"Creating named range: {named_range_name}")
            wb.create_named_range(named_range_name, sheet, named_range_range)

            wb.save(self.sheet_config['file'])
