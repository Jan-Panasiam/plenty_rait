"""
Sub application of the Plenty RAIT tool with the purpose of writing
identification, stock and prediciton data to a `xlsx/xlsm` file.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from collections import Counter
from decimal import Decimal, InvalidOperation
import os
import sys
import configparser
import getpass
import argparse
import pathlib
import keyring
from loguru import logger

from update_spreadsheet.service import UpdateService


PROG_NAME = 'update_spreadsheet'
USER = os.getlogin()
if sys.platform == 'linux':
    BASE_PATH = os.path.join(
        '/', 'home', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )
elif sys.platform == 'win32':
    BASE_PATH = os.path.join(
        'C:\\', 'Users', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )

if not os.path.exists(BASE_PATH):
    pathlib.Path(BASE_PATH).mkdir(parents=True, exist_ok=True)

DB_CONFIG = os.path.join(BASE_PATH, 'db_config.ini')
CONFIG_FOLDER = os.path.join(BASE_PATH, PROG_NAME)
if not os.path.exists(CONFIG_FOLDER):
    pathlib.Path(CONFIG_FOLDER).mkdir(parents=True, exist_ok=True)

CONFIG_PATH = os.path.join(CONFIG_FOLDER, 'config.ini')

if not os.path.exists(DB_CONFIG):
    open(DB_CONFIG, 'a').close()

if not os.path.exists(CONFIG_PATH):
    open(CONFIG_PATH, 'a').close()


def setup_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog=PROG_NAME)
    parser.add_argument('--database', '--db', '-d', required=False,
                        help='Name of the database',
                        dest='db')
    parser.add_argument('--host', '-l', required=False,
                        help='The location of the database remote server or '
                             'localhost',
                        dest='host', default='localhost')
    parser.add_argument('--reset', '-r', required=False,
                        help='Reset the system keyring (Db user name & '
                        'password)',
                        action='store_true', dest='reset')
    parser.add_argument('--stock_update', '--stock', '-s', required=False,
                        help='Update the stock sheet with the data from the '
                        'database.', dest='update_stock', action='store_true')
    parser.add_argument('--identification_update', '--identification', '-i',
                        required=False,
                        help='Update the sku, ean, asin, name and variation id'
                        ' within the suggestion sheets with the data from the '
                        'database.', dest='update_identification',
                        action='store_true')
    parser.add_argument('--predictions_update', '--predictions', '-p',
                        required=False,
                        help='Update the predictions for orders and inventory '
                        'reorders with the data from the database.',
                        dest='update_predictions', action='store_true')
    return parser.parse_args()


def setup() -> tuple:
    """
    Handle command line arguments and prepare the connection to the database.

    Return:
                            [tuple]     -   parsed CLI arguments and
                                            DB connection string
    """
    args = setup_argparser()

    db_config = configparser.ConfigParser()
    db_config.read(DB_CONFIG)
    config = configparser.ConfigParser()
    config.read(CONFIG_PATH)

    (host, db_name) = get_database(config=db_config, args=args)
    (user, password) = get_database_credentials(reset=args.reset,
                                                host=host,
                                                database=db_name)
    connection_string = str(
        f"mysql+pymysql://{user}:{password}@{host}/{db_name}")
    return (args, connection_string, config)


def get_database(config: configparser.ConfigParser,
                 args: argparse.ArgumentParser) -> tuple:
    """
    Fetch the database connection path from either the config or CLI parameter.
    """
    if args.db:
        db_name = args.db
    elif config.has_option(section='DATABASE', option='database_name'):
        db_name = config['DATABASE']['database_name']
    else:
        logger.error("Database name required either from the database "
                     "configuration or from CLI argument.")
        sys.exit(1)

    if args.host:
        host = args.host
    elif config.has_option(section='DATABASE', option='host'):
        host = config['DATABASE']['host']
    else:
        logger.info("No host declared in the configuration or CLI "
                    "arguments, fall back to default 'localhost'")
        host = 'localhost'

    return (host, db_name)


def get_database_credentials(reset: bool, host: str, database: str) -> tuple:
    """
    Login credentials for the database are saved within a system wide keyring.
    Retrieve/Set/Reset the values of the keyring.
    """
    user = keyring.get_password('order_list_database_identity', 'user')
    password = keyring.get_password('order_list_database_identity', 'password')
    if not user or not password or reset:
        print(f"Login credentials for {database}@{host}:")
        user = input('Username: ')
        password = getpass.getpass()
        keyring.set_password('order_list_database_identity', 'user', user)
        keyring.set_password('order_list_database_identity', 'password',
                             password)
        return (user, password)
    return (user, password)


def validate_configuration(config: configparser.ConfigParser) -> bool:
    if not config.has_section(section='Spreadsheet'):
        logger.error("No [Spreadsheet] section found. Fill the configuration.")
        return False

    for section in ['Suggestion_sheet', 'Prediction_sheet', 'Stock_sheet']:
        if not config.has_section(section=section):
            logger.error(f"Add the {section} section to your configuration. "
                         "Example option: 'origin_4.01=Sheet name'")
            return False

    required_options = ['file_location', 'stock', 'incoming_orders']
    for option in required_options:
        if not config.has_option(section='Spreadsheet', option=option):
            logger.error(f"[Spreadsheet] section requires {option} option.")
            return False

    return True


def get_sheet_configuration(config: configparser.ConfigParser) -> dict:
    sheet_config = {
        'file': '',
        'sheets': [],
        'stock_columns': {},
        'predicted_months': 13
    }

    if not validate_configuration(config=config):
        return {}

    file_location = config['Spreadsheet']['file_location']
    if not pathlib.Path(file_location).is_file():
        logger.error(f"File {file_location} doesn't exist.")
        return {}

    sheet_config['file'] = file_location
    sheet_config['sheets'].append({
        'sheet_name': config['Spreadsheet']['stock'],
        'sheet_type': 'stock',
        'origin_id': None
    })
    sheet_config['sheets'].append({
        'sheet_name': config['Spreadsheet']['incoming_orders'],
        'sheet_type': 'incoming',
        'origin_id': None
    })

    origins = []
    for name, config_section in [('prediction', 'Prediction_sheet'),
                                 ('suggestion', 'Suggestion_sheet')]:
        for option in config[config_section]:
            if not option[0:6] == 'origin':
                continue
            key_elements = option.split('_')
            if len(key_elements) != 2:
                logger.error(f"Invalid {name} sheet configuration [{option}]. "
                             "Example option: 'origin_4.01=Sheet name'")
                return {}
            try:
                origin = Decimal(key_elements[1])
            except InvalidOperation:
                logger.error(f"Invalid origin id. [{option}]")
                return {}
            origins.append(origin)

            sheet_name = config[config_section][option]
            sheet_config['sheets'].append({
                'sheet_name': sheet_name,
                'sheet_type': name,
                'origin_id': origin
            })

    # Get the count of each origin and make sure that the counts are equal
    count_of_each_origin = list(Counter(origins).values())
    if len(set(count_of_each_origin)) != 1 or count_of_each_origin[0] != 2:
        logger.error("Each prediction sheet needs a matching suggestion "
                     "sheet.")
        return {}

    sheet_config['stock_columns']['variation_id'] = 'variation_id'
    for key, value in config['Stock_sheet'].items():
        sheet_config['stock_columns']['stock_' + key] = value

    if config.has_option(section='Prediction_sheet', option='months'):
        sheet_config['predicted_months'] = config['Prediction_sheet']['months']

    return sheet_config


def main():
    (args, connection_string, config) = setup()

    sheet_config = get_sheet_configuration(config=config)
    update_service = UpdateService(sheet_config=sheet_config)
    update_service.create_database_session(connection_string=connection_string)

    if args.update_stock:
        update_service.fetch_stock_from_database()
    if args.update_identification:
        update_service.fetch_identification_data_from_database()
    if args.update_predictions:
        update_service.fetch_predictions_from_database()
