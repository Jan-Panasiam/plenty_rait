# Plenty RAIT: Update spreadsheet tool

This tool is part of the Plentymarkets Reorder And Inventory Transfer tool, it is used to write the identification, stock and prediciton data from the database into an excel or libreoffice sheet.

## Installation

Either install with the plenty_rait installation script or seperatly with: `poetry install`

## Configuration

In the configuration file you declare the purpose of each sheet. The Plenty RAIT project provides an excel sheet with order suggestions for different origins, each suggestion sheet is connected to a prediction sheet that contains the predicted sales for the given origin. Let's say you want to have a sheet with suggestions for only origin 4.01, then you would create an `origin_4.01={Name of the sheet}` entry within `[Suggestion_sheet]` and `[Prediction_sheet]`.

You can configure warehouses to sheet columns by using the index of the warehouse column in the database. Example:
```bash
# Database column name: stock_warehouse_1
[Stock_sheet]
warehouse_1=Warehouse 1
# This connects the database column "stock_warehouse_1" with the sheet column "Warehouse 1"
```

Full example:
```bash
[Spreadsheet]
file_location=/path/to/order_list.xlsm
stock=Stock sheet
incoming_orders=Incoming order sheet

[Suggestion_sheet]
origin_-1=suggestion total sheet

[Prediction_sheet]
origin_-1=prediction total sheet
months=14

[Stock_sheet]
warehouse_1=Warehouse 1
warehouse_2=Warehouse 2
warehouse_3=FBA Warehouse
```

## Usage

When the tool is properly configured using it simply means:

```bash
# Update the identification data (ean, sku) and synchronize the variations
# on the sheet with the variations of the database
python3 -m update_spreadsheet -i
# Update the inventory data for all configured warehouses in the configuration
python3 -m update_spreadsheet -s
# Update the prediction data for the configured amount of months from the database
python3 -m update_spreadsheet -p
```
