from decimal import Decimal
import pytest
import configparser

from update_spreadsheet.cli import (
    get_sheet_configuration
)


def describe_get_sheet_configuration() -> None:
    def with_empty_config() -> None:
        config = configparser.ConfigParser()
        result = get_sheet_configuration(config=config)

        assert {} == result

    def with_no_suggestion_sheet_config() -> None:
        config = configparser.ConfigParser()
        config['Spreadsheet'] = {
            'file_location': '/usr/bin/ls',
            'stock': 'stock_sheet',
            'incoming_orders': 'incoming_sheet'
        }
        config['Prediction_sheet'] = {
            'origin_4': 'prediction_for_4'
        }
        config['Stock_sheet'] = {
            'warehouse_1': 'warehouse_1'
        }
        result = get_sheet_configuration(config=config)

        assert {} == result

    def with_no_prediction_sheet_config() -> None:
        config = configparser.ConfigParser()
        config['Spreadsheet'] = {
            'file_location': '/usr/bin/ls',
            'stock': 'stock_sheet',
            'incoming_orders': 'incoming_sheet'
        }
        config['Suggestion_sheet'] = {
            'origin_4': 'suggestion_for_4'
        }
        config['Stock_sheet'] = {
            'warehouse_1': 'warehouse_1'
        }
        result = get_sheet_configuration(config=config)

        assert {} == result

    def with_no_stock_sheet_config() -> None:
        config = configparser.ConfigParser()
        config['Spreadsheet'] = {
            'file_location': '/usr/bin/ls',
            'incoming_orders': 'incoming_sheet'
        }
        config['Prediction_sheet'] = {
            'origin_4': 'prediction_for_4'
        }
        config['Suggestion_sheet'] = {
            'origin_4': 'suggestion_for_4'
        }
        config['Stock_sheet'] = {
            'warehouse_1': 'warehouse_1'
        }
        result = get_sheet_configuration(config=config)

        assert {} == result

    def with_no_stock_column_config() -> None:
        config = configparser.ConfigParser()
        config['Spreadsheet'] = {
            'file_location': '/usr/bin/ls',
            'stock': 'stock_sheet',
            'incoming_orders': 'incoming_sheet'
        }
        config['Prediction_sheet'] = {
            'origin_4': 'prediction_for_4'
        }
        config['Suggestion_sheet'] = {
            'origin_4': 'suggestion_for_4'
        }
        result = get_sheet_configuration(config=config)

        assert {} == result

    def with_no_incoming_sheet_config() -> None:
        config = configparser.ConfigParser()
        config['Spreadsheet'] = {
            'file_location': '/usr/bin/ls',
            'stock': 'stock_sheet'
        }
        config['Prediction_sheet'] = {
            'origin_4': 'prediction_for_4'
        }
        config['Suggestion_sheet'] = {
            'origin_4': 'suggestion_for_4'
        }
        config['Stock_sheet'] = {
            'warehouse_1': 'warehouse_1'
        }
        result = get_sheet_configuration(config=config)

        assert {} == result

    def with_invalid_suggestion_option() -> None:
        config = configparser.ConfigParser()
        config['Spreadsheet'] = {
            'file_location': '/usr/bin/ls',
            'stock': 'stock_sheet',
            'incoming_orders': 'incoming_sheet'
        }
        config['Prediction_sheet'] = {
            'origin_4': 'prediction_for_4'
        }
        config['Suggestion_sheet'] = {
            'origin_a': 'suggestion_for_a'
        }
        config['Stock_sheet'] = {
            'warehouse_1': 'warehouse_1'
        }
        result = get_sheet_configuration(config=config)

        assert {} == result

    def with_invalid_match_between_suggestion_and_prediction() -> None:
        config = configparser.ConfigParser()
        config['Spreadsheet'] = {
            'file_location': '/usr/bin/ls',
            'stock': 'stock_sheet',
            'incoming_orders': 'incoming_sheet'
        }
        config['Prediction_sheet'] = {
            'origin_4': 'prediction_for_4'
        }
        config['Suggestion_sheet'] = {
            'origin_6': 'suggestion_for_6'
        }
        config['Stock_sheet'] = {
            'warehouse_1': 'warehouse_1'
        }
        result = get_sheet_configuration(config=config)

        assert {} == result

    def with_no_file_config() -> None:
        config = configparser.ConfigParser()
        config['Spreadsheet'] = {
            'stock': 'stock_sheet',
            'incoming_orders': 'incoming_sheet'
        }
        config['Prediction_sheet'] = {
            'origin_4': 'prediction_for_4'
        }
        config['Suggestion_sheet'] = {
            'origin_a': 'suggestion_for_a'
        }
        config['Stock_sheet'] = {
            'warehouse_1': 'warehouse_1'
        }
        result = get_sheet_configuration(config=config)

        assert {} == result

    def with_correct_config() -> None:
        config = configparser.ConfigParser()
        config['Spreadsheet'] = {
            'file_location': '/usr/bin/ls',
            'stock': 'stock_sheet',
            'incoming_orders': 'incoming_sheet'
        }
        config['Prediction_sheet'] = {
            'origin_4': 'prediction_for_4',
            'months': 12
        }
        config['Suggestion_sheet'] = {
            'origin_4': 'suggestion_for_4'
        }
        config['Stock_sheet'] = {
            'warehouse_1': 'warehouse_1',
            'warehouse_2': 'warehouse_2'
        }
        result = get_sheet_configuration(config=config)
        expect = {
            'file': '/usr/bin/ls',
            'sheets': [
                {
                    'sheet_name': 'stock_sheet',
                    'sheet_type': 'stock',
                    'origin_id': None
                },
                {
                    'sheet_name': 'incoming_sheet',
                    'sheet_type': 'incoming',
                    'origin_id': None
                },
                {
                    'sheet_name': 'prediction_for_4',
                    'sheet_type': 'prediction',
                    'origin_id': Decimal('4')
                },
                {
                    'sheet_name': 'suggestion_for_4',
                    'sheet_type': 'suggestion',
                    'origin_id': Decimal('4')
                }
            ],
            'stock_columns': {
                'variation_id': 'variation_id',
                'stock_warehouse_1': 'warehouse_1',
                'stock_warehouse_2': 'warehouse_2'
            },
            'predicted_months': '12'
        }

        assert expect == result

    def with_multiple_correct_config() -> None:
        config = configparser.ConfigParser()
        config['Spreadsheet'] = {
            'file_location': '/usr/bin/ls',
            'stock': 'stock_sheet',
            'incoming_orders': 'incoming_sheet'
        }
        config['Prediction_sheet'] = {
            'origin_4': 'prediction_for_4',
            'origin_5': 'prediction_for_5',
            'months': 12
        }
        config['Suggestion_sheet'] = {
            'origin_4': 'suggestion_for_4',
            'origin_5': 'suggestion_for_5'
        }
        config['Stock_sheet'] = {
            'warehouse_1': 'warehouse_1',
            'warehouse_2': 'warehouse_2'
        }
        result = get_sheet_configuration(config=config)
        expect = {
            'file': '/usr/bin/ls',
            'sheets': [
                {
                    'sheet_name': 'stock_sheet',
                    'sheet_type': 'stock',
                    'origin_id': None
                },
                {
                    'sheet_name': 'incoming_sheet',
                    'sheet_type': 'incoming',
                    'origin_id': None
                },
                {
                    'sheet_name': 'prediction_for_4',
                    'sheet_type': 'prediction',
                    'origin_id': Decimal('4')
                },
                {
                    'sheet_name': 'prediction_for_5',
                    'sheet_type': 'prediction',
                    'origin_id': Decimal('5')
                },
                {
                    'sheet_name': 'suggestion_for_4',
                    'sheet_type': 'suggestion',
                    'origin_id': Decimal('4')
                },
                {
                    'sheet_name': 'suggestion_for_5',
                    'sheet_type': 'suggestion',
                    'origin_id': Decimal('5')
                }
            ],
            'stock_columns': {
                'variation_id': 'variation_id',
                'stock_warehouse_1': 'warehouse_1',
                'stock_warehouse_2': 'warehouse_2'
            },
            'predicted_months': '12'
        }

        assert expect == result
