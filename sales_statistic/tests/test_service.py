import datetime
from decimal import Decimal
import pytest
from sqlalchemy import and_, func

from sales_statistic.service import (
    DataCollector
)
from sales_statistic.schema import (
    StatisticModel, DailyStatisticModel, AmazonOrdersModel,
    AmazonOrderItemsModel, OrdersModel, OrderItemsModel, IdentificationModel
)


def setup_database(session, sources: dict) -> None:
    try:
        session.add_all(sources['order'])
        session.add_all(sources['ama_order'])
        session.add_all(sources['order_item'])
        session.add_all(sources['ama_order_item'])
        session.add_all(sources['id_table'])
    except Exception:
        session.rollback()
    else:
        session.commit()


def setup_database_with_stats(session, sources: dict, stats: dict) -> None:
    setup_database(session=session, sources=sources)
    try:
        session.add_all(stats)
    except Exception:
        session.rollback()
    else:
        session.commit()


def marked_items(session, table, field: str) -> int:
    return session.query(
        table
    ).filter(
        getattr(table, field) != -1
    ).count()


@pytest.fixture
def amazon_orders():
    orders = [
        AmazonOrdersModel(  # 1
            amazon_id='303-12311512-12313', origin_id='104.02',
            purchase_date='2020-02-13'
        ),
        AmazonOrdersModel(  # 2
            amazon_id='303-12311512-12314', origin_id=-1,
            purchase_date='2019-12-31'
        ),
        AmazonOrdersModel(  # 3
            amazon_id='303-12311512-12315', origin_id='104.03',
            purchase_date='2020-03-15'
        ),
        AmazonOrdersModel(  # 4
            amazon_id='303-12311512-12316', origin_id='104.02',
            purchase_date='2019-04-13'
        ),
        AmazonOrdersModel(  # 5
            amazon_id='303-12311512-12317', origin_id='4.01',
            purchase_date='2020-05-13'
        ),
        AmazonOrdersModel(  # 6
            amazon_id='303-12311512-12318', origin_id='4.01',
            purchase_date='2020-05-18'
        ),
        AmazonOrdersModel(  # 7
            amazon_id='303-12311512-12319', origin_id='104.04',
            purchase_date='2020-06-13'
        )
    ]
    return orders


@pytest.fixture
def orders():
    orders = [
        OrdersModel(  # 1
            origin_id='1', netto_total=1, gross_total=1,
            creation_date='2020-02-13', payment_date='2020-02-13',
            delivery_date='2020-02-13', b2b=False,
            external_id='403-1243415-51423'
        ),
        OrdersModel(  # 2
            origin_id=-1, netto_total=1, gross_total=1,
            creation_date='2020-04-13', payment_date='2020-04-13',
            delivery_date='2020-04-13', b2b=False,
            external_id='12346'
        ),
        OrdersModel(  # 3
            origin_id='104.03', netto_total=1, gross_total=1,
            creation_date='2020-03-15', payment_date='2020-03-15',
            delivery_date='2020-03-15', b2b=False,
            external_id='303-12311512-12315'
        ),
        OrdersModel(  # 4
            origin_id='4', netto_total=1, gross_total=1,
            creation_date='2019-05-20', payment_date='2019-04-20',
            delivery_date='2019-05-20', b2b=False,
            external_id='12347'
        ),
        OrdersModel(  # 5
            origin_id='4.01', netto_total=1, gross_total=1,
            creation_date='2020-05-13', payment_date='2020-05-13',
            delivery_date='2020-05-13', b2b=False,
            external_id='303-12311512-12317'
        ),
        OrdersModel(  # 6
            origin_id='2', netto_total=1, gross_total=1,
            creation_date='2020-06-13', payment_date='2020-06-13',
            delivery_date='2020-06-13', b2b=False,
            external_id='12349'
        ),
        OrdersModel(  # 7
            origin_id='1', netto_total=1, gross_total=1,
            creation_date='2020-06-13', payment_date='2020-06-13',
            delivery_date='2020-06-13', b2b=False,
            external_id='21354'
        )
    ]
    return orders


@pytest.fixture
def amazon_order_items():
    order_items = [
        AmazonOrderItemsModel(
            order_id=1, amazon_id='303-12311512-12313',
            origin_id='104.02', variation_id=1234, asin='B051238590',
            quantity=1, ean='1234567891011'
        ),
        AmazonOrderItemsModel(
            order_id=1, amazon_id='303-12311512-12313',
            origin_id='104.02', variation_id=2345, asin='B051238690',
            quantity=1, ean='1234567891012'
        ),
        AmazonOrderItemsModel(
            order_id=2, amazon_id='303-12311512-12314',
            origin_id=-1, variation_id=3456, asin='B051238790',
            quantity=1, ean='1234567891013'
        ),
        AmazonOrderItemsModel(
            order_id=3, amazon_id='303-12311512-12315',
            origin_id='104.03', variation_id=1234, asin='B051238590',
            quantity=2, ean='1234567891011'
        ),
        AmazonOrderItemsModel(
            order_id=3, amazon_id='303-12311512-12315',
            origin_id='104.03', variation_id=2345, asin='B051238690',
            quantity=1, ean='1234567891012'
        ),
        AmazonOrderItemsModel(
            order_id=4, amazon_id='303-12311512-12316',
            origin_id='104.02', variation_id=1234, asin='B051238590',
            quantity=2, ean='1234567891011'
        ),
        AmazonOrderItemsModel(
            order_id=5, amazon_id='303-12311512-12317',
            origin_id='4.01', variation_id=1234, asin='B051238590',
            quantity=1, ean='1234567891011'
        ),
        AmazonOrderItemsModel(
            order_id=6, amazon_id='303-12311512-12318',
            origin_id='4.01', variation_id=1234, asin='B051238590',
            quantity=1, ean='1234567891011'
        ),
        AmazonOrderItemsModel(
            order_id=7, amazon_id='303-12311512-12319',
            origin_id='104.04', variation_id=4567, asin='B051238890',
            quantity=2, ean='1234567891014'
        )
    ]
    return order_items


@pytest.fixture
def order_items():
    order_items = [
        OrderItemsModel(
            order_id=1, external_id='403-1243415-51423',
            origin_id='1', variation_id=1234, item_id='1010020030',
            name='Testarticle_1', quantity=1, netto=1, gross=1,
            ean='1234567891011'
        ),
        OrderItemsModel(
            order_id=1, external_id='403-1243415-51423',
            origin_id='1', variation_id=2345, item_id='1010020040',
            name='Testarticle_2', quantity=1, netto=1, gross=1,
            ean='1234567891012'
        ),
        OrderItemsModel(
            order_id=2, external_id='12346',
            origin_id=-1, variation_id=3456, item_id='1010020050',
            name='Testarticle_3', quantity=1, netto=1, gross=1,
            ean='1234567891013'
        ),
        OrderItemsModel(
            order_id=3, external_id='303-12311512-12315',
            origin_id='104.03', variation_id=1234, item_id='1010020030',
            name='Testarticle_1', quantity=2, netto=1, gross=1,
            ean='1234567891011'
        ),
        OrderItemsModel(
            order_id=3, external_id='303-12311512-12315',
            origin_id='104.03', variation_id=2345, item_id='1010020040',
            name='Testarticle_2', quantity=1, netto=1, gross=1,
            ean='1234567891012'
        ),
        OrderItemsModel(
            order_id=4, external_id='12347',
            origin_id='4', variation_id=1234, item_id='1010020030',
            name='Testarticle_1', quantity=2, netto=1, gross=1,
            ean='1234567891011'
        ),
        OrderItemsModel(
            order_id=5, external_id='303-12311512-12317',
            origin_id='4.01', variation_id=1234, item_id='1010020030',
            name='Testarticle_1', quantity=1, netto=1, gross=1,
            ean='1234567891011'
        ),
        OrderItemsModel(
            order_id=6, external_id='12349',
            origin_id='2', variation_id=4567, item_id='1010020030',
            name='Testarticle_1', quantity=1, netto=1, gross=1,
            ean='1234567891014'
        ),
        OrderItemsModel(
            order_id=7, external_id='21354',
            origin_id='1', variation_id=4567, item_id='1010020060',
            name='Testarticle_4', quantity=1, netto=1, gross=1,
            ean='1234567891014'
        )
    ]
    return order_items


@pytest.fixture
def identification_table():
    id_table = [
        IdentificationModel(
            variation_id=1234, ean='1234567891011', sku='test_sku_1',
            asin_all='B051238590'
        ),
        IdentificationModel(
            variation_id=2345, ean='1234567891012', sku='test_sku_2',
            asin_all='B051238690'
        ),
        IdentificationModel(
            variation_id=3456, ean='1234567891013', sku='test_sku_3',
            asin_all='B051238790'
        ),
        IdentificationModel(
            variation_id=4567, ean='1234567891014', sku='test_sku_4',
            asin_all='B051238890'
        )
    ]
    return id_table


@pytest.fixture
def source_tables(amazon_orders: list, amazon_order_items: list,
                  orders: list, order_items: list,
                  identification_table: list) -> dict:
    sources = {
        'ama_order': amazon_orders,
        'ama_order_item': amazon_order_items,
        'order': orders,
        'order_item': order_items,
        'id_table': identification_table
    }
    return sources


@pytest.fixture
def source_tables_with_stats(source_tables: dict) -> dict:
    amazon_order_items = source_tables['ama_order_item']
    order_items = source_tables['order_item']
    amazon_order_items[0].statistic_entry = 3
    amazon_order_items[1].statistic_entry = 4
    amazon_order_items[3].statistic_entry = 7
    amazon_order_items[4].statistic_entry = 8
    amazon_order_items[5].statistic_entry = 1
    amazon_order_items[6].statistic_entry = -1
    amazon_order_items[7].statistic_entry = -1
    amazon_order_items[8].statistic_entry = -1
    order_items[0].statistic_entry = 5
    order_items[1].statistic_entry = 6
    order_items[3].statistic_entry = 7
    order_items[4].statistic_entry = 8
    order_items[5].statistic_entry = 2
    order_items[6].statistic_entry = -1
    order_items[7].statistic_entry = -1
    order_items[8].statistic_entry = -1

    return source_tables


@pytest.fixture
def source_tables_with_daily_stats(source_tables: dict) -> dict:
    amazon_order_items = source_tables['ama_order_item']
    order_items = source_tables['order_item']
    amazon_order_items[0].daily_statistic_entry = 3
    amazon_order_items[1].daily_statistic_entry = 4
    amazon_order_items[3].daily_statistic_entry = 7
    amazon_order_items[4].daily_statistic_entry = 8
    amazon_order_items[5].daily_statistic_entry = 1
    order_items[0].daily_statistic_entry = 5
    order_items[1].daily_statistic_entry = 6
    order_items[3].daily_statistic_entry = 7
    order_items[4].daily_statistic_entry = 8
    order_items[5].daily_statistic_entry = 2

    return source_tables


@pytest.fixture
def existing_statistic_entries() -> dict:
    statistic_items = [
        StatisticModel(
            variation_id=1234, origin_id=104.02, month=4, year=2019,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=4, month=5, year=2019,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=104.02, month=2, year=2020,
            quantity=1, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=104.02, month=2, year=2020,
            quantity=1, predicted=False,ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=1, month=2, year=2020,
            quantity=1, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=1, month=2, year=2020,
            quantity=1, predicted=False,ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=104.03, month=3, year=2020,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=104.03, month=3, year=2020,
            quantity=1, predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=4.01, month=5, year=2020,
            quantity=1, predicted=True, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=4567, origin_id=2, month=6, year=2020,
            quantity=3, predicted=True, ean='1234567891014'
        ),
        StatisticModel(
            variation_id=4567, origin_id=104.04, month=6, year=2020,
            quantity=2, predicted=True, ean='1234567891014'
        ),
        StatisticModel(
            variation_id=4567, origin_id=1, month=6, year=2020,
            quantity=3, predicted=True, ean='1234567891014'
        )
    ]
    return statistic_items


@pytest.fixture
def existing_total_statistic_entries() -> dict:
    statistic_items = [
        StatisticModel(
            variation_id=1234, origin_id=-1, month=4, year=2019,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=-1, month=5, year=2019,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=-1, month=2, year=2020,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=-1, month=2, year=2020,
            quantity=2, predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=-1, month=3, year=2020,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=-1, month=3, year=2020,
            quantity=1, predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=-1, month=5, year=2020,
            quantity=1, predicted=True, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=-1, month=6, year=2020,
            quantity=1, predicted=True, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=4567, origin_id=-1, month=6, year=2020,
            quantity=7, predicted=True, ean='1234567891014'
        )
    ]
    return statistic_items


@pytest.fixture
def existing_daily_statistic_entries() -> dict:
    statistic_items = [
        DailyStatisticModel(
            variation_id=1234, origin_id=104.02, month=4, year=2019,
            day=13, quantity=2, ean='1234567891011'
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4, month=5, year=2019,
            day=20, quantity=2, ean='1234567891011'
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=104.02, month=2, year=2020,
            day=13, quantity=1, ean='1234567891011'
        ),
        DailyStatisticModel(
            variation_id=2345, origin_id=104.02, month=2, year=2020,
            day=13, quantity=1, ean='1234567891012'
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=1, month=2, year=2020,
            day=13, quantity=1, ean='1234567891011'
        ),
        DailyStatisticModel(
            variation_id=2345, origin_id=1, month=2, year=2020,
            day=13, quantity=1, ean='1234567891012'
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=104.03, month=3, year=2020,
            day=15, quantity=2, ean='1234567891011'
        ),
        DailyStatisticModel(
            variation_id=2345, origin_id=104.03, month=3, year=2020,
            day=15, quantity=1, ean='1234567891012'
        )
    ]
    return statistic_items


@pytest.fixture
def dup_tables() -> dict:
    sources = {
        'ama_order': [
            AmazonOrdersModel(  # 1
                amazon_id='303-12345678-12345', origin_id='4.01',
                purchase_date='2020-02-13'
            ),
            AmazonOrdersModel(  # 2
                amazon_id='303-12345678-12346', origin_id='4.02',
                purchase_date='2020-02-14'
            )
        ],
        'ama_order_item': [
            AmazonOrderItemsModel(
                order_id=1, amazon_id='303-12345678-12345',
                origin_id='4.01', variation_id=1234, asin='B051238590',
                quantity=1, ean='1234567891011'
            ),
            # Duplicate
            AmazonOrderItemsModel(
                order_id=1, amazon_id='303-12345678-12345',
                origin_id='4.01', variation_id=1234, asin='B051238590',
                quantity=1, ean='1234567891011'
            ),
            AmazonOrderItemsModel(
                order_id=2, amazon_id='303-12345678-12346',
                origin_id='4.02', variation_id=1235, asin='B051238591',
                quantity=1
            )
        ],
        'order': [
            OrdersModel(  # 1
                origin_id='4.01', netto_total=1, gross_total=1,
                creation_date='2020-02-13', payment_date='2020-02-13',
                delivery_date='2020-02-13', b2b=False,
                external_id='303-12345678-12345'
            )
        ],
        'order_item': [
            OrderItemsModel(
                order_id=1, external_id='303-12345678-12345',
                origin_id='4.01', variation_id=1234, item_id='1010020060',
                name='Testarticle_1', quantity=1, netto=1, gross=1,
                ean='1234567891011'
            )
        ],
        'id_table': []
    }
    return sources


@pytest.fixture
def statistic_items():
    statistic_items = [
        StatisticModel(
            variation_id=1234, origin_id=4.01, month=8, year=2020, quantity=3,
            predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=4.01, month=7, year=2020, quantity=3,
            predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=2, month=6, year=2020, quantity=2,
            predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=1, month=5, year=2020, quantity=1,
            predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=3456, origin_id=104.01, month=4, year=2020,
            quantity=3, predicted=False, ean='1234567891013'
        )
    ]
    return statistic_items


@pytest.fixture
def total_statistic_sample_entries():
    statistic_items = [
        StatisticModel(
            variation_id=1234, origin_id=1.00, month=9, year=2018, quantity=3,
            predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=0, month=9, year=2018, quantity=1,
            predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=4.01, month=9, year=2018, quantity=4,
            predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=104.01, month=3, year=2019,
            quantity=5, predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=2345, origin_id=4.01, month=3, year=2019, quantity=2,
            predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=2345, origin_id=1.00, month=3, year=2019, quantity=1,
            predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=3456, origin_id=104.01, month=10, year=2019,
            quantity=2, predicted=False, ean='1234567891013'
        ),
        StatisticModel(
            variation_id=3456, origin_id=4.01, month=10, year=2019, quantity=1,
            predicted=False, ean='1234567891013'
        ),
        StatisticModel(
            variation_id=4567, origin_id=2.01, month=2, year=2020, quantity=3,
            predicted=True, ean='1234567891014'
        )
    ]
    return statistic_items


@pytest.mark.dependency()
def test_db_lookup(db_session, source_tables: dict) -> None:
    """
    Verify that all of the used tables work properly.
    """
    stat = StatisticModel(variation_id=1234, month=11, year=2020, quantity=5,
                          predicted=False, ean='1234567891011')
    db_session.add(stat)
    assert 1 == db_session.query(StatisticModel).count()

    db_session.add_all(source_tables['ama_order'])
    assert 7 == db_session.query(AmazonOrdersModel).count()

    db_session.add_all(source_tables['ama_order_item'])
    assert 9 == db_session.query(AmazonOrderItemsModel).count()

    db_session.add_all(source_tables['order'])
    assert 7 == db_session.query(OrdersModel).count()

    db_session.add_all(source_tables['order_item'])
    assert 9 == db_session.query(OrderItemsModel).count()


def test_db_is_rolled_back(db_session) -> None:
    assert 0 == db_session.query(StatisticModel).count()
    assert 0 == db_session.query(AmazonOrdersModel).count()
    assert 0 == db_session.query(AmazonOrderItemsModel).count()
    assert 0 == db_session.query(OrdersModel).count()
    assert 0 == db_session.query(OrderItemsModel).count()


@pytest.mark.dependency(depends=['test_db_lookup'])
def describe_find_duplicate() -> None:
    def with_item_only_in_orders(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        data_collector = DataCollector(session=db_session)
        result = data_collector._DataCollector__find_duplicate(
            variation_id=1234, external_id='403-1243415-51423')

        assert (-1, -1) == result

    def with_item_in_both(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        data_collector = DataCollector(session=db_session)
        result = data_collector._DataCollector__find_duplicate(
            variation_id=1234, external_id='303-12311512-12315')

        assert (4, 4) == result

    def with_item_unknown(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        data_collector = DataCollector(session=db_session)
        result = data_collector._DataCollector__find_duplicate(
            variation_id=1234, external_id='unknown_id')

        assert (-1, -1) == result

    def with_duplicate_in_single_table_match(db_session,
                                             dup_tables: dict) -> None:
        setup_database(session=db_session, sources=dup_tables)
        data_collector = DataCollector(session=db_session)
        result = data_collector._DataCollector__find_duplicate(
            variation_id=1234, external_id='303-12345678-12345'
        )

        assert (1, 1) == result

    def with_duplicate_in_single_table_no_match(db_session,
                                                dup_tables: dict) -> None:
        setup_database(session=db_session, sources=dup_tables)
        data_collector = DataCollector(session=db_session)
        result = data_collector._DataCollector__find_duplicate(
            variation_id=1235, external_id='303-12345678-12346'
        )

        assert (-1, -1) == result


def describe_find_statistic_entry() -> None:
    def with_empty_table(db_session) -> None:
        data_collector = DataCollector(session=db_session)
        stat_entry = data_collector._DataCollector__find_statistic_entry(
            order_date=datetime.datetime(2020, 8, 10, 0, 0, 0),
            variation_id=1234, origin_id=4.01
        )

        assert stat_entry is None

    def with_existing_entries(db_session, statistic_items: list) -> None:
        try:
            db_session.add_all(statistic_items)
        except Exception:
            db_session.rollback()
        db_session.commit()

        data_collector = DataCollector(session=db_session)
        stat_entry = data_collector._DataCollector__find_statistic_entry(
            order_date=datetime.datetime(2020, 8, 13, 0, 0, 0),
            variation_id=1234, origin_id=4.01
        )
        assert statistic_items[0] == stat_entry

        stat_entry = data_collector._DataCollector__find_statistic_entry(
            order_date=datetime.datetime(2020, 7, 15, 0, 0, 0),
            variation_id=2345, origin_id=4.01
        )
        assert statistic_items[1] == stat_entry

        stat_entry = data_collector._DataCollector__find_statistic_entry(
            order_date=datetime.datetime(2020, 6, 20, 0, 0, 0),
            variation_id=1234, origin_id=2
        )
        assert statistic_items[2] == stat_entry

        stat_entry = data_collector._DataCollector__find_statistic_entry(
            order_date=datetime.datetime(2020, 5, 14, 0, 0, 0),
            variation_id=2345, origin_id=1
        )
        assert statistic_items[3] == stat_entry

        stat_entry = data_collector._DataCollector__find_statistic_entry(
            order_date=datetime.datetime(2020, 4, 11, 0, 0, 0),
            variation_id=3456, origin_id=104.01
        )
        assert statistic_items[4] == stat_entry
        stat_entry = data_collector._DataCollector__find_statistic_entry(
            order_date=datetime.datetime(2020, 3, 12, 0, 0, 0),
            variation_id=1234, origin_id=104.01
        )
        assert stat_entry is None


def describe_fetch_statistic_data() -> None:
    def with_no_entries_within_range(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        stat_entries_before = db_session.query(StatisticModel).count()

        data_collector = DataCollector(session=db_session)
        start = datetime.datetime.strptime('2018-10-01', "%Y-%m-%d")
        end = datetime.datetime.strptime('2018-11-01', "%Y-%m-%d")
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        assert stat_entries_before == stat_entries_after

    def with_unique_orders(db_session, source_tables: dict) -> None:
        """
        Get unique orders from the amazon order items and the order items
        table.
        Expected result:
            When the origin is valid exactly one entry should be added
            to the statistics table.
        """
        setup_database(session=db_session, sources=source_tables)

        # There is only one order in april 2019 (AmazonOrderItemsModel)
        stat_entries_before = db_session.query(StatisticModel).count()
        data_collector = DataCollector(session=db_session)
        start = datetime.datetime.strptime('2019-04-01', "%Y-%m-%d")
        end = datetime.datetime.strptime('2019-05-01', "%Y-%m-%d")
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        assert (stat_entries_before + 1) == stat_entries_after

        # There is only one order in may 2019 (OrderItemsModel)
        stat_entries_before = db_session.query(StatisticModel).count()
        start = datetime.datetime.strptime('2019-05-01', "%Y-%m-%d")
        end = datetime.datetime.strptime('2019-06-01', "%Y-%m-%d")
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        assert (stat_entries_before + 1) == stat_entries_after

        # There is only one order in december 2019 (AmazonOrderItemsModel)
        # With invalid an origin id (-1)
        stat_entries_before = db_session.query(StatisticModel).count()
        start = datetime.datetime.strptime('2019-12-01', "%Y-%m-%d")
        end = datetime.datetime.strptime('2020-01-01', "%Y-%m-%d")
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        assert stat_entries_before == stat_entries_after

        # There is only one order in april 2020 (OrderItemsModel)
        # With invalid an origin id (-1)
        stat_entries_before = db_session.query(StatisticModel).count()
        start = datetime.datetime.strptime('2020-04-01', "%Y-%m-%d")
        end = datetime.datetime.strptime('2020-05-01', "%Y-%m-%d")
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        assert stat_entries_before == stat_entries_after

    def with_overlapping_orders(db_session, source_tables: dict) -> None:
        """
        Get orders that are present on both tables.
        Expected result:
            only one of both is added to the statistics table
            but both have a reference to the statistics entry.
        """
        setup_database(session=db_session, sources=source_tables)

        # There are two orders in march 2020 (OrderItemsModel &
        # AmazonOrderItemsModel), only one of both should be added but
        # there has to be one row per table afterwards with statistic_entry
        # filled
        stat_entries_before = db_session.query(StatisticModel).count()
        data_collector = DataCollector(session=db_session)
        start = datetime.datetime.strptime('2020-03-01', "%Y-%m-%d")
        end = datetime.datetime.strptime('2020-04-01', "%Y-%m-%d")
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()

        assert (stat_entries_before + 2) == stat_entries_after

        amazon_with_stat = db_session.query(AmazonOrderItemsModel).filter(
            AmazonOrderItemsModel.statistic_entry > 0
        )
        order_with_stat = db_session.query(OrderItemsModel).filter(
            OrderItemsModel.statistic_entry > 0
        )
        for order in db_session.query(OrderItemsModel).all():
            print(order)

        assert 4 == (amazon_with_stat.count() + order_with_stat.count())

    def with_different_origins(db_session, source_tables: dict) -> None:
        """
        Get orders that contain the same variation but from different
        marketplaces.
        Expected result:
            3 orders containing the same variation from different marketplaces
            should create 3 new statistic entries
        """
        setup_database(session=db_session, sources=source_tables)

        stat_entries_before = db_session.query(StatisticModel).count()
        data_collector = DataCollector(session=db_session)
        start = datetime.datetime.strptime('2020-06-01', "%Y-%m-%d")
        end = datetime.datetime.strptime('2020-06-30', "%Y-%m-%d")
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()

        assert (stat_entries_before + 3) == stat_entries_after

    def with_multiple_orders(db_session, source_tables: dict) -> None:
        """
        Get orders that contain the same variation, created in the same month.
        Expected result:
            3 orders containing the same variation from the same marketplace
            from the same month, should create exactly one statistics entry
            with the total sum of the sub quantities.
        """
        setup_database(session=db_session, sources=source_tables)

        # There are 2 orders on the amazon orders table and 1 on the orders
        # table for may 2020. With a total quantity of 4.
        stat_entries_before = db_session.query(StatisticModel).count()
        amazon_marked_items_before = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='statistic_entry')
        orders_marked_items_before = marked_items(
            session=db_session, table=OrderItemsModel,
            field='statistic_entry')

        data_collector = DataCollector(session=db_session)
        start = datetime.datetime.strptime('2020-05-01', "%Y-%m-%d")
        end = datetime.datetime.strptime('2020-05-30', "%Y-%m-%d")
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        amazon_marked_items_after = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='statistic_entry')
        orders_marked_items_after = marked_items(
            session=db_session, table=OrderItemsModel,
            field='statistic_entry')

        assert (stat_entries_before + 1) == stat_entries_after
        assert (amazon_marked_items_before + 2) == amazon_marked_items_after
        assert (orders_marked_items_before + 1) == orders_marked_items_after

        total = db_session.query(
            func.sum(StatisticModel.quantity).label("total")
        ).filter(
                and_(
                    StatisticModel.month == 5,
                    StatisticModel.year == 2020,
                )
        ).first().total

        assert total == 2

    def with_multiple_orders_daily(db_session, source_tables: dict) -> None:
        """
        Get orders that contain the same variation, created in the same month.
        Use the daily target to get the total quantities for each day.
        Expected result:
            3 orders containing the same variation from the same marketplace
            from the same month but with two daily statistic entries for two
            different days, the quantity of the statistic entries has to match
            the quantity of the items.
        """
        setup_database(session=db_session, sources=source_tables)

        # There are 2 orders on the amazon orders table and 1 on the orders
        # table for may 2020. With a total quantity of 4. For the 13th there
        # is a total quantity of 3 and for the 18th a quantity of 1.
        daily_entries_before = db_session.query(DailyStatisticModel).count()
        monthly_entries_before = db_session.query(StatisticModel).count()
        amazon_marked_items_before = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='daily_statistic_entry')
        orders_marked_items_before = marked_items(
            session=db_session, table=OrderItemsModel,
            field='daily_statistic_entry')

        data_collector = DataCollector(session=db_session)
        data_collector.target = 'daily'
        start = datetime.datetime.strptime('2020-05-01', "%Y-%m-%d")
        end = datetime.datetime.strptime('2020-05-30', "%Y-%m-%d")
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        daily_entries_after = db_session.query(DailyStatisticModel).count()
        monthly_entries_after = db_session.query(StatisticModel).count()
        amazon_marked_items_after = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='daily_statistic_entry')
        orders_marked_items_after = marked_items(
            session=db_session, table=OrderItemsModel,
            field='daily_statistic_entry')

        assert (daily_entries_before + 2) == daily_entries_after
        assert monthly_entries_before == monthly_entries_after
        assert (amazon_marked_items_before + 2) == amazon_marked_items_after
        assert (orders_marked_items_before + 1) == orders_marked_items_after

        total = db_session.query(
            func.sum(DailyStatisticModel.quantity).label("total")
        ).filter(
                and_(
                    DailyStatisticModel.day == 13,
                    DailyStatisticModel.month == 5,
                    DailyStatisticModel.year == 2020,
                )
        ).first().total

        assert total == 1

        total = db_session.query(
            func.sum(DailyStatisticModel.quantity).label("total")
        ).filter(
                and_(
                    DailyStatisticModel.day == 18,
                    DailyStatisticModel.month == 5,
                    DailyStatisticModel.year == 2020,
                )
        ).first().total

        assert total == 1

    def with_overwritting_predictions(db_session, source_tables_with_stats,
                                      existing_statistic_entries):
        """
        Predicted entries should be overwritten by actual entries, when
        new items are analyzed. In this test case, we provide existing
        statistic entries, with predicted entries, the input items overlap
        with those entries, which means all predicted entries have to be
        overwritten.
        """
        setup_database_with_stats(session=db_session,
                                  sources=source_tables_with_stats,
                                  stats=existing_statistic_entries)

        stats_entries_before = db_session.query(StatisticModel).count()
        stats_entries_predicted_before = db_session.query(
            StatisticModel
        ).filter(StatisticModel.predicted.is_(True)).count()
        amazon_marked_orders_before = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='statistic_entry')
        orders_marked_before = marked_items(
            session=db_session, table=OrderItemsModel,
            field='statistic_entry')
        total_before = db_session.query(
            func.sum(StatisticModel.quantity).label("total")
        ).first().total
        data_collector = DataCollector(session=db_session)
        data_collector.target = 'monthly'
        start = datetime.datetime.strptime('2019-04-01', "%Y-%m-%d")
        end = datetime.datetime.strptime('2020-07-01', "%Y-%m-%d")
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stats_entries_after = db_session.query(StatisticModel).count()
        stats_entries_predicted_after = db_session.query(
            StatisticModel
        ).filter(StatisticModel.predicted.is_(True)).count()
        amazon_marked_orders_after = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='statistic_entry')
        orders_marked_after = marked_items(
            session=db_session, table=OrderItemsModel,
            field='statistic_entry')
        total_after = db_session.query(
            func.sum(StatisticModel.quantity).label("total")
        ).first().total

        assert stats_entries_before == stats_entries_after
        assert (stats_entries_predicted_before - 4) ==\
            stats_entries_predicted_after
        assert (amazon_marked_orders_before + 3) == amazon_marked_orders_after
        assert (orders_marked_before + 3) == orders_marked_after
        assert (total_before - 3) == total_after


def setup_total_stats_test(session, statistic_items: list) -> tuple:
    try:
        session.add_all(statistic_items)
    except Exception:
        session.rollback()
    else:
        session.commit()

    monthly_entries_before = session.query(StatisticModel).count()
    data_collector = DataCollector(session=session)
    data_collector.target = 'monthly'
    return (data_collector, monthly_entries_before)


def describe_total_statistic_per_variation_and_date_unit() -> None:
    def with_no_entries_in_date_range(
            db_session, total_statistic_sample_entries: list) -> None:
        start_date = datetime.date(2018, 11, 10)
        end_date = datetime.date(2019, 2, 15)
        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, statistic_items=total_statistic_sample_entries)
        data_collector.total_statistic_per_variation_and_date_unit(
            start_date=start_date, end_date=end_date)
        entries_after = db_session.query(StatisticModel).count()

        assert entries_before == entries_after

    def with_single_variation(
            db_session, total_statistic_sample_entries: list) -> None:
        start_date = datetime.date(2019, 9, 5)
        end_date = datetime.date(2019, 11, 1)
        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, statistic_items=total_statistic_sample_entries)
        data_collector.total_statistic_per_variation_and_date_unit(
            start_date=start_date, end_date=end_date)
        entries_after = db_session.query(StatisticModel).count()

        assert (entries_before + 1) == entries_after

        total = db_session.query(
            StatisticModel.quantity
        ).filter(
                and_(
                    StatisticModel.month == 10,
                    StatisticModel.year == 2019,
                    StatisticModel.variation_id == 3456,
                    StatisticModel.origin_id == Decimal('-1')
                )
        ).first().quantity

        assert 3 == total

    def with_multiple_variations(
            db_session, total_statistic_sample_entries: list) -> None:
        start_date = datetime.date(2018, 9, 1)
        end_date = datetime.date(2019, 11, 20)
        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, statistic_items=total_statistic_sample_entries)
        data_collector.total_statistic_per_variation_and_date_unit(
            start_date=start_date, end_date=end_date)
        entries_after = db_session.query(StatisticModel).count()

        assert (entries_before + 3) == entries_after
        total_1 = db_session.query(
            StatisticModel.quantity
        ).filter(
                and_(
                    StatisticModel.month == 9,
                    StatisticModel.year == 2018,
                    StatisticModel.variation_id == 1234,
                    StatisticModel.origin_id == Decimal('-1')
                )
        ).first().quantity

        assert 8 == total_1

        total_2 = db_session.query(
            StatisticModel.quantity
        ).filter(
                and_(
                    StatisticModel.month == 3,
                    StatisticModel.year == 2019,
                    StatisticModel.variation_id == 2345,
                    StatisticModel.origin_id == Decimal('-1')
                )
        ).first().quantity

        assert 8 == total_2

        total_3 = db_session.query(
            StatisticModel.quantity
        ).filter(
                and_(
                    StatisticModel.month == 10,
                    StatisticModel.year == 2019,
                    StatisticModel.variation_id == 3456,
                    StatisticModel.origin_id == Decimal('-1')
                )
        ).first().quantity

        assert 3 == total_3

    def with_predicted_values_in_date_range(
            db_session, total_statistic_sample_entries: list) -> None:
        start_date = datetime.date(2019, 6, 1)
        end_date = datetime.date(2020, 4, 3)
        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, statistic_items=total_statistic_sample_entries)
        data_collector.total_statistic_per_variation_and_date_unit(
            start_date=start_date, end_date=end_date)
        entries_after = db_session.query(StatisticModel).count()

        assert (entries_before + 1) == entries_after


def describe_cleanup_statistics():
    def with_no_invalid_statistic_entries(
            db_session, source_tables_with_stats: dict,
            existing_statistic_entries: dict, monkeypatch) -> None:

        monkeypatch.setattr('builtins.input', lambda _: "y")
        setup_database_with_stats(session=db_session,
                                  sources=source_tables_with_stats,
                                  stats=existing_statistic_entries)

        expected = [[x.variation_id, x.origin_id, x.quantity, x.year, x.month, x.ean]
                    for x in db_session.query(StatisticModel).all()]
        data_collector = DataCollector(session=db_session)
        data_collector.cleanup_statistics(print_summary=True)

        result = [[x.variation_id, x.origin_id, x.quantity, x.year, x.month, x.ean]
                  for x in db_session.query(StatisticModel).all()]

        assert expected == result

    def with_invalid_statistic_entries(
            db_session, source_tables_with_stats: dict,
            existing_statistic_entries: dict, monkeypatch) -> None:

        monkeypatch.setattr('builtins.input', lambda _: "y")
        stats = existing_statistic_entries
        stats[0].variation_id = 2234
        stats[2].variation_id = 3234
        setup_database_with_stats(session=db_session,
                                  sources=source_tables_with_stats,
                                  stats=stats)

        expected_id_set = [1234, 2345, 4567]
        stats_before = db_session.query(StatisticModel)
        stats_count_before = stats_before.count()
        id_set_before = sorted(set([x.variation_id for x in stats_before]))

        data_collector = DataCollector(session=db_session)
        data_collector.cleanup_statistics(print_summary=True)

        stats_after = db_session.query(StatisticModel)
        stats_count_after = stats_after.count()
        id_set_after = sorted(set([x.variation_id for x in stats_after]))

        assert stats_count_after == stats_count_before
        assert id_set_after != id_set_before
        assert expected_id_set == id_set_after

    def with_invalid_daily_statistic_entries(
            db_session, source_tables_with_daily_stats: dict,
            existing_daily_statistic_entries: dict, monkeypatch) -> None:

        monkeypatch.setattr('builtins.input', lambda _: "y")
        stats = existing_daily_statistic_entries
        stats[0].variation_id = 2234
        stats[2].variation_id = 3234
        setup_database_with_stats(session=db_session,
                                  sources=source_tables_with_daily_stats,
                                  stats=stats)

        expected_id_set = [1234, 2345]
        daily_before = db_session.query(DailyStatisticModel)
        daily_count_before = daily_before.count()
        id_set_before = sorted(set([x.variation_id for x in daily_before]))

        data_collector = DataCollector(session=db_session)
        data_collector.target = 'daily'
        data_collector.cleanup_statistics(print_summary=True)

        daily_after = db_session.query(DailyStatisticModel)
        daily_count_after = daily_after.count()
        id_set_after = sorted(set([x.variation_id for x in daily_after]))

        assert daily_count_after == daily_count_before
        assert id_set_after != id_set_before
        assert expected_id_set == id_set_after

    def with_invalid_total_statistic_entries(
            db_session, source_tables_with_stats: dict,
            existing_statistic_entries: dict,
            existing_total_statistic_entries: dict,
            monkeypatch) -> None:

        monkeypatch.setattr('builtins.input', lambda _: "y")
        for entry in [x for x in existing_statistic_entries
                      if x.variation_id == 1234]:
            entry.variation_id = 2234
        for entry in [x for x in existing_statistic_entries
                      if x.variation_id == 2345]:
            entry.variation_id = 3345

        total_stats = existing_total_statistic_entries
        for entry in [x for x in total_stats if x.variation_id == 1234]:
            entry.variation_id = 2234
        for entry in [x for x in total_stats if x.variation_id == 2345]:
            entry.variation_id = 3345
        existing_statistic_entries += total_stats
        setup_database_with_stats(session=db_session,
                                  sources=source_tables_with_stats,
                                  stats=existing_statistic_entries)

        expected_id_set = [1234, 2345, 4567]
        totals_before = db_session.query(StatisticModel).filter(
            StatisticModel.origin_id == -1)
        totals_count_before = totals_before.count()
        id_set_before = sorted(set([x.variation_id for x in totals_before]))

        data_collector = DataCollector(session=db_session)
        data_collector.cleanup_statistics(print_summary=True)

        totals_after = db_session.query(StatisticModel).filter(
            StatisticModel.origin_id == -1)
        totals_count_after = totals_after.count()
        id_set_after = sorted(set([x.variation_id for x in totals_after]))

        assert totals_count_after == totals_count_before
        assert id_set_after != id_set_before
        assert expected_id_set == id_set_after
