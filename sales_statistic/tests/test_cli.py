import sys
import pytest
import argparse
import datetime

from sales_statistic.cli import (
    setup_argparser
)


def describe_setup_argparser() -> None:
    def with_good_arguments():
        sys.argv[1:] = ['-s', '2020-11-10', '-e', '2020-11-14']
        args = setup_argparser()
        expect_start = datetime.datetime.strptime("2020-11-10", "%Y-%m-%d")
        expect_end = datetime.datetime.strptime("2020-11-14", "%Y-%m-%d")
        args = vars(args)

        assert expect_start == args['start'] and expect_end == args['end']

    def with_bad_separator():
        sys.argv[1:] = ['-s', '2019/11/10', '-e', '2020/11/14']
        args = setup_argparser()

        assert args is None

    def with_bad_date_order():
        sys.argv[1:] = ['-s', '10-11-2020', '-e', '14-11-2020']
        args = setup_argparser()

        assert args is None

    def with_missing_start():
        sys.argv[1:] = ['-e', '2020-11-14']
        args = setup_argparser()

        assert args is None

    def with_missing_end():
        sys.argv[1:] = ['-s', '2020-11-10']
        args = setup_argparser()

        assert args is None

    def with_no_arguments():
        sys.argv[1:] = []
        args = setup_argparser()

        assert args is None
