# sales_statistic

Sort raw data from a MariaDB database into monthly sales tables with different criteria.

Specific the order origin ID prefix for Amazon FBM & FBA on your Plentymarkets system (eg. AmazonFBA Germany 104.01, prefix: 104) within the configuration under {HOME}/.config/plenty_rait_tool/sales_statistic/config.ini.

ini
```
[GENERAL]
amazon_origin_prefix=4,104
```


## Setting up the test environment

In order to execute the database tests, you need a test database user and a test database. In order to create those do the following:
```bash
mysql -u root -p

create database test_monthly_statistics;
grant all privileges on test_monthly_statistics.* TO 'db_test_user'@'localhost' identified by '1234';
flush privileges;
```
