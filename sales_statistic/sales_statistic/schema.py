from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy_utils import generic_repr
from sqlalchemy import (
    Column, Integer, Text, String, Date, Numeric, Boolean, create_engine,
    ForeignKey, func
)

Base = declarative_base()


@generic_repr
class StatisticModel(Base):
    __tablename__ = "statistics"

    id = Column(Integer, primary_key=True)
    variation_id = Column(Integer)
    ean = Column(String(13))
    origin_id = Column(Numeric(precision=8, scale=2))
    month = Column(Integer)
    year = Column(Integer)
    quantity = Column(Integer)
    predicted = Column(Boolean)

    @hybrid_property
    def date_value(self):
        return func.str_to_date(func.concat(self.year, "-", self.month, "-",
                                            "1"), "%Y-%m-%d")


@generic_repr
class DailyStatisticModel(Base):
    __tablename__ = "daily_statistics"

    id = Column(Integer, primary_key=True)
    variation_id = Column(Integer)
    ean = Column(String(13))
    origin_id = Column(Numeric(precision=8, scale=2))
    day = Column(Integer)
    month = Column(Integer)
    year = Column(Integer)
    quantity = Column(Integer)

    @hybrid_property
    def date_value(self):
        return func.str_to_date(func.concat(self.year, "-", self.month, "-",
                                            self.day), "%Y-%m-%d")


@generic_repr
class AmazonOrdersModel(Base):
    __tablename__ = "amazon_orders"

    id = Column(Integer, primary_key=True, nullable=False)
    amazon_id = Column(String(length=25), nullable=False)
    origin_id = Column(Numeric(precision=8, scale=2))
    purchase_date = Column(Date)
    netto_total = Column(Numeric(precision=8, scale=2))
    gross_total = Column(Numeric(precision=8, scale=2))
    b2b = Column(Boolean)
    order_items = relationship("AmazonOrderItemsModel",
                               backref="amazon_orders")


@generic_repr
class AmazonOrderItemsModel(Base):
    __tablename__ = "amazon_order_items"

    id = Column(Integer, primary_key=True, nullable=False)
    order_id = Column(Integer, ForeignKey(AmazonOrdersModel.id),
                      nullable=False)
    amazon_id = Column(Text, nullable=False)
    origin_id = Column(Numeric(precision=8, scale=2))
    variation_id = Column(Integer, nullable=False)
    ean = Column(String(13))
    asin = Column(String(length=10), nullable=False)
    netto = Column(Numeric(precision=8, scale=2))
    gross = Column(Numeric(precision=8, scale=2))
    quantity = Column(Integer, nullable=False)
    statistic_entry = Column(Integer, nullable=True, default=-1)
    daily_statistic_entry = Column(Integer, nullable=True, default=-1)
    order = relationship("AmazonOrdersModel",
                         foreign_keys="AmazonOrderItemsModel.order_id",
                         lazy="joined")


@generic_repr
class OrdersModel(Base):
    __tablename__ = "orders"

    id = Column(Integer, primary_key=True, nullable=False)
    origin_id = Column(Numeric(precision=8, scale=2), nullable=False)
    netto_total = Column(Numeric(precision=8, scale=2), nullable=False)
    gross_total = Column(Numeric(precision=8, scale=2), nullable=False)
    creation_date = Column(Date, nullable=False)
    payment_date = Column(Date)
    delivery_date = Column(Date)
    b2b = Column(Boolean, nullable=False)
    external_id = Column(Text)
    order_items = relationship("OrderItemsModel", backref="orders")


@generic_repr
class OrderItemsModel(Base):
    __tablename__ = "order_items"

    id = Column(Integer, primary_key=True, nullable=False)
    order_id = Column(Integer, ForeignKey(OrdersModel.id), nullable=False)
    external_id = Column(Text)
    origin_id = Column(Numeric(precision=8, scale=2), nullable=False)
    variation_id = Column(Integer, nullable=False)
    ean = Column(String(13))
    item_id = Column(Integer, nullable=False)
    name = Column(Text)
    quantity = Column(Integer, nullable=False)
    netto = Column(Numeric(precision=8, scale=2), nullable=False)
    gross = Column(Numeric(precision=8, scale=2), nullable=False)
    statistic_entry = Column(Integer, nullable=True, default=-1)
    daily_statistic_entry = Column(Integer, nullable=True, default=-1)
    order = relationship("OrdersModel",
                         foreign_keys="OrderItemsModel.order_id",
                         lazy="joined")


@generic_repr
class IdentificationModel(Base):
    __tablename__ = "identification"

    variation_id = Column(Integer, primary_key=True, autoincrement=False)
    ean = Column(String(13))
    sku = Column(Text(50))
    asin_all = Column(String(10))
    name = Column(String(300))


class DataAccessLayer:
    def __init__(self):
        self.connection = ''
        self.engine = None
        self.Session = None
        self.session = None

    def connect(self):
        self.engine = create_engine(self.connection)
        Base.metadata.bind = self.engine
        Base.metadata.create_all(self.engine)
        self.session = sessionmaker(bind=self.engine)


dal = DataAccessLayer()
