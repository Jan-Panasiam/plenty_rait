import os
import sys
import datetime
import configparser
import argparse
import keyring
import getpass
from loguru import logger

from sales_statistic.service import DataCollector


PROG_NAME = 'sales_statistic'
USER = os.getlogin()
if sys.platform == 'linux':
    BASE_PATH = os.path.join(
        '/', 'home', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )
elif sys.platform == 'win32':
    BASE_PATH = os.path.join(
        'C:\\', 'Users', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )

DB_CONFIG = os.path.join(BASE_PATH, 'db_config.ini')
CONFIG_FOLDER = os.path.join(BASE_PATH, PROG_NAME)
if not os.path.exists(CONFIG_FOLDER):
    os.mkdir(CONFIG_FOLDER)

CONFIG_PATH = os.path.join(CONFIG_FOLDER, 'config.ini')

if not os.path.exists(DB_CONFIG):
    open(DB_CONFIG, 'a').close()

if not os.path.exists(CONFIG_PATH):
    open(CONFIG_PATH, 'a').close()


def check_arguments(args: argparse.Namespace) -> bool:
    if args.cleanup:
        return True
    if not args.start or not args.end:
        return False
    arguments = vars(args)
    for key, value in arguments.items():
        if key not in ['start', 'end']:
            continue
        try:
            arguments[key] = datetime.datetime.strptime(value, "%Y-%m-%d")
        except ValueError:
            logger.error(f"{key} date invalid, required: YYYY-MM-DD")
            return False

    return True


def check_config(config: configparser.ConfigParser) -> bool:
    sections = {'DATABASE': ['host', 'database_name']}

    for section in sections:
        if not config.has_section(section=section):
            logger.error(f"Configuration requires a [{section}] section.")
            return False

        for option in sections[section]:
            if not config.has_option(section=section, option=option):
                logger.error(f"Configuration requires a {option} option, "
                             f"within the [{section}] section.")
                return False

    return True


def get_database_credentials(reset: bool,
                             config: configparser.ConfigParser) -> tuple:
    user = keyring.get_password('order_list_database_identity', 'user')
    password = keyring.get_password('order_list_database_identity', 'password')
    if not user or not password or reset:
        print(f"Login credentials for {config['DATABASE']['database']}@"
              f"{config['DATABASE']['host']}:")
        user = input('Username: ')
        password = getpass.getpass()
        keyring.set_password('order_list_database_identity', 'user', user)
        keyring.set_password('order_list_database_identity', 'password',
                             password)
        return (user, password)
    return (user, password)


def setup_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog=PROG_NAME)
    parser.add_argument('-v', '--verbose', required=False,
                        help='Additional output for debugging',
                        dest='verbose', action='store_true')
    parser.add_argument('--start', '-s', '--from', '-f', required=False,
                        help='Pick the start date of the range to be pulled.'
                             '[YYYY-MM-DD]',
                        dest='start')
    parser.add_argument('--end', '-e', '--to', '-t', required=False,
                        help='Pick the end date of the range to be pulled.'
                             '[YYYY-MM-DD]',
                        dest='end')
    parser.add_argument('--reset', '-r', required=False,
                        help='Reset the system keyring',
                        action='store_true', dest='reset')
    parser.add_argument('--create_tables', '-c', required=False,
                        help="Create the 'statistics' and 'daily_statistics' "
                             "tables, when they don't already exist.",
                        action='store_true', dest='tables')
    parser.add_argument('--with_daily_statistic', '--with_daily', '--daily',
                        required=False, help='Fill the daily_statistics table',
                        action='store_true', dest='daily')
    parser.add_argument('--with_monthly_statistic', '--with_monthly',
                        '--monthly', required=False,
                        help='Fill the statistics table with monthly entries',
                        action='store_true', dest='monthly')
    parser.add_argument('--cleanup', '--clean', '--cleanup_statistics',
                        required=False, help='Find and replace deprecated '
                        'variation IDs', action='store_true', dest='cleanup')
    args = parser.parse_args()

    if not check_arguments(args=args):
        return None
    return args


def main():
    db_config = configparser.ConfigParser()
    db_config.read(DB_CONFIG)
    config = configparser.ConfigParser()
    config.read(CONFIG_PATH)

    args = setup_argparser()
    if not args:
        sys.exit(1)

    if not check_config(config=db_config):
        logger.error("Invalid or missing configuration.")
        sys.exit(1)

    (user, password) = get_database_credentials(reset=args.reset,
                                                config=config)
    host = db_config['DATABASE']['host']
    database = db_config['DATABASE']['database_name']
    connection_string = str(
        f'mysql+pymysql://{user}:{password}@{host}/{database}')
    if (config.has_section(section='GENERAL') and config.has_option(
            section='GENERAL', option='amazon_origin_prefix')):
        data_collector = DataCollector(
            amazon_origin_prefix=config['GENERAL']['amazon_origin_prefix'])
    else:
        data_collector = DataCollector()
    data_collector.create_database_session(connection_string=connection_string)
    if args.tables:
        data_collector.create_tables()

    if args.cleanup:
        if args.monthly:
            data_collector.cleanup_statistics(print_summary=True)
        if args.daily:
            data_collector.target = 'daily'
            data_collector.cleanup_statistics(print_summary=True)
        sys.exit(0)

    if args.monthly:
        data_collector.fetch_statistic_data(start_date=args.start,
                                            end_date=args.end)
        data_collector.total_statistic_per_variation_and_date_unit(
            start_date=args.start, end_date=args.end)
    if args.daily:
        data_collector.target = 'daily'
        data_collector.fetch_statistic_data(start_date=args.start,
                                            end_date=args.end)
