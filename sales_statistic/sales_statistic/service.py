"""
Summarize sales data from multiple database tables into daily and monthly
sales.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import datetime
from decimal import Decimal
import importlib
from sqlalchemy import and_, func
from sqlalchemy.orm import scoped_session, sessionmaker, Query
from sqlalchemy.sql import case
import tqdm
from loguru import logger

from sales_statistic.schema import (
    dal, AmazonOrdersModel, OrdersModel, AmazonOrderItemsModel,
    OrderItemsModel, StatisticModel, DailyStatisticModel,
    IdentificationModel
)


class DataCollector:
    """
    Collect the order item entries from the amazon and plentymarkets order
    items.
    """
    def __init__(self, session=None, amazon_origin_prefix='4,104'):
        self.session = session
        self.amazon_origin_prefix = amazon_origin_prefix
        self.target = 'monthly'

    def create_database_session(self, connection_string: str) -> None:
        dal.connection = connection_string
        dal.connect()

        dal.Session = scoped_session(sessionmaker(bind=dal.engine))
        dal.session = dal.Session()
        dal.Session.registry.clear()

        self.session = dal.session

    def create_tables(self) -> None:
        for table in ['statistics', 'daily_statistics']:
            if not dal.engine.dialect.has_table(dal.engine, table):
                table_model = importlib.import_module('schema')
                ORMTable = getattr(table_model, table)
                ORMTable.__table__.create(bind=dal.engine, checkfirst=True)

    def cleanup_statistics(self, print_summary: bool):
        """
        Replace deprecated variation IDs within monthly or daily statistics.

        The variation ID of a product can change, for example when a product
        was deleted and recreated. In such a case, the variation IDs of the
        statistic entries point to an invalid variation ID.
        The clean up of the database, has to be performed in a specific order:
        - Replace the variation ID on the deprecated identification table entry
        - Clean up the tables for the Plentymarkets and Amazon order items
        - Clean up the statistic entries

        This routine will try to replace deprecated variation IDs for
        daily/monthly/monthly total/monthly predicted entries.
        Before writing the changes to the database, a list of pending
        changes can be viewed and the changes can be declined by setting
        @print_summary to True.

        Parameters:
            print_summary[bool] -   Ask user to approve any pending changes
        """
        variations = self.session.query(IdentificationModel.variation_id)
        if self.target == 'monthly':
            statistic_target = StatisticModel
            statistic_filter = and_(
                StatisticModel.variation_id.notin_(variations),
                StatisticModel.origin_id != -1
            )
        else:
            statistic_target = DailyStatisticModel
            statistic_filter = DailyStatisticModel.variation_id.notin_(
                variations)
        stats_entries = self.session.query(
            statistic_target
        ).filter(statistic_filter)

        if stats_entries.count() == 0:
            logger.info(f"No clean up required for {self.target} statistics.")
            return

        summary = []
        ean_dict = {}
        variation_dict = {}

        for entry in stats_entries:
            ean_dict[str(entry.variation_id)] = entry.ean
            entry_id = entry.id
            old_var_id = entry.variation_id
            new_var_id = self.session.query(
                    IdentificationModel
                ).filter(IdentificationModel.ean == entry.ean).one()
            new_var_id = new_var_id.variation_id
            variation_dict[old_var_id] = new_var_id
            summary.append({
                'id': entry_id,
                'old': old_var_id,
                'new': new_var_id
            })

        self.session.query(statistic_target).filter(
            statistic_target.variation_id.in_(ean_dict.keys())
        ).update(
            {
                'variation_id':
                case(variation_dict, value=statistic_target.variation_id)
            }, synchronize_session=False
        )

        if not print_summary:
            self.session.commit()
            return

        logger.info("Summary of pending changes")
        for entry in summary:
            if self.target == 'monthly':
                table = 'StatisticModel'
            else:
                table = 'DailyStatisticModel'
            logger.info(f"Database table: {table} id: {entry['id']} "
                        f"({entry['old']} -> {entry['new']})")

        accept = ''
        while (accept != 'n' and accept != 'y'):
            accept = input(f'Write {len(summary)} changes to DB?(y/n)').lower()
            if accept == 'n':
                self.session.rollback()
                return
        self.session.commit()

    def __find_duplicate(self, variation_id: int, external_id: str) -> int:
        """
        Check if an order was created on both the orders as well as the
        amazon orders table.

        Parameter:
            variation_id        [int]   :   PlentyMarkets ID for a variation
            external_id         [str]   :   ID of the marketplace order

        Return:
                                [tuple] :   ID of the order item and amazon
                                            order item
                                            (order_item.id, amazon_item.id)
        """
        amazon_order_items = self.session.query(
            AmazonOrderItemsModel.id,
            func.count(AmazonOrderItemsModel.id).label('amount')
        ).filter(
            and_(
                AmazonOrderItemsModel.amazon_id.like(external_id),
                AmazonOrderItemsModel.variation_id.like(variation_id)
            )
        ).first()

        order_items = self.session.query(
            OrderItemsModel.id, func.count(OrderItemsModel.id).label('amount')
        ).filter(
            and_(
                OrderItemsModel.external_id.like(external_id),
                OrderItemsModel.variation_id.like(variation_id)
            )
        ).first()

        if amazon_order_items.amount > 1 or order_items.amount > 1:
            logger.warning("Duplicate order items within the same table for "
                           f"ID: {external_id} and variation: {variation_id}."
                           f"\namazon_items: {amazon_order_items.amount}\n"
                           f"order_items: {order_items.amount}")

        if amazon_order_items.amount == 0 or order_items.amount == 0:
            return (-1, -1)

        return (order_items.id, amazon_order_items.id)

    def __is_amazon_order(self, origin_id: float) -> bool:
        """
        Check if the given Plentymarkets origin ID is associated with Amazon.

        Parameter:
            origin_id           [float]     -   Origin ID from Plentymarkets
                                                representing a marketplace

        Return:
                                [bool]      -   True if it is a amazon origin
                                                False if not
        """
        return int(origin_id) in [
            int(x) for x in self.amazon_origin_prefix.split(',')
        ]

    def __find_statistic_entry(self, order_date: datetime,
                               variation_id: int, origin_id: int) -> None:
        if isinstance(order_date, datetime.datetime):
            order_date = order_date.date()
        if self.target == 'daily':
            stat = self.session.query(DailyStatisticModel).filter(
                and_(
                    DailyStatisticModel.date_value == order_date,
                    DailyStatisticModel.variation_id == variation_id,
                    DailyStatisticModel.origin_id == origin_id
                )
            )
        elif self.target == 'monthly':
            stat = self.session.query(StatisticModel).filter(
                and_(
                    StatisticModel.month == order_date.month,
                    StatisticModel.year == order_date.year,
                    StatisticModel.variation_id == variation_id,
                    StatisticModel.origin_id == origin_id
                )
            )
        if stat.count() > 1 or stat.count() == 0:
            return None
        return stat.one()

    def __find_total_statistic_entry(self, entry, local_entries: list) -> bool:
        if self.target == 'monthly':
            query = self.session.query(StatisticModel).filter(
                StatisticModel.variation_id == entry.variation_id,
                StatisticModel.month == entry.month,
                StatisticModel.year == entry.year,
                StatisticModel.origin_id == Decimal('-1'),
                StatisticModel.predicted.is_(False)
            ).count()
        elif self.target == 'daily':
            query = self.session.query(DailyStatisticModel).filter(
                DailyStatisticModel.variation_id == entry.variation_id,
                DailyStatisticModel.day == entry.day,
                DailyStatisticModel.month == entry.month,
                DailyStatisticModel.year == entry.year,
                DailyStatisticModel.origin_id == Decimal('-1'),
            ).count()

        if query > 0:
            return True

        for local in local_entries:
            if local.variation_id != entry.variation_id:
                continue
            if self.target == 'daily':
                if (local.day != entry.day or local.month != entry.month or
                        local.year != entry.year):
                    continue
            elif self.target == 'monthly':
                if local.month != entry.month or local.year != entry.year:
                    continue
            return True

        return False

    def __get_total_quantity(self, entry) -> int:
        if self.target == 'monthly':
            query = self.session.query(
                # StatisticModel
                func.sum(StatisticModel.quantity).label('total')
            ).filter(
                StatisticModel.variation_id == entry.variation_id,
                StatisticModel.month == entry.month,
                StatisticModel.year == entry.year,
                StatisticModel.predicted.is_(False)
            ).first()
        elif self.target == 'daily':
            query = self.session.query(
                func.sum(DailyStatisticModel.quantity).label('total')
            ).filter(
                DailyStatisticModel.variation_id == entry.variation_id,
                DailyStatisticModel.day == entry.day,
                DailyStatisticModel.month == entry.month,
                DailyStatisticModel.year == entry.year,
            ).first()
        if query:
            return query.total
        return 0

    def __get_daily_statistics_entries_within_date_range(
            self, start: datetime.datetime, end: datetime.datetime) -> Query:
        items = self.session.query(
            DailyStatisticModel
        ).filter(
            and_(
                DailyStatisticModel.date_value <= end,
                DailyStatisticModel.date_value >= start
            )
        )
        return items

    def __get_statistics_entries_within_date_range(
            self, start: datetime.datetime, end: datetime.datetime,
            exclude_origin: list = None) -> Query:
        if not exclude_origin:
            exclude_origin = []

        items = self.session.query(
            StatisticModel
        ).filter(
            and_(
                StatisticModel.predicted.is_(False),
                StatisticModel.origin_id.notin_(exclude_origin),
                StatisticModel.date_value <= end,
                StatisticModel.date_value >= start
            )
        )
        return items

    def __create_stat_entry_from_order(self, order_item, order_date):
        order_month = order_date.strftime("%m")
        order_year = order_date.strftime("%Y")
        if self.target == 'monthly':
            return self.__create_stat_entry(
                variation_id=order_item.variation_id,
                ean=order_item.ean,
                origin_id=order_item.origin_id,
                month=order_month, year=order_year,
                quantity=order_item.quantity
            )
        order_day = order_date.strftime("%d")
        return self.__create_stat_entry(
            variation_id=order_item.variation_id,
            ean=order_item.ean,
            origin_id=order_item.origin_id,
            day=order_day, month=order_month, year=order_year,
            quantity=order_item.quantity
        )

    def __create_stat_entry(self, variation_id: int, origin_id: Decimal,
                            month: int, year: int, quantity: int, ean: str,
                            day: int = 1):
        if self.target == 'monthly':
            return StatisticModel(
                variation_id=variation_id,
                ean=ean,
                origin_id=origin_id,
                month=month,
                year=year,
                quantity=quantity,
                predicted=False
            )
        return DailyStatisticModel(
            variation_id=variation_id,
            ean=ean,
            origin_id=origin_id,
            day=day,
            month=month,
            year=year,
            quantity=quantity
        )

    def __get_order_items(self, table: str,
                          start_date: datetime, end_date: datetime) -> Query:
        if table == 'orders':
            order_table = OrdersModel
            item_table = OrderItemsModel
            date_field = 'creation_date'
        elif table == 'amazon_orders':
            order_table = AmazonOrdersModel
            item_table = AmazonOrderItemsModel
            date_field = 'purchase_date'

        if self.target == 'daily':
            stats_field = 'daily_statistic_entry'
        elif self.target == 'monthly':
            stats_field = 'statistic_entry'

        return self.session.query(item_table).join(order_table).filter(
            and_(
                getattr(order_table, date_field) >= start_date,
                getattr(order_table, date_field) <= end_date,
                getattr(item_table, stats_field) == -1,
                item_table.variation_id != -1,
                item_table.origin_id != -1
            )
        )

    def __add_orders_from_table(self, table: str,
                                start_date: datetime.datetime,
                                end_date: datetime.datetime) -> None:
        if table not in ['orders', 'amazon_orders']:
            logger.error("Invalid table name, valid names: "
                         "['orders', 'amazon_orders']")
            return

        if self.target == 'daily':
            stat_entry_field = 'daily_statistic_entry'
        if self.target == 'monthly':
            stat_entry_field = 'statistic_entry'

        order_items = self.__get_order_items(table=table,
                                             start_date=start_date,
                                             end_date=end_date)

        logger.info(f"Processing {order_items.count()} rows on table {table}")

        for order_item in tqdm.tqdm(order_items.all()):
            if table == 'orders':
                order_date = order_item.order.creation_date
            elif table == 'amazon_orders':
                order_date = order_item.order.purchase_date

            current_stats_entry = self.__find_statistic_entry(
                order_date=order_date, variation_id=order_item.variation_id,
                origin_id=order_item.origin_id)
            create_new_entry = False if current_stats_entry else True

            if create_new_entry:
                statistic_entry = self.__create_stat_entry_from_order(
                    order_item=order_item, order_date=order_date)
                self.session.add(statistic_entry)
                # Push to database transaction buffer in order to get an ID
                self.session.flush()
                setattr(order_item, stat_entry_field,
                        statistic_entry.id)
            else:
                if (self.target == 'daily' or
                        (self.target == 'monthly' and
                         current_stats_entry.predicted is False)):
                    current_stats_entry.quantity += order_item.quantity
                    setattr(order_item, stat_entry_field,
                            current_stats_entry.id)
                else:
                    current_stats_entry.quantity = order_item.quantity
                    setattr(order_item, stat_entry_field,
                            current_stats_entry.id)
                    current_stats_entry.predicted = False

            # The orders table is checked first, which means that all
            # duplicates are already marked, when we check the amazon table.
            if (self.__is_amazon_order(origin_id=order_item.origin_id) and
                    table != 'amazon_orders'):
                external_id = order_item.external_id
                (order_id, amazon_id) = self.__find_duplicate(
                    variation_id=order_item.variation_id,
                    external_id=external_id
                )
                if order_id != -1 and amazon_id != -1:
                    item = self.session.query(
                        AmazonOrderItemsModel
                    ).filter(
                            AmazonOrderItemsModel.id == amazon_id
                    )
                    if create_new_entry:
                        setattr(item.first(), stat_entry_field,
                                statistic_entry.id)
                    else:
                        setattr(item.first(), stat_entry_field,
                                current_stats_entry.id)

        # Call commit last to persist all changes from DB transaction buffer
        self.session.commit()

    def fetch_statistic_data(self, start_date: datetime.datetime,
                             end_date: datetime.datetime) -> None:
        """
        Get a set of unique orders, that were created during the given
        date-range.

        The statistic table contains three columns, that are used to filter:
            - month
            - year
            - origin_id

        Parameter:
            start_date          [datetime] :    Start of the date range
            end_date            [datetime] :    End of the date range
        """
        self.__add_orders_from_table(table='orders', start_date=start_date,
                                     end_date=end_date)
        self.__add_orders_from_table(table='amazon_orders',
                                     start_date=start_date, end_date=end_date)
        self.session.commit()

    def total_statistic_per_variation_and_date_unit(
            self, start_date: datetime.datetime,
            end_date: datetime.datetime) -> None:
        """
        Sum up all entries for a given date-unit (month-year, day-month-year)
        and variation.

        The origin_id for all origins is '-1'.

        Parameter:
            start_date          [datetime] :    Start of the date range
            end_date            [datetime] :    End of the date range
        """
        if self.target == 'monthly':
            query = self.__get_statistics_entries_within_date_range(
                start=start_date, end=end_date, exclude_origin=[Decimal('-1')])
        elif self.target == 'daily':
            query = self.__get_daily_statistics_entries_within_date_range(
                start=start_date, end=end_date)

        logger.info("Calculating the total for each variation date unit "
                    f"combination over {query.count()} rows.")

        new_entries = []
        for entry in tqdm.tqdm(query.all()):
            if self.__find_total_statistic_entry(entry=entry,
                                                 local_entries=new_entries):
                continue
            total_qty = self.__get_total_quantity(entry=entry)
            if not total_qty:
                if self.target == 'monthly':
                    date = datetime.date(year=entry.year, month=entry.month,
                                         day=1)
                elif self.target == 'daily':
                    date = datetime.date(year=entry.year, month=entry.month,
                                         day=entry.day)
                logger.warning("No valid sum for variation "
                               f"{entry.variation_id} @ {date}")
                continue
            if self.target == 'monthly':
                new_entry = self.__create_stat_entry(
                    variation_id=entry.variation_id, origin_id=Decimal('-1'),
                    month=entry.month, year=entry.year, quantity=total_qty,
                    ean=entry.ean)
            if self.target == 'daily':
                new_entry = self.__create_stat_entry(
                    variation_id=entry.variation_id, origin_id=Decimal('-1'),
                    day=entry.day, month=entry.month, year=entry.year,
                    quantity=total_qty, ean=entry.ean)
            new_entries.append(new_entry)

        self.session.add_all(new_entries)
        self.session.commit()
