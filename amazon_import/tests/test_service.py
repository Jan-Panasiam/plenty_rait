import copy
import datetime
from decimal import Decimal
import pandas
import pytest
from pandas.testing import assert_frame_equal

from amazon_import.service import DataImport, DataCollector
from amazon_import.schema import (
    AmazonOrdersModel, AmazonOrderItemsModel, IdentificationModel, StockModel
)

def setup_order_database(session, sources: dict) -> None:
    try:
        session.add_all(sources['orders'])
        session.add_all(sources['order_items'])
        session.add_all(sources['ident'])
    except Exception:
        session.rollback()
    else:
        session.commit()


def setup_stock_database(session, sources: dict) -> None:
    try:
        session.add_all(sources['ident'])
        session.add_all(sources['stock'])
    except Exception:
        session.rollback()
    else:
        session.commit()


@pytest.fixture
def origin_map() -> dict:
    origin_map = {
        'FBA': {
            'Amazon.de': {'B2C': '104.01', 'B2B': '104.21'},
            'Amazon.co.uk': {'B2C': '104.02', 'B2B': '104.22'},
            'Amazon.com': {'B2C': '104.03', 'B2B': '104.23'},
        },
        'FBM': {
            'Amazon.de': {'B2C': '4.01', 'B2B': '4.21'},
            'Amazon.co.uk': {'B2C': '4.02', 'B2B': '4.22'},
            'Amazon.com': {'B2C': '4.03', 'B2B': '4.23'},
        }
    }
    return origin_map


@pytest.fixture
def amazon_order_report() -> pandas.DataFrame:
    orders = [
        # 0. Amazon FBA Germany
        ['123-1234567-1234567', '', '2019-12-31T17:28:35+00:00', '',
         'Shipped', 'Amazon', 'Amazon.de', '', '', '', 'test_name_1',
         'test_sku_1', 'TESTASIN01', '', '2', 'EUR', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
        # 1. Amazon FBM Germany
        ['123-1234567-1234566', '', '2019-12-23T17:28:35+00:00', '',
         'Shipped', 'Merchant', 'Amazon.de', '', '', '', 'test_name_2',
         'test_sku_2', 'TESTASIN02', '', '1', 'EUR', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
        # 2. Amazon FBA USA
        ['123-1234567-1234565', '', '2019-12-24T17:28:35+00:00', '',
         'Shipped', 'Amazon', 'Amazon.com', '', '', '', 'test_name_3',
         'test_sku_3', 'TESTASIN03', '', '4', 'USD', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
        # 3. Amazon FBM USA 1/3
        ['123-1234567-1234564', '', '2019-12-26T17:28:35+00:00', '',
         'Shipped', 'Merchant', 'Amazon.com', '', '', '', 'test_name_4',
         'test_sku_4', 'TESTASIN04', '', '1', 'USD', '12.3', '2.35',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
        # 4. Amazon FBM USA 2/3
        ['123-1234567-1234564', '', '2019-12-26T17:28:35+00:00', '',
         'Shipped', 'Merchant', 'Amazon.com', '', '', '', 'test_name_4_2',
         'test_sku_4_2', 'TESTASIN41', '', '1', 'USD', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
        # 5. Amazon FBM USA 3/3
        ['123-1234567-1234564', '', '2019-12-26T17:28:35+00:00', '',
         'Shipped', 'Merchant', 'Amazon.com', '', '', '', 'test_name_4_3',
         'test_sku_4_3', 'TESTASIN42', '', '3', 'USD', '30', '6',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
        # 6. Amazon FBA UK
        ['123-1234567-1234563', '', '2019-12-8T17:28:35+00:00', '',
         'Shipped', 'Amazon', 'Amazon.co.uk', '', '', '', 'test_name_5',
         'test_sku_5', 'TESTASIN05', '', '2', 'GBP', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
        # 7. Amazon FBM UK - already exists in DB
        ['123-1234567-1234562', '', '2019-12-14T17:28:35+00:00', '',
         'Shipped', 'Merchant', 'Amazon.co.uk', '', '', '', 'test_name_6',
         'test_sku_6', 'TESTASIN06', '', '1', 'GBP', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
        # 8. Amazon FBA Germany B2B
        ['123-1234567-1234561', '', '2019-12-1T17:28:35+00:00', '',
         'Shipped', 'Amazon', 'Amazon.de', '', '', '', 'test_name_7',
         'test_sku_7', 'TESTASIN07', '', '1', 'EUR', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'true', '', '', ''],
        # 9. Amazon FBM Germany B2B
        ['123-1234567-1234560', '', '2019-12-7T17:28:35+00:00', '',
         'Shipped', 'Merchant', 'Amazon.de', '', '', '', 'test_name_8',
         'test_sku_8', 'TESTASI010', '', '3', 'EUR', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'true', '', '', ''],
        # 10. Amazon FBA USA B2B
        ['123-1234567-1234559', '', '2019-12-17T17:28:35+00:00', '',
         'Shipped', 'Amazon', 'Amazon.com', '', '', '', 'test_name_9',
         'test_sku_9', 'TESTASI011', '', '1', 'USD', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'true', '', '', ''],
        # 11. Amazon FBM USA B2B
        ['123-1234567-1234558', '', '2019-12-20T17:28:35+00:00', '',
         'Shipped', 'Merchant', 'Amazon.com', '', '', '', 'test_name_10',
         'test_sku_10', 'TESTASI012', '', '1', 'USD', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'true', '', '', ''],
        # 12. Amazon FBA UK B2B 1/2
        ['123-1234567-1234557', '', '2019-12-19T17:28:35+00:00', '',
         'Shipped', 'Amazon', 'Amazon.co.uk', '', '', '', 'test_name_11',
         'test_sku_11', 'TESTASI013', '', '1', 'GBP', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'true', '', '', ''],
        # 13. Amazon FBA UK B2B 2/2
        ['123-1234567-1234557', '', '2019-12-19T17:28:35+00:00', '',
         'Shipped', 'Amazon', 'Amazon.co.uk', '', '', '', 'test_name_11_2',
         'test_sku_11_2', 'TESTASI131', '', '2', 'GBP', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'true', '', '', ''],
        # 14. Amazon FBM UK B2B - order exists in DB but not the item
        ['123-1234567-1234556', '', '2019-12-18T17:28:35+00:00', '',
         'Shipped', 'Merchant', 'Amazon.co.uk', '', '', '', 'test_name_12',
         'test_sku_12', 'TESTASI014', '', '1', 'GBP', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'true', '', '', ''],
        # 15. Test order with no mapped origin
        ['123-1234567-1234555', '', '2019-12-27T17:28:35+00:00', '',
         'Shipped', 'Amazon', 'Amazon.fr', '', '', '', 'test_name_13',
         'test_sku_13', 'TESTASI015', '', '1', 'EUR', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
        # 16. Test order cancelled
        ['123-1234567-1234554', '', '2019-12-30T17:28:35+00:00', '',
         'Cancelled', 'Amazon', 'Amazon.fr', '', '', '', 'test_name_14',
         'test_sku_14', 'TESTASI016', '', '1', 'EUR', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
        # 17. Test order Non-Amazon
        ['123-1234567-1234553', '', '2019-12-21T17:28:35+00:00', '',
         'Shipped', 'Amazon', 'Non-Amazon', '', '', '', 'test_name_15',
         'test_sku_15', 'TESTASI017', '', '1', 'EUR', '23.7', '3.95',
         '', '', '', '', '', '', '', '', '', '', '', 'false', '', '', ''],
    ]
    return pandas.DataFrame(data=orders, columns=[
        'amazonorderid', 'merchantorderid', 'purchasedate',
        'lastupdateddate', 'orderstatus', 'fulfillmentchannel',
        'saleschannel', 'orderchannel', 'url', 'shipservicelevel',
        'productname', 'sku', 'asin', 'itemstatus', 'quantity', 'currency',
        'itemprice', 'itemtax', 'shippingprice', 'shippingtax',
        'giftwrapprice', 'giftwraptax', 'itempromotiondiscount',
        'shippromotiondiscount', 'shipcity', 'shipstate',
        'shippostalcode', 'shipcountry', 'promotionids',
        'isbusinessorder', 'purchaseordernumber',
        'pricedesignation', 'issoldbyab'])


@pytest.fixture
def test_identification_table() -> list:
    idents = [
        IdentificationModel(
            variation_id=1234, ean='1234567891011', sku='test_sku_1',
            asin_all='TESTASIN01'
        ),
        IdentificationModel(
            variation_id=1235, ean='1234567891012', sku='test_sku_2',
            asin_all='TESTASIN02'
        ),
        IdentificationModel(
            variation_id=1236, ean='1234567891013', sku='test_sku_3',
            asin_all='TESTASIN03'
        ),
        IdentificationModel(
            variation_id=1237, ean='1234567891014', sku='test_sku_4',
            asin_all='TESTASIN04'
        ),
        IdentificationModel(
            variation_id=1337, ean='1234567892014', sku='test_sku_4_1',
            asin_all='TESTASIN41'
        ),
        IdentificationModel(
            variation_id=1437, ean='1234567893014', sku='test_sku_4_2',
            asin_all='TESTASIN42'
        ),
        IdentificationModel(
            variation_id=1238, ean='1234567891015', sku='test_sku_5',
            asin_all='TESTASIN05'
        ),
        IdentificationModel(
            variation_id=1239, ean='1234567891016', sku='test_sku_6',
            asin_all='TESTASIN06'
        ),
        IdentificationModel(
            variation_id=1240, ean='1234567891017', sku='test_sku_7',
            asin_all='TESTASIN07'
        ),
        IdentificationModel(
            variation_id=1241, ean='1234567891018', sku='test_sku_8',
            asin_all='TESTASI010'
        ),
        IdentificationModel(
            variation_id=1242, ean='1234567891019', sku='test_sku_9',
            asin_all='TESTASI011'
        ),
        IdentificationModel(
            variation_id=1243, ean='1234567891020', sku='test_sku_10',
            asin_all='TESTASI012'
        ),
        IdentificationModel(
            variation_id=1244, ean='1234567891021', sku='test_sku_11',
            asin_all='TESTASI013'
        ),
        IdentificationModel(
            variation_id=1344, ean='1234567892021', sku='test_sku_11_1',
            asin_all='TESTASI131'
        ),
        IdentificationModel(
            variation_id=1245, ean='1234567891022', sku='test_sku_12',
            asin_all='TESTASI014'
        ),
        IdentificationModel(
            variation_id=1255, ean='1234567892022', sku='test_sku_12_1',
            asin_all='TESTASI141'
        ),
        IdentificationModel(
            variation_id=1246, ean='1234567891023', sku='test_sku_13',
            asin_all='TESTASI015'
        ),
        IdentificationModel(
            variation_id=1247, ean='1234567891024', sku='test_sku_14',
            asin_all='TESTASI016'
        ),
        IdentificationModel(
            variation_id=1248, ean='1234567891025', sku='test_sku_15',
            asin_all='TESTASI017'
        )
    ]
    return idents


@pytest.fixture
def test_existing_amazon_orders() -> list:
    orders = [
        # Duplicate of a new entry
        AmazonOrdersModel(
            amazon_id='123-1234567-1234562', origin_id=4.02,
            purchase_date='2019-12-14', b2b=False
        ),
        # Matches a new order item
        AmazonOrdersModel(
            amazon_id='123-1234567-1234556', origin_id=4.22,
            purchase_date='2019-12-18', b2b=True
        ),
        # Not found in new entries
        AmazonOrdersModel(
            amazon_id='123-1234567-1234553', origin_id=4.01,
            purchase_date='2019-11-13', b2b=False
        )
    ]
    return orders


@pytest.fixture
def test_existing_amazon_order_items() -> list:
    order_items = [
        # Duplicate of a new entry
        AmazonOrderItemsModel(
            order_id=1, amazon_id='123-1234567-1234562', origin_id=4.02,
            variation_id=1239, asin='TESTASIN06', quantity=1,
            ean='1234567891016'
        ),
        # Not found in new entries but order exists
        AmazonOrderItemsModel(
            order_id=2, amazon_id='123-1234567-1234556', origin_id=4.22,
            variation_id=1255, asin='TESTASI141', quantity=1,
            ean='1234567892022'
        ),
        # Not found in new entries
        AmazonOrderItemsModel(
            order_id=3, amazon_id='123-1234567-1234553', origin_id=4.01,
            variation_id=1248, asin='TESTASI017', quantity=1,
            ean='1234567891025'
        )
    ]
    return order_items


@pytest.fixture
def missing_existing_amazon_order_items(
        test_existing_amazon_order_items) -> list:
    order_items = test_existing_amazon_order_items
    order_items[0].variation_id = 2239
    order_items[1].variation_id = 2255
    order_items[2].variation_id = 1248
    return order_items


@pytest.fixture
def test_existing_amazon_stock() -> list:
    stock = [
        StockModel(
            variation_id=1234,
            ean=1234567891011,
            stock_warehouse_1=10,
            stock_warehouse_2=10,
            stock_warehouse_3=10,
            stock_warehouse_4=10,
            stock_warehouse_5=10
        ),
        StockModel(
            variation_id=1235,
            ean=1234567891012,
            stock_warehouse_1=10,
            stock_warehouse_2=10,
            stock_warehouse_3=10,
            stock_warehouse_4=10,
            stock_warehouse_5=10
        )
    ]
    return stock


@pytest.fixture
def sources(test_identification_table: list, test_existing_amazon_orders: list,
            test_existing_amazon_order_items: list,
            test_existing_amazon_stock: list) -> dict:
    return {'orders': test_existing_amazon_orders,
            'order_items': test_existing_amazon_order_items,
            'ident': test_identification_table,
            'stock': test_existing_amazon_stock}


@pytest.fixture
def missing_sources(test_identification_table: list,
                    test_existing_amazon_orders: list,
                    missing_existing_amazon_order_items: list) -> dict:
    return {'orders': test_existing_amazon_orders,
            'order_items': missing_existing_amazon_order_items,
            'ident': test_identification_table}


def describe_search_order() -> None:
    def with_no_row(db_session, sources: dict) -> None:
        setup_order_database(session=db_session, sources=sources)

        amazon_import = DataImport(session=db_session, origin_map=None)
        assert amazon_import._DataImport__search_order(row=None) is None

    def with_order_found(db_session, sources: dict,
                         amazon_order_report: pandas.DataFrame) -> None:
        setup_order_database(session=db_session, sources=sources)

        row = list(amazon_order_report.itertuples(index=False))[7]
        amazon_import = DataImport(session=db_session, origin_map=None)
        assert amazon_import._DataImport__search_order(row=row) is not None

    def with_order_not_found(db_session, sources: dict,
                             amazon_order_report: pandas.DataFrame) -> None:
        setup_order_database(session=db_session, sources=sources)

        row = list(amazon_order_report.itertuples(index=False))[0]
        amazon_import = DataImport(session=db_session, origin_map=None)
        assert amazon_import._DataImport__search_order(row=row) is None


def describe_search_order_item() -> None:
    def with_no_row(db_session, sources: dict) -> None:
        setup_order_database(session=db_session, sources=sources)

        amazon_import = DataImport(session=db_session, origin_map=None)
        assert amazon_import._DataImport__search_order_item(row=None) is None

    def with_order_found_but_not_item(
            db_session, sources: dict,
            amazon_order_report: pandas.DataFrame) -> None:
        setup_order_database(session=db_session, sources=sources)

        row = list(amazon_order_report.itertuples(index=False))[14]
        amazon_import = DataImport(session=db_session, origin_map=None)
        assert amazon_import._DataImport__search_order_item(row=row) is None

    def with_order_found_and_item(
            db_session, sources: dict,
            amazon_order_report: pandas.DataFrame) -> None:
        setup_order_database(session=db_session, sources=sources)

        row = list(amazon_order_report.itertuples(index=False))[7]
        amazon_import = DataImport(session=db_session, origin_map=None)
        assert amazon_import._DataImport__search_order_item(row=row) is not None

    def with_order_not_found(db_session, sources: dict,
                             amazon_order_report: pandas.DataFrame) -> None:
        setup_order_database(session=db_session, sources=sources)

        row = list(amazon_order_report.itertuples(index=False))[0]
        amazon_import = DataImport(session=db_session, origin_map=None)
        assert amazon_import._DataImport__search_order_item(row=row) is None


def describe_handle_order() -> None:
    def with_order_creation(
            db_session, sources: dict, origin_map: dict,
            amazon_order_report: pandas.DataFrame) -> None:
        """
        Using a row that is neither found in the local orders nor in the DB
        should result in a newly created order. Calling `handle_order` on that
        row again should return the same order again.
        """
        setup_order_database(session=db_session, sources=sources)

        orders = []
        row = list(amazon_order_report.itertuples(index=False))[0]
        orders_before = db_session.query(AmazonOrdersModel).count()
        order_items_before = db_session.query(AmazonOrdersModel).count()

        amazon_import = DataImport(origin_map=origin_map, session=db_session)
        order = amazon_import.handle_order(row=row, local=orders)

        expect_order = AmazonOrdersModel(
            amazon_id='123-1234567-1234567', origin_id='104.01',
            purchase_date=datetime.date(2019, 12, 31), b2b=False,
            netto_total=0, gross_total=0
        )
        orders_after = db_session.query(AmazonOrdersModel).count()
        order_items_after = db_session.query(AmazonOrdersModel).count()

        assert expect_order.amazon_id == order.amazon_id
        assert expect_order.origin_id == order.origin_id
        assert expect_order.purchase_date == order.purchase_date
        assert expect_order.b2b == order.b2b
        assert order.order_items == []
        assert orders[0] == order
        assert orders_before == orders_after
        assert order_items_before == order_items_after

        new_order = amazon_import.handle_order(row=row, local=orders)
        orders_after = db_session.query(AmazonOrdersModel).count()

        assert order == new_order
        assert orders_before == orders_after

    def with_order_in_database_but_unknown_item(
            db_session, sources: dict, origin_map: dict,
            amazon_order_report: pandas.DataFrame) -> None:
        """
        Using a row that is already present in the database should result in
        the database entry, while the local list should stay empty.
        """
        setup_order_database(session=db_session, sources=sources)

        orders = []
        row = list(amazon_order_report.itertuples(index=False))[14]
        orders_before = db_session.query(AmazonOrdersModel).count()
        order_items_before = db_session.query(AmazonOrdersModel).count()

        amazon_import = DataImport(origin_map=origin_map, session=db_session)
        order = amazon_import.handle_order(row=row, local=orders)

        expect_order = AmazonOrdersModel(
            amazon_id='123-1234567-1234556', origin_id=Decimal('4.22'),
            purchase_date=datetime.date(2019, 12, 18), b2b=True
        )
        orders_after = db_session.query(AmazonOrdersModel).count()
        order_items_after = db_session.query(AmazonOrdersModel).count()

        assert expect_order.amazon_id == order.amazon_id
        assert expect_order.origin_id == order.origin_id
        assert expect_order.purchase_date == order.purchase_date
        assert expect_order.b2b == order.b2b
        assert len(order.order_items) == 1
        assert len(orders) == 0
        assert orders_before == orders_after
        assert order_items_before == order_items_after

    def with_order_and_item_in_database(
            db_session, sources: dict, origin_map: dict,
            amazon_order_report: pandas.DataFrame) -> None:
        """
        Using a row that is already present in the database as order and item
        should result in `None`, to conclude that there is nothing left to
        do for the row.
        """
        setup_order_database(session=db_session, sources=sources)

        orders = []
        row = list(amazon_order_report.itertuples(index=False))[7]
        orders_before = db_session.query(AmazonOrdersModel).count()
        order_items_before = db_session.query(AmazonOrdersModel).count()

        amazon_import = DataImport(origin_map=origin_map, session=db_session)
        order = amazon_import.handle_order(row=row, local=orders)

        orders_after = db_session.query(AmazonOrdersModel).count()
        order_items_after = db_session.query(AmazonOrdersModel).count()

        assert order is None
        assert len(orders) == 0
        assert orders_before == orders_after
        assert order_items_before == order_items_after

    def with_order_in_locals(
            db_session, sources: dict, origin_map: dict,
            amazon_order_report: pandas.DataFrame) -> None:
        """
        A order that is already present in local orders should not be
        added again
        """
        setup_order_database(session=db_session, sources=sources)

        expect_order = AmazonOrdersModel(
            amazon_id='123-1234567-1234567', origin_id=4.01,
            purchase_date='2019-12-31', b2b=False,
            netto_total=19.75, gross_total=23.7
        )
        orders = [expect_order]
        row = list(amazon_order_report.itertuples(index=False))[0]
        orders_before = db_session.query(AmazonOrdersModel).count()
        order_items_before = db_session.query(AmazonOrdersModel).count()

        amazon_import = DataImport(origin_map=origin_map, session=db_session)
        order = amazon_import.handle_order(row=row, local=orders)

        orders_after = db_session.query(AmazonOrdersModel).count()
        order_items_after = db_session.query(AmazonOrdersModel).count()

        assert expect_order == order
        assert orders[0] == order
        assert len(orders) == 1
        assert orders_before == orders_after
        assert order_items_before == order_items_after

    def with_cancelled_order(
            db_session, sources: dict, origin_map: dict,
            amazon_order_report: pandas.DataFrame) -> None:
        setup_order_database(session=db_session, sources=sources)

        orders = []
        row = list(amazon_order_report.itertuples(index=False))[16]
        orders_before = db_session.query(AmazonOrdersModel).count()
        order_items_before = db_session.query(AmazonOrdersModel).count()

        amazon_import = DataImport(origin_map=origin_map, session=db_session)
        order = amazon_import.handle_order(row=row, local=orders)

        orders_after = db_session.query(AmazonOrdersModel).count()
        order_items_after = db_session.query(AmazonOrdersModel).count()

        assert order is None
        assert len(orders) == 0
        assert orders_before == orders_after
        assert order_items_before == order_items_after

    def with_origin_not_mapped(
            db_session, sources: dict, origin_map: dict,
            amazon_order_report: pandas.DataFrame) -> None:
        setup_order_database(session=db_session, sources=sources)

        orders = []
        row = list(amazon_order_report.itertuples(index=False))[15]
        orders_before = db_session.query(AmazonOrdersModel).count()
        order_items_before = db_session.query(AmazonOrdersModel).count()
        expect_order = AmazonOrdersModel(
            amazon_id='123-1234567-1234555', origin_id=Decimal('-1'),
            purchase_date=datetime.date(2019, 12, 27), b2b=False
        )

        amazon_import = DataImport(origin_map=origin_map, session=db_session)
        order = amazon_import.handle_order(row=row, local=orders)

        orders_after = db_session.query(AmazonOrdersModel).count()
        order_items_after = db_session.query(AmazonOrdersModel).count()

        assert expect_order.amazon_id == order.amazon_id
        assert expect_order.origin_id == order.origin_id
        assert expect_order.purchase_date == order.purchase_date
        assert expect_order.b2b == order.b2b
        assert len(orders) == 1
        assert orders_before == orders_after
        assert order_items_before == order_items_after

    def with_non_amazon_order(
            db_session, sources: dict, origin_map: dict,
            amazon_order_report: pandas.DataFrame) -> None:
        setup_order_database(session=db_session, sources=sources)

        orders = []
        row = list(amazon_order_report.itertuples(index=False))[17]
        orders_before = db_session.query(AmazonOrdersModel).count()
        order_items_before = db_session.query(AmazonOrdersModel).count()

        amazon_import = DataImport(origin_map=origin_map, session=db_session)
        order = amazon_import.handle_order(row=row, local=orders)

        orders_after = db_session.query(AmazonOrdersModel).count()
        order_items_after = db_session.query(AmazonOrdersModel).count()

        assert order is None
        assert len(orders) == 0
        assert orders_before == orders_after
        assert order_items_before == order_items_after


def describe_find_origin() -> None:
    def with_origin_map(
            origin_map: dict, amazon_order_report: pandas.DataFrame) -> None:
        amazon_import = DataImport(origin_map=origin_map)
        result = []
        expect = ['104.01', '4.01', '104.03', '4.03', '4.03', '4.03',
                  '104.02', '4.02', '104.21', '4.21', '104.23', '4.23',
                  '104.22', '104.22', '4.22', '', '', '']

        for row in amazon_order_report.itertuples(index=False):
            result.append(amazon_import._DataImport__find_origin(row=row))

        assert expect == result

    def without_origin_map(amazon_order_report: pandas.DataFrame) -> None:
        amazon_import = DataImport(origin_map=None)

        for row in amazon_order_report.itertuples(index=False):
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                amazon_import._DataImport__find_origin(row=row)
                assert pytest_wrapped_e.type == SystemExit
                assert pytest_wrapped_e.value.code == 1


def describe_create_order_item() -> None:
    def with_full_document(
            db_session, origin_map: dict, sources: dict,
            amazon_order_report: pandas.DataFrame) -> None:

        setup_order_database(session=db_session, sources=sources)

        orders = [
            AmazonOrdersModel(amazon_id='123-1234567-1234567',
                              origin_id=104.01, netto_total=0,
                              gross_total=0, b2b=False),
            AmazonOrdersModel(amazon_id='123-1234567-1234566',
                              origin_id=4.01, netto_total=0,
                              gross_total=0, b2b=False),
            AmazonOrdersModel(amazon_id='123-1234567-1234565',
                              origin_id=104.03, netto_total=0,
                              gross_total=0, b2b=False),
            AmazonOrdersModel(amazon_id='123-1234567-1234564',
                              origin_id=4.03, netto_total=0,
                              gross_total=0, b2b=False),
            AmazonOrdersModel(amazon_id='123-1234567-1234563',
                              origin_id=104.02, netto_total=0,
                              gross_total=0, b2b=False),
            AmazonOrdersModel(amazon_id='123-1234567-1234562',
                              origin_id=4.02, netto_total=0,
                              gross_total=0, b2b=False),
            AmazonOrdersModel(amazon_id='123-1234567-1234561',
                              origin_id=104.21, netto_total=0,
                              gross_total=0, b2b=True),
            AmazonOrdersModel(amazon_id='123-1234567-1234560',
                              origin_id=4.21, netto_total=0,
                              gross_total=0, b2b=True),
            AmazonOrdersModel(amazon_id='123-1234567-1234559',
                              origin_id=104.23, netto_total=0,
                              gross_total=0, b2b=True),
            AmazonOrdersModel(amazon_id='123-1234567-1234558',
                              origin_id=4.23, netto_total=0,
                              gross_total=0, b2b=True),
            AmazonOrdersModel(amazon_id='123-1234567-1234557',
                              origin_id=104.22, netto_total=0,
                              gross_total=0, b2b=True),
            AmazonOrdersModel(amazon_id='123-1234567-1234556',
                              origin_id=4.22, netto_total=0,
                              gross_total=0, b2b=True)
        ]

        amazon_import = DataImport(origin_map=origin_map, session=db_session)
        result = []
        expected_id = ['123-1234567-1234567', '123-1234567-1234566',
                       '123-1234567-1234565', '123-1234567-1234564',
                       '123-1234567-1234564', '123-1234567-1234564',
                       '123-1234567-1234563', '123-1234567-1234562',
                       '123-1234567-1234561', '123-1234567-1234560',
                       '123-1234567-1234559', '123-1234567-1234558',
                       '123-1234567-1234557', '123-1234567-1234557',
                       '123-1234567-1234556']
        expected_asin = ['TESTASIN01', 'TESTASIN02', 'TESTASIN03',
                         'TESTASIN04', 'TESTASIN41', 'TESTASIN42', 'TESTASIN05',
                         'TESTASIN06', 'TESTASIN07', 'TESTASI010',
                         'TESTASI011', 'TESTASI012', 'TESTASI013',
                         'TESTASI131', 'TESTASI014']
        expected_gross = ['23.7', '23.7', '21.17', '58.96', '27.02', '27.02',
                          '23.7', '23.7', '21.17', '21.17', '54.04', '27.02']
        expected_netto = ['19.75', '19.75', '17.64', '47.97', '22.52', '22.52',
                          '19.75', '19.75', '17.64', '17.64', '45.04', '22.52']

        order_index = [0, 1, 2, 3, 3, 3, 4, 5, 6, 7, 8, 9, 10, 10, 11]
        for index, row in enumerate(
                amazon_order_report.head(15).itertuples(index=False)):
            result.append(amazon_import.create_order_item(
                row=row, order=orders[order_index[index]]))

        assert 15 == len(result)

        for index, item in enumerate(result):
            assert expected_id[index] == item.amazon_id
            assert expected_asin[index] == item.asin

        for index, order in enumerate(orders):
            assert Decimal(expected_netto[index]) == order.netto_total
            assert Decimal(expected_gross[index]) == order.gross_total


def describe_cleanup_variation_ids() -> None:
    def with_no_missing_variations(db_session, sources: dict,
                                   origin_map: dict) -> None:
        setup_order_database(session=db_session, sources=sources)
        var_before = [x.variation_id for x in
                      db_session.query(AmazonOrderItemsModel).all()]
        amazon_import = DataImport(origin_map=origin_map, session=db_session)
        amazon_import.cleanup_variation_ids()
        var_after = [x.variation_id for x in
                     db_session.query(AmazonOrderItemsModel).all()]

        assert var_after == var_before

    def with_missing_variations(db_session, sources: dict,
                                origin_map: dict) -> None:
        sources['order_items'][0].variation_id = 2239
        sources['order_items'][1].variation_id = 2255
        setup_order_database(session=db_session, sources=sources)
        var_before = [x.variation_id for x in
                      db_session.query(AmazonOrderItemsModel).all()]
        amazon_import = DataImport(origin_map=origin_map, session=db_session)
        amazon_import.cleanup_variation_ids()
        var_after = [x.variation_id for x in
                     db_session.query(AmazonOrderItemsModel).all()]

        assert var_after != var_before
        assert 1239 == var_after[0]
        assert 1255 == var_after[1]


def describe_read_amazon_stock_data() -> None:
    test_columns = [
        'Country', 'Currency code', 'ASIN', 'Total Units', 'Customer Order'
    ]
    expected_columns = ['Currency code', 'ASIN', 'sum']

    def with_no_dups(monkeypatch) -> None:
        test_df = pandas.DataFrame(
            [
                ['DE', 'EUR', 'TESTASIN01', '10', '0'],
                ['FR', 'EUR', 'TESTASIN02', '10', '0'],
                ['ES', 'EUR', 'TESTASIN03', '10', '0'],
                ['IT', 'EUR', 'TESTASIN04', '10', '0'],
                ['GB', 'GBP', 'TESTASIN05', '10', '0'],
            ],
            columns=test_columns
        )
        expected_df = pandas.DataFrame(
            [
                ['EUR', 'TESTASIN01', 10],
                ['EUR', 'TESTASIN02', 10],
                ['EUR', 'TESTASIN03', 10],
                ['EUR', 'TESTASIN04', 10],
                ['GBP', 'TESTASIN05', 10],
            ],
            columns=expected_columns
        )

        def substitute_func(data, sep, dtype):
            return test_df
        amazon_export = DataCollector()
        monkeypatch.setattr(pandas, 'read_csv', substitute_func)
        result = amazon_export.read_amazon_stock_data("test.csv")

        assert_frame_equal(result, expected_df)

    def with_multiple_countries(monkeypatch) -> None:
        test_df = pandas.DataFrame(
            [
                ['DE', 'EUR', 'TESTASIN01', '10', '0'],
                ['FR', 'EUR', 'TESTASIN01', '10', '0'],
                ['ES', 'EUR', 'TESTASIN01', '10', '0'],
                ['IT', 'EUR', 'TESTASIN01', '10', '0'],
                ['GB', 'GBP', 'TESTASIN01', '10', '0'],
            ],
            columns=test_columns
        )
        expected_df = pandas.DataFrame(
            [
                ['EUR', 'TESTASIN01', 10],
                ['GBP', 'TESTASIN01', 10],
            ],
            columns=expected_columns
        )

        def substitute_func(data, sep, dtype):
            return test_df
        amazon_export = DataCollector()
        monkeypatch.setattr(pandas, 'read_csv', substitute_func)
        result = amazon_export.read_amazon_stock_data("test.csv")

        assert_frame_equal(result, expected_df)

    def with_multiple_asins(monkeypatch) -> None:
        test_df = pandas.DataFrame(
            [
                ['DE', 'EUR', 'TESTASIN01', '10', '0'],
                ['DE', 'EUR', 'TESTASIN01', '10', '0'],
                ['GB', 'GBP', 'TESTASIN01', '10', '0'],
                ['GB', 'GBP', 'TESTASIN01', '10', '0'],
            ],
            columns=test_columns
        )
        expected_df = pandas.DataFrame(
            [
                ['EUR', 'TESTASIN01', 20],
                ['GBP', 'TESTASIN01', 20],
            ],
            columns=expected_columns
        )

        def substitute_func(data, sep, dtype):
            return test_df
        amazon_export = DataCollector()
        monkeypatch.setattr(pandas, 'read_csv', substitute_func)
        result = amazon_export.read_amazon_stock_data("test.csv")

        assert_frame_equal(result, expected_df)

    def with_customer_order(monkeypatch) -> None:
        test_df = pandas.DataFrame(
            [
                ['DE', 'EUR', 'TESTASIN01', '7', '3'],
                ['FR', 'EUR', 'TESTASIN01', '9', '1'],
            ],
            columns=test_columns
        )
        expected_df = pandas.DataFrame(
            [
                ['EUR', 'TESTASIN01', 10],
            ],
            columns=expected_columns
        )

        def substitute_func(data, sep, dtype):
            return test_df
        amazon_export = DataCollector()
        monkeypatch.setattr(pandas, 'read_csv', substitute_func)
        result = amazon_export.read_amazon_stock_data("test.csv")

        assert_frame_equal(result, expected_df)

    def with_unsynced_countries_within_region(monkeypatch) -> None:
        """
        A region (EU, UK, USA) with multiple subcountries can be in an
        unsynchronized state in which sales orders have been registered
        for the country but not for the region. In this state, the total
        amount differs between the countries and we only want to take the
        maximum amount to determine the actual stock.
        """
        test_df = pandas.DataFrame(
            [
                ['DE', 'EUR', 'TESTASIN01', '18', '2'],
                ['FR', 'EUR', 'TESTASIN01', '19', '1'],
                ['ES', 'EUR', 'TESTASIN01', '20', '0'],
            ],
            columns=test_columns
        )
        expected_df = pandas.DataFrame(
            [
                ['EUR', 'TESTASIN01', 20],
            ],
            columns=expected_columns
        )

        def substitute_func(data, sep, dtype):
            return test_df
        amazon_export = DataCollector()
        monkeypatch.setattr(pandas, 'read_csv', substitute_func)
        result = amazon_export.read_amazon_stock_data("test.csv")

        assert_frame_equal(result, expected_df)


def assert_table(session, expectation):
    """ Helper function for import_amazon_stock test"""
    assert session.query(StockModel).count() == len(expectation)
    for index, row in enumerate(session.query(StockModel).all()):
        assert row.variation_id == expectation[index].variation_id
        assert row.ean == expectation[index].ean
        assert row.stock_warehouse_1 == expectation[index].stock_warehouse_1
        assert row.stock_warehouse_2 == expectation[index].stock_warehouse_2
        assert row.stock_warehouse_3 == expectation[index].stock_warehouse_3
        assert row.stock_warehouse_4 == expectation[index].stock_warehouse_4
        assert row.stock_warehouse_5 == expectation[index].stock_warehouse_5


def describe_import_amazon_stock() -> None:
    test_columns = ['Currency code', 'ASIN', 'sum']
    def with_export_equals_db(
            db_session, origin_map: dict, sources: dict) -> None:
        setup_stock_database(session=db_session, sources=sources)
        test_df = pandas.DataFrame(
            [
                ['EUR', 'TESTASIN01', 10],
                ['EUR', 'TESTASIN02', 10]
            ],
            columns=test_columns
        )
        expected_stock = [
            StockModel(
                variation_id=1234,
                ean='1234567891011',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=10
            ),
            StockModel(
                variation_id=1235,
                ean='1234567891012',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=10
            )
        ]
        amazon_import=DataImport(origin_map=origin_map, session=db_session)
        amazon_import.import_amazon_stock(dataframe=test_df, region='EU',
                                          warehouse='stock_warehouse_3')
        assert_table(session=db_session, expectation=expected_stock)

    def with_update_stock(
            db_session, origin_map: dict, sources: dict) -> None:
        setup_stock_database(session=db_session, sources=sources)
        test_df = pandas.DataFrame(
            [
                ['EUR', 'TESTASIN01', 5],
                ['EUR', 'TESTASIN02', 10]
            ],
            columns=test_columns
        )
        expected_stock = [
            StockModel(
                variation_id=1234,
                ean='1234567891011',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=5,
                stock_warehouse_4=10,
                stock_warehouse_5=10
            ),
            StockModel(
                variation_id=1235,
                ean='1234567891012',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=10
            )
        ]
        amazon_import=DataImport(origin_map=origin_map, session=db_session)
        amazon_import.import_amazon_stock(dataframe=test_df, region='EU',
                                         warehouse='stock_warehouse_3')
        assert_table(session=db_session, expectation=expected_stock)

    def with_create_new_item(
            db_session, origin_map: dict, sources: dict) -> None:
        setup_stock_database(session=db_session, sources=sources)
        test_df = pandas.DataFrame(
            [
                ['GBP', 'TESTASIN01', 10],
                ['GBP', 'TESTASIN02', 10],
                ['GBP', 'TESTASIN04', 10]
            ],
            columns=test_columns
        )
        expected_stock = [
            StockModel(
                variation_id=1234,
                ean='1234567891011',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=10
            ),
            StockModel(
                variation_id=1235,
                ean='1234567891012',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=10
            ),
            StockModel(
                variation_id=1237,
                ean='1234567891014',
                stock_warehouse_1=0,
                stock_warehouse_2=0,
                stock_warehouse_3=0,
                stock_warehouse_4=10,
                stock_warehouse_5=0
            )
        ]
        amazon_import=DataImport(origin_map=origin_map, session=db_session)
        amazon_import.import_amazon_stock(dataframe=test_df, region='UK',
                                          warehouse='stock_warehouse_4')
        assert_table(session=db_session, expectation=expected_stock)

    def with_delete_stock_of_item(
            db_session, origin_map: dict, sources: dict) -> None:
        setup_stock_database(session=db_session, sources=sources)
        test_df = pandas.DataFrame(
            [
                ['USD', 'TESTASIN01', 10]
            ],
            columns=test_columns
        )
        expected_stock = [
            StockModel(
                variation_id=1234,
                ean='1234567891011',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=10
            ),
            StockModel(
                variation_id=1235,
                ean='1234567891012',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=0
            )
        ]
        amazon_import=DataImport(origin_map=origin_map, session=db_session)
        amazon_import.import_amazon_stock(dataframe=test_df, region='US',
                                          warehouse='stock_warehouse_5')
        assert_table(session=db_session, expectation=expected_stock)

    def with_no_match_in_id(
            db_session, origin_map: dict, sources: dict) -> None:
        setup_stock_database(session=db_session, sources=sources)
        test_df = pandas.DataFrame(
            [
                ['USD', 'TESTASIN01', 10],
                ['USD', 'TESTASIN02', 10],
                ['USD', 'TESTASS007', 99]
            ],
            columns=test_columns
        )
        expected_stock = [
            StockModel(
                variation_id=1234,
                ean='1234567891011',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=10
            ),
            StockModel(
                variation_id=1235,
                ean='1234567891012',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=10
            )
        ]
        amazon_import=DataImport(origin_map=origin_map, session=db_session)
        amazon_import.import_amazon_stock(dataframe=test_df, region='US',
                                          warehouse='stock_warehouse_5')
        assert_table(session=db_session, expectation=expected_stock)

    def with_multiple_asin(
            db_session, origin_map: dict, sources: dict) -> None:
        setup_stock_database(session=db_session, sources=sources)
        test_df = pandas.DataFrame(
            [
                ['USD', 'TESTASIN01', 10],
                ['USD', 'TESTASIN02', 10],
                ['USD', 'TESTASIN01', 10]
            ],
            columns=test_columns
        )
        expected_stock = [
            StockModel(
                variation_id=1234,
                ean='1234567891011',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=20
            ),
            StockModel(
                variation_id=1235,
                ean='1234567891012',
                stock_warehouse_1=10,
                stock_warehouse_2=10,
                stock_warehouse_3=10,
                stock_warehouse_4=10,
                stock_warehouse_5=10
            )
        ]
        amazon_import=DataImport(origin_map=origin_map, session=db_session)
        amazon_import.import_amazon_stock(dataframe=test_df, region="US",
                                          warehouse='stock_warehouse_5')
        assert_table(session=db_session, expectation=expected_stock)
