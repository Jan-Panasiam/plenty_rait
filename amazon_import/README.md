# Import Amazon orders (Fulfillment by merchant and amazon) to a MariaDB database

## Installation

Set up the configuration, the folder is automatically created when you run the program the first time.
Look at the [configuration](#config) section of this description for advice.
You can use the `plenty_sync` application for the creation of the mapping database table.

Before you can run the program you will need to install all dependencies to ensure functionality.

```bash
# Clone the the project from GitLab to your computer.
git clone https://gitlab.com/initBasti/plenty_rait.git
# Navigate to the amazon_import directory.
cd plenty_rait/amazon_import
# Install all dependencies
poetry build
tar -xf dist/amazon_import-0.1.1.tar.gz -C ~/
python3 ~/amazon_import-0.1.1/setup.py install
python3 -m amazon_import -h
```

## Usage

To import amazon orders into the database, go to your [seller central](https://sellercentral.amazon.com):
Download the export, Reports -> Fulfillment by Amazon -> [Sales] All Orders

```bash
# Run the command
python3 -m amazon_import -o amazon_order_export.txt
```

To import amazon FBA inventory into the database, go to your [seller central](https.//sellercentral.amazon.com):
Download the export, Reports -> Fulfillment by Amazon -> [Inventory] Manage FBA Inventory

```bash
# Run the command
python3 -m amazon_import -s amazon_stock_export.txt
```

## Configuration <a name="config">

The application requires a few information in order to work properly, you will have to provide the host and the target database like this within the `db_config.ini` file in your `.config/plenty_rait_tool/` folder:

```ini
[DATABASE]
host=localhost
database=mydb
```

All other configurations should be placed into the `.config/plenty_rait_tool/amazon_import/config.ini` file.

As this tool is created with a close relationship to Plentymarkets, you have to provide a way for ASINs to be mapped to Plentymarkets Variation IDs.
Create a new table in the target database which contains rows, that maps a variation ID to a SKU and to a ASIN (I use an ASIN, that is mapped for all marketplaces).
The table should have the following columns: `variation_id`, `sku`, `ean`, `asin_all`. The `plenty_sync` tool contains an option to create such a table.

The marketplaces have to mapped with the origin IDs from Plentymarkets, you can locate those values in your Plentymarkets system under: Setup->Orders->order referrer:
The ID of a fulfillment by Amazon (FBA) marketplace is added with: `amazon_fba_{ISO-3166-1 Alpha-3 code}={ID}` (example: `amazon_fba_GER=104.01`).
Likewise the same is required for fulfillment by merchant (FBM) marketplaces (example: `amazon_GER=4.01`).
A list of **ISO-3166-1 Alpha-3 codes** can be found [here](https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes).

```ini
[Origins]
amazon_GER=4.01
amazon_GER_b2b=4.21
amazon_fba_GER=104.01
amazon_fba_GER_b2b=104.21
```

In order to get the most recent currency exchange rate, there is the possibility to add links from the [exchangerate-api](https://exchangerate-api.com).

```ini
[General]
exchange_rate_uk=https://api.exchangerate-api.com/v4/latest/GBP
exchange_rate_us=https://api.exchangerate-api.com/v4/latest/USD
exchange_rate_se=https://api.exchangerate-api.com/v4/latest/SEK
exchange_rate_pl=https://api.exchangerate-api.com/v4/latest/PLN
```

Finally, you have to map the warehouse-regions of your amazon stock to the tablenames in your Database

```ini
[Stocks]
EU=stock_warehouse_3
UK=stock_warehouse_4
US=stock_warehouse_5
```

### WARNING

The calculated Euro prices are not 100% accurate as the rate changes frequently, the values should be an approximate summary and should **never** be used for any official billing, in which the amount must be correct.
