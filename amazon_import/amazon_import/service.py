"""
Import Amazon orders from a seller-central export to a MariaDB database.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import datetime
from decimal import Decimal
import importlib
import sys
import pandas
import re
from sqlalchemy import and_
from sqlalchemy.orm import scoped_session, sessionmaker
from loguru import logger
from amazon_import.schema import (
    dal, AmazonOrdersModel, AmazonOrderItemsModel, IdentificationModel,
    StockModel
)
from amazon_import.utils import convert_price, validate_row
import os
import configparser


def check_header(check: str, frame: pandas.DataFrame) -> bool:
    """
    Check that the export file has the correct header.

    Parameters:
        check                   [str]       -   Define the export file type to
                                                verify
        frame                   [DataFrame] -   Dataframe to check

    Return:
                                [bool]      -   boolean verifying if the header
                                                passed the check or not
    """
    if len(frame.index) == 0:
        return False

    columns = {
        'orders': [
            'amazon-order-id', 'order-status', 'purchase-date',
            'fulfillment-channel', 'sales-channel', 'asin',
            'item-price', 'quantity'
        ],
        'stock': [
            'Country', 'Currency code', 'ASIN', 'Total Units', 'Customer Order'
        ]
    }
    required_columns = columns[check]

    return all([column in frame.columns for column in required_columns])

def adjust_column_names(frame: pandas.DataFrame) -> pandas.DataFrame:
    """
    Remove hyphons from the column names as these are difficult to handle
    within named tuples

    Parameters:
        frame       [pandas.Dataframe]  -   Dataframe where columns are to
                                            be adjusted

    Return:
        frame       [pandas.DataFrame]  -   Adjusted dataframe
    """
    frame.columns = frame.columns.str.replace('-', '')
    return frame


class DataCollector:
    def read_amazon_order_data(self, path: str) -> pandas.DataFrame:
        """
        Read out all amazon orders from the amazon export file and return them as
        a DataFrame

        Parameters:
            import_path [str]               -   path to the amazon export file

        Return:
            df          [pandas.DataFrame]  -   Dataframe containing all the
                                                amazon order-data
        """
        df = pandas.read_csv(path, sep='\t', header=0,
                             na_values=[], dtype=str)
        if not check_header(check='orders', frame=df):
            logger.error(f'Invalid file {path}')
            return pandas.DataFrame()
        df.fillna('')

        df = adjust_column_names(frame=df)

        return df


    def read_amazon_stock_data(self, import_path) -> pandas.DataFrame:
        """
        Read out the stock stored in amazon warehouses for each available
        variation.

        Parameters:
            import_path [str]               -   path to the amazon export file

        Return:
            df          [pandas.DataFrame]  -   Dataframe containing
                                                amazon stock-data
        """
        if re.search(".csv$", import_path):
            df = pandas.read_csv(import_path, sep=',', dtype=str)
        elif re.search(".txt$", import_path):
            df = pandas.read_csv(import_path, sep='\t', dtype=str)
        else:
            logger.error("Amazon stock export expects a [.txt]/[.csv] file "
                         f"(got: {import_path})")
            return pandas.DataFrame()

        if not check_header(check='stock', frame=df):
            logger.error(f'Invalid file {import_path}')
            return pandas.DataFrame()

        df = df.astype({'Total Units': int, 'Customer Order': int})

        df = df.groupby(
            ['Country', 'Currency code', 'ASIN'], as_index=False
        )[['Total Units', 'Customer Order']].sum()
        df['sum'] = df.loc[:, ['Total Units', 'Customer Order']].sum(axis=1)
        df = df.groupby(['Currency code', 'ASIN'], as_index=False)['sum'].max()

        return df


class DataImport:
    def __init__(self, origin_map: dict, session=None):
        self.origin_map = origin_map
        self.session = session

    def create_database_session(self, connection_string: str) -> None:
        dal.connection = connection_string
        dal.connect()

        dal.Session = scoped_session(sessionmaker(bind=dal.engine))
        dal.session = dal.Session()
        dal.Session.registry.clear()

        self.session = dal.session

    def create_tables(self) -> None:
        for table in ['amazon_orders', 'amazon_order_items']:
            if not dal.engine.dialect.has_table(dal.engine, table):
                table_model = importlib.import_module(
                    'amazon_import.schema')
                orm_table = getattr(table_model, table)
                orm_table.__table__.create(bind=dal.engine, checkfirst=True)

    def cleanup_variation_ids(self) -> None:
        """
        Search and replace old variation IDs with their new counterparts.

        Search for variation IDs that were removed from the identification
        table (removed on Plentymarkets) and replace them with the current
        matching variation ID for the ASIN.
        """
        variations = self.session.query(IdentificationModel.variation_id)
        order_items = self.session.query(
            AmazonOrderItemsModel
        ).filter(
            AmazonOrderItemsModel.variation_id != -1,
            AmazonOrderItemsModel.variation_id.notin_(variations)
        )
        for item in order_items.all():
            match = self.session.query(
                IdentificationModel.variation_id
            ).filter(
                IdentificationModel.asin_all == item.asin
            ).one()
            item.variation_id = match[0]
        self.session.commit()

    def __find_origin(self, row: tuple) -> str:
        """
        Find the correct Plentymarkets ID for the given order referrer.

        The mapping of Plentymarkets ID to Amazon marketplace is done within
        the configuration file.

        Parameters:
            row         [tuple] -   Row from the pandas dataframe

        Return:
                        [str]   -   Plentymarkets order referrer ID
                                    or empty if no match is found.
        """
        if self.origin_map is None:
            logger.error("No origin map found, can't map orders to IDs.")
            sys.exit(1)
        fulfiller = "FBA" if row.fulfillmentchannel == 'Amazon' else "FBM"
        try:
            customer_class = "B2B" if row.isbusinessorder == 'true' else "B2C"
        except AttributeError:
            customer_class = "B2C"
        sales_channel = row.saleschannel

        try:
            origin = self.origin_map[fulfiller][sales_channel][customer_class]
        except KeyError:
            return ''

        return origin

    def __search_order(self, row: tuple) -> AmazonOrdersModel:
        """
        Check if the given order is already created within the database.

        Parameters:
            row         [tuple] -   Row from the pandas dataframe

        Return:
                        [object]-   SQLalchemy object or None in case it
                                    is not found in the database
        """
        if not row:
            return None
        query = self.session.query(AmazonOrdersModel).filter(
            AmazonOrdersModel.amazon_id == row.amazonorderid
        )
        if query.count() == 0:
            return None

        if query.count() == 1:
            return query.first()

        if query.count() > 1:
            logger.warning("Order with Amazon ID: {row.amazonorderid} exists "
                           "multiple times within the amazon_orders database.")
            return query.first()

    def __search_order_item(self, row: tuple) -> AmazonOrderItemsModel:
        """
        Check if the given order item is already created within the database.

        Parameters:
            row         [tuple] -   Row from the pandas dataframe

        Return:
                        [object]-   SQLalchemy object or None in case it
                                    is not found in the database
        """
        if not row:
            return None
        query = self.session.query(AmazonOrderItemsModel).filter(
            and_(
                AmazonOrderItemsModel.amazon_id == row.amazonorderid,
                AmazonOrderItemsModel.asin == row.asin
            )
        )
        if query.count() == 0:
            return None

        if query.count() == 1:
            return query.first()

        if query.count() > 1:
            logger.warning("Order item with Amazon ID: {row.amazonorderid} "
                           " and ASIN {row.asin} exists multiple times within "
                           "the amazon_order_items database.")
            return query.first()

    def __create_order(self, row: tuple) -> AmazonOrdersModel:
        """
        Create a new order entry in the database with no price information.

        The price information is updated by processing the order items of the
        order.

        Parameters:
            row         [tuple] -   Row from the pandas dataframe

        Return:
                        [object]-   SQLalchemy object or None in case it
                                    is not found in the database
        """
        origin_id = self.__find_origin(row=row)
        if not origin_id:
            origin_id = -1
        try:
            b2b = True if row.isbusinessorder == 'true' else False
        except AttributeError:
            b2b = False
        order = AmazonOrdersModel(
            amazon_id=row.amazonorderid,
            origin_id=origin_id,
            purchase_date=datetime.date.fromisoformat(row.purchasedate[:-15]),
            netto_total=0,
            gross_total=0,
            b2b=b2b
        )

        return order

    def create_order_item(self, row: tuple,
                          order: AmazonOrdersModel) -> AmazonOrderItemsModel:
        """
        Create an order item for a given order.

        Calculate the price in Euro & update the price information of the order

        Parameters:
            row         [tuple] -   Row from the pandas dataframe
            order       [object]-   Associated order object

        Return:
                        [object]-   SQLalchemy object or None in case it
                                    is not found in the database
        """
        variation_id = self.session.query(IdentificationModel).filter(
            IdentificationModel.asin_all == row.asin
        ).first()
        ean = variation_id.ean
        if not variation_id:
            logger.warning(f"ASIN {row.asin} not found in Identification "
                           "Table.")
            variation_id = -1
        else:
            variation_id = variation_id.variation_id
        (gross, netto) = convert_price(total=row.itemprice, tax=row.itemtax,
                                       currency=row.currency,
                                       date=row.purchasedate)
        if not gross:
            logger.warning(f"Invalid gross price value [{row.itemprice}], "
                           f"default to 0. Amazon ID: {order.amazon_id}")
            gross = 0

        order_item = AmazonOrderItemsModel(
            order_id=order.id,
            amazon_id=order.amazon_id,
            origin_id=order.origin_id,
            variation_id=variation_id,
            ean=ean,
            asin=row.asin,
            netto=netto,
            gross=gross,
            quantity=row.quantity
        )
        order.netto_total += Decimal(netto)
        order.gross_total += Decimal(gross)
        order.order_items.append(order_item)

        return order_item

    def handle_order(self, row: tuple, local: list) -> AmazonOrdersModel:
        """
        Check if the given order exists, should be skipped or created.

        Parameters:
            row         [tuple] -   Row from the pandas dataframe
            local       [list]  -   List of created orders that are not synced
                                    with the database

        Return:
                        [object]-   SQLalchemy object or None in case it
                                    is not found in the database
        """
        if not validate_row(row=row):
            return None

        match_order = [x for x in local if x.amazon_id == row.amazonorderid]
        if len(match_order) > 1:
            logger.error("Multiple orders with the same Amazon order ID:\n"
                         f"{match_order}\nABORT")
            sys.exit(1)

        if not self.__search_order_item(row=row):
            order = self.__search_order(row=row)
            if not order and len(match_order) == 0:
                order = self.__create_order(row=row)
                if not order:
                    logger.error(f"Creation of order failed on row:\n{row}"
                                 "\nABORT")
                    sys.exit(1)
                local.append(order)
                return order

            elif order and len(match_order) == 0:
                return order
        else:
            return None

        return match_order[0]

    def import_amazon_stock(
        self, dataframe: pandas.DataFrame, warehouse: str, region: str
    ) -> None:
        """
        Import amazon stock data to database

        Parameter:
            dataframe           [DataFrame] -   dataframe containing amazon
                                                stock data
            warehouse           [str]       -   warehouse in database table
            region              [str]       -   region of the amazon stock
        """
        asin_dict = {}
        db_eans = []
        """
        Multiple countries are potentially grouped under one Amazon
        region (e.g. PAN-EU), these groups currently have the currency
        as a common denominator. This dictionary maps the valid region
        values from the CLI to the currency values. If you add a new
        region to the CLI, you have to extend this mapping.
        """
        currency_dict = {'EU': 'EUR', 'UK': 'GBP', 'US': 'USD'}
        dataframe = dataframe[dataframe['Currency code']==currency_dict[region]]

        # Sum stock over all asins
        for row in dataframe.itertuples():
            if row.ASIN in asin_dict.keys():
                asin_dict[row.ASIN] =\
                    asin_dict[row.ASIN] + row.sum
            else:
                asin_dict[row.ASIN] = row.sum

        # Map asin to variation_id and ean
        llist = []
        for data_row in self.session.query(
            IdentificationModel.asin_all,
            IdentificationModel.ean,
            IdentificationModel.variation_id
        ).filter(
            IdentificationModel.asin_all.in_(asin_dict.keys())
        ).all():
            llist.append(
                [data_row.variation_id, data_row.ean,
                 asin_dict[data_row.asin_all]])
        db_asin = [x for x, in self.session.query(IdentificationModel.asin_all)]
        missing = [asin for asin in asin_dict.keys() if asin not in db_asin]
        if len(missing) > 0:
            logger.warning(
                "Identification table is not up-to-date!\nMissing ASINs "
                f"{missing}")

        df = pandas.DataFrame(llist,
                              columns=['variation_id', 'ean', warehouse],
                              dtype=str)
        df = df.astype(dtype={'variation_id': 'int32', 'ean': 'object',
                              warehouse: 'int32'})

        for data_row in self.session.query(StockModel).filter(
            StockModel.ean.notin_(df['ean'])
        ).all():
            setattr(data_row, warehouse, 0)

        for data_row in self.session.query(StockModel).filter(
            StockModel.ean.in_(df['ean'])
        ).all():
            data_row.update_by_dataframe(dataframe=df)
            db_eans.append(data_row.ean)

        for row in df[~df['ean'].isin(db_eans)].to_dict(orient='records'):
            data_row = StockModel(variation_id=row['variation_id'])
            data_row.update_row(row=row)
            self.session.add(data_row)
        self.session.commit()
