"""
Import Amazon orders from a seller-central export to a MariaDB database.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from sqlalchemy import (
    Column, Integer, Text, String, Date, Numeric, Boolean, create_engine,
    ForeignKey
)
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import generic_repr
from loguru import logger

Base = declarative_base()


@generic_repr
class AmazonOrdersModel(Base):
    __tablename__ = "amazon_orders"

    id = Column(Integer, primary_key=True, nullable=False)
    amazon_id = Column(String(length=25), nullable=False)
    origin_id = Column(Numeric(precision=8, scale=2))
    purchase_date = Column(Date)
    netto_total = Column(Numeric(precision=8, scale=2))
    gross_total = Column(Numeric(precision=8, scale=2))
    b2b = Column(Boolean)
    order_items = relationship("AmazonOrderItemsModel",
                               back_populates="order")


@generic_repr
class AmazonOrderItemsModel(Base):
    __tablename__ = "amazon_order_items"

    id = Column(Integer, primary_key=True, nullable=False)
    order_id = Column(Integer, ForeignKey(AmazonOrdersModel.id),
                      nullable=False)
    amazon_id = Column(Text, nullable=False)
    origin_id = Column(Numeric(precision=8, scale=2))
    variation_id = Column(Integer, nullable=False)
    ean = Column(String(13))
    asin = Column(String(length=10), nullable=False)
    netto = Column(Numeric(precision=8, scale=2))
    gross = Column(Numeric(precision=8, scale=2))
    quantity = Column(Integer, nullable=False)
    statistic_entry = Column(Integer, nullable=True, default=-1)
    daily_statistic_entry = Column(Integer, nullable=True, default=-1)
    returned = Column(Boolean, default=False)
    order = relationship("AmazonOrdersModel",
                         foreign_keys="AmazonOrderItemsModel.order_id",
                         lazy="joined",
                         back_populates="order_items")


@generic_repr
class IdentificationModel(Base):
    __tablename__ = "identification"

    variation_id = Column(Integer, primary_key=True, autoincrement=False)
    ean = Column(String(13))
    sku = Column(Text(50))
    asin_all = Column(String(10))
    name = Column(String(300))


@generic_repr
class StockModel(Base):
    __tablename__ = "current_stock"

    variation_id = Column(Integer, primary_key=True, autoincrement=False)
    ean = Column(String(13))
    stock_warehouse_1 = Column(Integer, default=0)
    stock_warehouse_2 = Column(Integer, default=0)
    stock_warehouse_3 = Column(Integer, default=0)
    stock_warehouse_4 = Column(Integer, default=0)
    stock_warehouse_5 = Column(Integer, default=0)
    stock_warehouse_6 = Column(Integer, default=0)
    stock_warehouse_7 = Column(Integer, default=0)
    stock_warehouse_8 = Column(Integer, default=0)
    stock_warehouse_9 = Column(Integer, default=0)

    def update_row(self, row):
        for key, value in row.items():
            setattr(self, key, value)

    def update_by_dataframe(self, dataframe):
        """
        Get a matching row from a dataframe and update the database entry.
        """
        row = dataframe[dataframe['ean'] == self.ean].to_dict(orient='records')
        if len(row) > 1:
            logger.warning(f"[{self.__tablename__}] Multiple rows match ean "
                           f"{self.ean}.\nVariation ID: "
                           f"{[x['variation_id'] for x in row]}")
        try:
            for key, value in row[0].items():
                setattr(self, key, value)
        except IndexError:
            logger.warning(f"[{self.__tablename__}] No row matches ean "
                           f"{self.ean}")


class DataAccessLayer:
    def __init__(self):
        self.connection = ''
        self.engine = None
        self.Session = None
        self.session = None

    def connect(self):
        self.engine = create_engine(self.connection)
        Base.metadata.bind = self.engine
        Base.metadata.create_all(self.engine)
        self.session = sessionmaker(bind=self.engine)


dal = DataAccessLayer()
