"""
Import Amazon orders from a seller-central export to a MariaDB database.

Copyright (C) 2021  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import configparser
import datetime
import getpass
import os
import pathlib
import sys
from urllib.parse import urlparse
import keyring
import requests
import pandas
import tqdm

from loguru import logger

from amazon_import.service import DataImport, DataCollector
import amazon_import.currency_rate as currency_rate


PROG_NAME = 'amazon_import'
USER = os.getlogin()
if sys.platform == 'linux':
    BASE_PATH = os.path.join(
        '/', 'home', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )
elif sys.platform == 'win32':
    BASE_PATH = os.path.join(
        'C:\\', 'Users', str(f'{USER}'), '.config', 'plenty_rait_tool'
    )

DB_CONFIG = os.path.join(BASE_PATH, 'db_config.ini')
CONFIG_FOLDER = os.path.join(BASE_PATH, PROG_NAME)
if not os.path.exists(CONFIG_FOLDER):
    pathlib.Path(CONFIG_FOLDER).mkdir(parents=True, exist_ok=True)

CONFIG_PATH = os.path.join(CONFIG_FOLDER, 'config.ini')

if not os.path.exists(DB_CONFIG):
    open(DB_CONFIG, 'a').close()

if not os.path.exists(CONFIG_PATH):
    open(CONFIG_PATH, 'a').close()


def is_url(url):
    try:
        result = urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False


def setup_argparser():
    """ Return: [Argparse Object] """
    parser = argparse.ArgumentParser(prog='')
    parser.add_argument('--amazon_order_export_file', '-o', '--order_export',
                        required=False, help="Export from Amazon containing"
                        " the orders and order items to be imported.",
                        dest='orders', type=pathlib.Path)
    parser.add_argument('--reset-credentials', '-r', '--reset', required=False,
                        help="Reset the credentials within your system's"
                             " keyring",
                        action='store_true', dest='reset')
    parser.add_argument('--create_tables', required=False,
                        help="Create the database tables for amazon orders"
                        " and amazon order items.",
                        action='store_true', dest='tables')
    parser.add_argument('--host', '-l', required=False,
                        help='The location of the database remote server or '
                             'localhost',
                        dest='host', default='localhost')
    parser.add_argument('--database', '--db', '-d', required=False,
                        help='Name of the database',
                        dest='db')
    parser.add_argument('--cleanup', '--clean', '--variationid_cleanup',
                        required=False, help="Replace deleted variation IDs"
                        " with the new ID", dest='cleanup',
                        action='store_true')
    parser.add_argument('--amazon_stock_export_file', '-s', '--stock_export',
                        required=False, help="Export from Amazon containing "
                        "the stock-data of the amazon-warehouse to be imported",
                        dest='stock', type=pathlib.Path)
    parser.add_argument('--amazon_warehouse_region', '--region',
                        required=False, help="Select the warehouse-region for "
                        "your amazon_stock_export_file", dest='region',
                        choices=['EU', 'UK', 'US'])

    namespace = parser.parse_args()
    if not any([getattr(namespace, x) for x in ['orders', 'stock', 'cleanup']]):
        logger.error("Either '--order_export', '--stock_export' or "
                     "'--cleanup' are required.")
        return None
    if namespace.stock and not namespace.region:
        logger.error("amazon_stock_export_file was given but no"
                     " amazon_warehouse_region.")
        return None
    return namespace


def build_origin_map(config: configparser.ConfigParser) -> dict:
    """
    Create the match table for a amazon sales channel/fulfillment channel
    combination to a plentymarkets origin ID.
    """
    origin_map: dict = {
        'FBA': {
            'Amazon.de': {'B2C': '', 'B2B': ''},
            'Amazon.co.uk': {'B2C': '', 'B2B': ''},
            'Amazon.com': {'B2C': '', 'B2B': ''},
            'Amazon.fr': {'B2C': '', 'B2B': ''},
            'Amazon.it': {'B2C': '', 'B2B': ''},
            'Amazon.es': {'B2C': '', 'B2B': ''},
            'Amazon.nl': {'B2C': '', 'B2B': ''},
            'Amazon.se': {'B2C': '', 'B2B': ''}
        },
        'FBM': {
            'Amazon.de': {'B2C': '', 'B2B': ''},
            'Amazon.co.uk': {'B2C': '', 'B2B': ''},
            'Amazon.com': {'B2C': '', 'B2B': ''},
            'Amazon.fr': {'B2C': '', 'B2B': ''},
            'Amazon.it': {'B2C': '', 'B2B': ''},
            'Amazon.es': {'B2C': '', 'B2B': ''},
            'Amazon.nl': {'B2C': '', 'B2B': ''},
            'Amazon.se': {'B2C': '', 'B2B': ''}
        }
    }
    config_option_names: list = [
        'amazon_fba_GER', 'amazon_fba_GER_b2b',
        'amazon_GER', 'amazon_GER_b2b',
        'amazon_fba_GBR', 'amazon_fba_GBR_b2b',
        'amazon_GBR', 'amazon_GBR_b2b',
        'amazon_fba_USA', 'amazon_USA',
        'amazon_fba_USA_b2b', 'amazon_USA_b2b',
        'amazon_fba_FRA', 'amazon_FRA',
        'amazon_fba_FRA_b2b', 'amazon_FRA_b2b',
        'amazon_fba_ITA', 'amazon_ITA',
        'amazon_fba_ITA_b2b', 'amazon_ITA_b2b',
        'amazon_fba_ESP', 'amazon_ESP',
        'amazon_fba_ESP_b2b', 'amazon_ESP_b2b',
        'amazon_fba_NLD', 'amazon_NLD',
        'amazon_fba_NLD_b2b', 'amazon_NLD_b2b',
        'amazon_fba_SWE', 'amazon_SWE'
        'amazon_fba_SWE_b2b', 'amazon_SWE_b2b'
    ]
    chan_ctry_map: dict = {
        'GER': 'Amazon.de',
        'GBR': 'Amazon.co.uk',
        'USA': 'Amazon.com',
        'FRA': 'Amazon.fr',
        'ITA': 'Amazon.it',
        'ESP': 'Amazon.es',
        'NLD': 'Amazon.nl',
        'SWE': 'Amazon.se'
    }

    if not config.has_section(section='Origins'):
        return {}

    for option_name in config_option_names:
        if not config.has_option(section='Origins', option=option_name):
            continue
        option_parts = option_name.split('_')
        if 'fba' in option_parts:
            if 'b2b' in option_parts:
                origin_map['FBA'][chan_ctry_map[option_parts[-2]]]['B2B'] =\
                    config['Origins'][option_name]
            else:
                origin_map['FBA'][chan_ctry_map[option_parts[-1]]]['B2C'] =\
                    config['Origins'][option_name]
        else:
            if 'b2b' in option_parts:
                origin_map['FBM'][chan_ctry_map[option_parts[-2]]]['B2B'] =\
                    config['Origins'][option_name]
            else:
                origin_map['FBM'][chan_ctry_map[option_parts[-1]]]['B2C'] =\
                    config['Origins'][option_name]

    return origin_map


def add_recent_exchange_rate(config: configparser.ConfigParser) -> None:
    """
    Gather the most recent conversion rates for british pound, us dollar,
    swedish krones and Polish złoty. These are used to convert the sales price
    to euro for orders from the current year, as there is no yearly average for
    the current year available.
    """
    if not config.has_section(section='General'):
        logger.error("Configuration doesn't contain the 'General' section.")
        return
    options = ['exchange_rate_uk', 'exchange_rate_us', 'exchange_rate_se']
    current_year = datetime.datetime.now().strftime("%Y")
    key_option_map: dict = {
        'exchange_rate_uk': 'GBP',
        'exchange_rate_us': 'USD',
        'exchange_rate_se': 'SEK',
        'exchange_rate_pl': 'PLN'
    }
    for option in options:
        currency = key_option_map[option]
        if not config.has_option(section='General', option=option):
            logger.warning("Configuration doesn't contain the most recent "
                           "{currency} exchange rate data.")
            continue
        endpoint = config['General'][option]

        if not endpoint or not is_url(url=endpoint):
            logger.warning("Configuration doesn't contain the most recent "
                           "{currency} exchange rate data.")
            continue

        rate = requests.get(endpoint)
        if not rate:
            logger.warning("No data received from: {endpoint}.")
        try:
            currency_rate.currency_rate[current_year][currency] =\
                rate.json()['rates']['EUR']
        except Exception:
            logger.warning("No data received from: {endpoint}.")


def get_database(config: configparser.ConfigParser,
                 args: argparse.ArgumentParser) -> tuple:
    """
    Get the name of the target database and the host (localhost/remote IP).

    CLI arguments are prioritized over configuration entries.
    """
    if args.db:
        db_name = args.db
    elif config.has_option(section='DATABASE', option='database_name'):
        db_name = config['DATABASE']['database_name']
    else:
        logger.error("Database name required either from the database "
                     "configuration or from CLI argument.")
        sys.exit(1)

    if args.host:
        host = args.host
    elif config.has_option(section='DATABASE', option='host'):
        host = config['DATABASE']['host']
    else:
        logger.info("No host declared in the configuration or CLI "
                    "arguments, fall back to default 'localhost'")
        host = 'localhost'

    return (host, db_name)


def get_database_credentials(reset: bool, host: str, database: str) -> tuple:
    """
    Get the login credentials for the database from a system wide keyring.

    Parameters:
        reset           [bool]  -   CLI parameter that indicates that old
                                    credentials should be overwritten.
        host            [str]   -   Either localhost or the IP of the remote
        database        [str]   -   Name of the database at the host

    Return:
                        [tuple] -   The username and password
    """
    user = keyring.get_password('order_list_database_identity', 'user')
    password = keyring.get_password('order_list_database_identity', 'password')
    if not user or not password or reset:
        print(f"Login credentials for {database}@{host}:")
        user = input('Username: ')
        password = getpass.getpass()
        keyring.set_password('order_list_database_identity', 'user', user)
        keyring.set_password('order_list_database_identity', 'password',
                             password)
        return (user, password)
    return (user, password)


def main():
    db_config = configparser.ConfigParser()
    db_config.read(DB_CONFIG)
    config = configparser.ConfigParser()
    config.read(CONFIG_PATH)

    args = setup_argparser()
    if not args:
        sys.exit(1)

    collector = DataCollector()

    origin_map = build_origin_map(config=config)
    if not origin_map:
        logger.error('No origins section found in Configuration.')
        sys.exit(1)

    (host, db_name) = get_database(config=db_config, args=args)
    (user, password) = get_database_credentials(reset=args.reset, host=host,
                                                database=db_name)

    connection_string = str(
        f"mysql+pymysql://{user}:{password}@{host}/{db_name}")
    amazon_import = DataImport(origin_map=origin_map)
    amazon_import.create_database_session(connection_string=connection_string)
    add_recent_exchange_rate(config=config)

    if args.tables:
        amazon_import.create_tables()

    if args.cleanup:
        amazon_import.cleanup_variation_ids()
        sys.exit(0)

    if args.orders:
        if not args.orders.exists():
            logger.error(f"File at path {args.orders} doesn't exist")
            sys.exit(1)
        export = collector.read_amazon_order_data(path=str(args.orders))
        if len(export.index) == 0:
            sys.exit(1)
        orders = []
        for row in tqdm.tqdm(export.itertuples(), total=len(export.index)):
            if row.orderstatus == 'Cancelled' or row.itemstatus == 'Cancelled':
                continue

            order = amazon_import.handle_order(row=row, local=orders)
            if not order:
                continue

            order_item = amazon_import.create_order_item(row=row,
                                                        order=order)
            if not order_item:
                logger.error(f"Creation of order item failed on row:\n{row}"
                            "\nABORT")
                sys.exit(1)

        amazon_import.session.add_all(orders)
        amazon_import.session.commit()

    if args.stock:
        if not config.has_section(section='Stocks'):
            logger.error("Configuration doesn't contain the 'General' section.")
        if not args.stock.exists():
            logger.error(f"File at path {args.stock} doesn't exist")
            sys.exit(1)
        export = collector.read_amazon_stock_data(import_path=str(args.stock))
        if len(export.index) == 0:
            sys.exit(1)
        try:
            amazon_import.import_amazon_stock(
                dataframe=export,warehouse=config['Stocks'][args.region],
                region = args.region
            )
        except KeyError:
            logger.error(f"{args.region} region not found in the config")
